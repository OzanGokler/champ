﻿BalanceUIMobile = function () {
}

var BalanceUIMobile = function () { };

BalanceUIMobile.prototype = {
    cashValueText: null,
    coinsValueText: null,
    cashBetValueText: null,
    cashWinValueText: null,

    coinBet: null,
    betLevel: null,
    coinBetText: null,
    betLevelText: null,


    cashText: null,
    coinsText: null,
    cashBetText: null,
    cashWinText: null,
    fullscreenBtn: null,
    fullscreenBtnBg: null,

    cashValue: 0,
    coinsValue: 0,
    cashBetValue: 0,
    cashWinValue: 0,

    cashValueProgress: { step: 0 },
    coinsValueProgress: { step: 0 },
    cashBetValueProgress: { step: 0 },
    cashWinValueProgress: { step: 0 },

    layer: null,
    balanceTextGroup: null,
    betTextGroup: null,
    youWinText: null,
    wonCoinAmountText: null,
    youWinCoinsText: null,
    youWinTextGroup: null,
    buttonsGroup: null,
    greyBarBg: null,
    blackBarBg: null,
    blackBarBg2: null,

    groupLayoutTop: new GroupHorizontalLayout(175, 200),
    groupLayoutBottom: new GroupHorizontalLayout(175, 200),
    groupLayoutWinText: new GroupHorizontalLayout(175, 200),

    menuButtonBg: null,
    menuButtonSprite: null,
    balanceMenu: null,

    greyBarBgHeight: null,
    blackBarBgHeight: null,

    buttonWidth: 165,

    balanceBarHeight: 120,

    greyBarBgAlpha: 0.85,

    decimalCount: 2,
    style: { font: "14px Helvetica", fill: "#ffffff", wordWrap: false },
    style2: { font: "20px Helvetica", fill: "#ffffff", wordWrap: false },
    //spinButtonHtml:null,

    positionUI: function () {
        if (!this.layer || !this.layer.game) this.layer = game.add.group();
        if (!this.balanceTextGroup || !this.balanceTextGroup.game) this.balanceTextGroup = game.add.group();
        if (!this.betTextGroup || !this.betTextGroup.game) this.betTextGroup = game.add.group();
        if (!this.buttonsGroup || !this.buttonsGroup.game) this.buttonsGroup = game.add.group();
        if (!this.youWinTextGroup || !this.youWinTextGroup.game) this.youWinTextGroup = game.add.group();
        if (!this.balanceMenu) this.balanceMenu = new BalanceMenu();

        this.blackBarBgHeight = this.balanceBarHeight * 0.25;
        this.greyBarBgHeight = this.balanceBarHeight * 0.75;

        this.balanceMenu.positionUI(this.greyBarBgHeight);

        vm.onGameScaled.remove(this.repositionUi, this);
        vm.onGameScaled.add(this.repositionUi, this);

        //No need to recreate stuff, just return.
        if (this.greyBarBg && this.greyBarBg.game) return;

        console.log("BalanceUI - positionUI");

        this.blackBarBg = game.add.graphics(0, 0);
        this.blackBarBg.beginFill(0x000000);
        this.blackBarBg.drawRect(0, 0, game.width, this.blackBarBgHeight);
        this.blackBarBg.endFill();

        this.greyBarBg = game.add.graphics(0, 0);
        this.greyBarBg.beginFill(0x282828, this.greyBarBgAlpha);
        this.greyBarBg.drawRect(0, 0, game.width, this.greyBarBgHeight - this.blackBarBgHeight);
        this.greyBarBg.endFill();

        this.menuButtonBg = game.add.graphics(0, 0);
        this.menuButtonBg.beginFill(0x000000);
        this.menuButtonBg.drawRect(0, 0, this.buttonWidth / 2, this.greyBarBgHeight);
        this.menuButtonBg.endFill();

        this.fullscreenBtnBg = game.add.graphics(0, 0);
        this.fullscreenBtnBg.beginFill(0x000000);
        this.fullscreenBtnBg.drawRect(0, 0, this.buttonWidth / 2, this.greyBarBgHeight);
        this.fullscreenBtnBg.endFill();

        this.fullscreenBtn = game.add.sprite(game.width - this.buttonWidth * 7.40 - game.scalingOffset, game.height - this.greyBarBgHeight, "maximize-icon");
        this.setMainButton(this.fullscreenBtn);

        this.menuButtonSprite = game.add.sprite(game.width - this.buttonWidth / 2, game.height - this.greyBarBgHeight / 2, "menu-options");
        this.setMainButton(this.menuButtonSprite);
        this.menuButtonSprite.inputEnabled = true;
        this.menuButtonSprite.events.onInputDown.add(this.onMenuButtonClicked, this);

        this.cashText = game.add.text(0, 0, languageManager.langJSON.balanceUICash, this.style);
        this.cashText.anchor.setTo(0.5, 0.5);
        this.coinsText = game.add.text(0, 0, languageManager.langJSON.balanceUICoins, this.style);
        this.coinsText.anchor.setTo(0.5, 0.5);
        this.cashBetText = game.add.text(0, 0, languageManager.langJSON.balanceUIBet, this.style);
        this.cashBetText.anchor.setTo(0.5, 0.5);
        this.cashWinText = game.add.text(0, 0, languageManager.langJSON.balanceUIWin, this.style);
        this.cashWinText.anchor.setTo(0.5, 0.5);

        this.cashValueText = game.add.text(0, 0, this.cashValue.numberFormat(this.decimalCount), this.style);
        this.cashValueText.anchor.setTo(0, 0.5);
        this.coinsValueText = game.add.text(0, 0, this.coinsValue.numberFormat(this.decimalCount), this.style);
        this.coinsValueText.anchor.setTo(0, 0.5);
        this.cashBetValueText = game.add.text(0, 0, this.cashBetValue.numberFormat(this.decimalCount), this.style);
        this.cashBetValueText.anchor.setTo(0, 0.5);
        this.cashWinValueText = game.add.text(0, 0, this.cashWinValue.numberFormat(this.decimalCount), this.style);
        this.cashWinValueText.anchor.setTo(0, 0.5);

        this.coinBetText = game.add.text(0, 0, languageManager.langJSON.mobileCoinValue, this.style2);
        this.coinBetText.anchor.setTo(0.5, 0.5);
        this.coinBetText.alpha = 0.4;
        this.betLevelText = game.add.text(0, 0, languageManager.langJSON.mobileBetLevel, this.style2);
        this.betLevelText.anchor.setTo(0.5, 0.5);
        this.betLevelText.alpha = 0.4;

        this.coinBet = game.add.text(0, 0, "0", this.style2);
        this.coinBet.anchor.setTo(0, 0.5);
        this.betLevel = game.add.text(0, 0, "0", this.style2);
        this.betLevel.anchor.setTo(0, 0.5);

        this.youWinText = game.add.text(0, 0, languageManager.langJSON.youWonCoins, this.style2);
        this.youWinText.anchor.setTo(0, 0.5);

        this.wonCoinAmountText = game.add.text(0, 0, "0", this.style2);
        this.wonCoinAmountText.anchor.setTo(0, 0.5);

        this.youWinCoinsText = game.add.text(0, 0, languageManager.langJSON.youWonCoins2, this.style2);
        this.youWinCoinsText.anchor.setTo(0, 0.5);

        this.youWinTextGroup.alpha = 0;

        this.buttonsGroup.add(this.balanceMenu.uiGroup);
        this.buttonsGroup.add(this.greyBarBg);
        this.buttonsGroup.add(this.blackBarBg);
        this.buttonsGroup.add(this.menuButtonBg);
        this.buttonsGroup.add(this.fullscreenBtnBg);
        this.buttonsGroup.add(this.menuButtonSprite);
        this.buttonsGroup.add(this.fullscreenBtn);

        this.balanceTextGroup.add(this.cashText);
        this.balanceTextGroup.add(this.coinsText);
        this.balanceTextGroup.add(this.cashBetText);
        this.balanceTextGroup.add(this.cashWinText);

        this.balanceTextGroup.add(this.cashValueText);
        this.balanceTextGroup.add(this.coinsValueText);
        this.balanceTextGroup.add(this.cashBetValueText);
        this.balanceTextGroup.add(this.cashWinValueText);

        this.betTextGroup.add(this.betLevelText);
        this.betTextGroup.add(this.coinBetText);
        this.betTextGroup.add(this.betLevel);
        this.betTextGroup.add(this.coinBet);

        this.youWinTextGroup.add(this.youWinText);
        this.youWinTextGroup.add(this.wonCoinAmountText);
        this.youWinTextGroup.add(this.youWinCoinsText);

        this.layer.add(this.buttonsGroup);
        this.layer.add(this.balanceTextGroup);
        this.layer.add(this.betTextGroup);
        this.layer.add(this.youWinTextGroup);

        this.greyBarBg.position.setTo(0, 0);
        this.blackBarBg.position.setTo(0, 0);

        this.updateBalanceUI(gm.CurrentRealMoney, gm.currentCredits, gm.TotalBetInRealMoney, gm.CumulativeRealMoneyWon, true);

        this.repositionUi();
    },


    repositionUi: function () {
        if (!this.menuButtonBg || !this.menuButtonBg.game) return;
        console.log("repositioned: " + game.scalingOffset);

        this.menuButtonSprite.x = game.width - this.buttonWidth / 4.5 - game.scalingOffset;
        this.fullscreenBtn.x = game.scalingOffset + 40;
        this.menuButtonSprite.y = game.height + 35 - this.greyBarBgHeight;
        this.fullscreenBtn.y = game.height + 35 - this.greyBarBgHeight;

        var textY = (game.height - this.blackBarBgHeight) + this.blackBarBgHeight / 2;
        this.menuButtonBg.x = game.width - this.buttonWidth - game.scalingOffset + 85;
        this.menuButtonBg.y = game.height - this.greyBarBgHeight;
        this.blackBarBg.y = game.height - this.blackBarBgHeight;
        this.greyBarBg.y = game.height - this.greyBarBgHeight;
        this.fullscreenBtnBg.x = game.scalingOffset;
        this.fullscreenBtnBg.y = game.height - this.greyBarBgHeight;
        this.cashText.y = textY;
        this.coinsText.y = textY;
        this.cashBetText.y = textY;
        this.cashWinText.y = textY;
        this.cashValueText.y = textY;
        this.coinsValueText.y = textY;
        this.cashBetValueText.y = textY;
        this.cashWinValueText.y = textY;

        this.betLevelText.y = textY - 40;
        this.coinBetText.y = textY - 40;
        this.betLevel.y = textY - 40;
        this.coinBet.y = textY - 40;
        this.youWinText.y = textY - 40;
        this.wonCoinAmountText.y = textY - 40;
        this.youWinCoinsText.y = textY - 40;

        this.groupLayoutBottom.positionItems(this.balanceTextGroup, 2);
        this.groupLayoutTop.positionItems(this.betTextGroup, 2);
    },

    showCoinWin: function (data, delay) {
        this.wonCoinAmountText.text = data.numberFormat(0);
        this.groupLayoutWinText.positionItems(this.youWinTextGroup, 3, true);

        game.tweens.removeFrom(this.betTextGroup, false);
        game.tweens.removeFrom(this.youWinTextGroup, false);

        var fadeOutTween = game.add.tween(this.betTextGroup).to({ alpha: 0 }, 200, Phaser.Easing.Linear.In, true, delay);
        var fadeInTween = game.add.tween(this.youWinTextGroup).to({ alpha: 1 }, 300, Phaser.Easing.Linear.In, false);
        var fadeOutTween2 = game.add.tween(this.youWinTextGroup).to({ alpha: 0 }, 300, Phaser.Easing.Linear.In, false, 1000);
        var fadeInTween2 = game.add.tween(this.betTextGroup).to({ alpha: 1 }, 200, Phaser.Easing.Linear.In, false);
        fadeOutTween.chain(fadeInTween);
        fadeInTween.chain(fadeOutTween2);
        fadeOutTween2.chain(fadeInTween2);
    },

    toggleFullScreenButton: function (active) {
        if (active) {
            this.fullscreenBtn.loadTexture("minimize-icon");
        }
        else {
            this.fullscreenBtn.loadTexture("maximize-icon");
        }
    },

    toggleMenuButton: function (active) {
        this.toggleButton(this.menuButtonSprite, active, true);
    },

    toggleBalanceMenuButtons: function (active) {
        this.toggleButton(this.balanceMenu.spinsettings, active, true);
        this.toggleButton(this.balanceMenu.paytable, active, true);
        this.toggleButton(this.balanceMenu.sound, active, true);
        this.toggleButton(this.balanceMenu.betsettings, active, true);
        this.toggleButton(this.balanceMenu.house_icon, active, true);
    },
    toggleButton: function (button, activate, animate, visualOnly) {
        game.tweens.removeFrom(button, false);
        animate = animate === undefined ? true : animate;
        var targetAlpha = activate ? 1 : 0.2;
        if (animate) game.add.tween(button).to({ alpha: targetAlpha }, 250, Phaser.Easing.Linear.In, true);
        else button.alpha = targetAlpha;
        if (!visualOnly && button.input) {
            if (button instanceof Phaser.Group) {
                button.forEach(function (target) { target.input.enabled = this; }, activate);
                button.input.enabled = activate; //To be able to properly check its status without loooping the children.
            }
            else button.input.enabled = activate;
        }
    },

    setMainButton: function (item) {
        item.anchor.setTo(0.5);
        var buttonScale = item.width / item.height;
        item.height = this.greyBarBgHeight / 2;
        item.width = (buttonScale * item.height);
    },

    toggleMenu: function (isOpened) {
        if (isOpened) {
            this.menuButtonSprite.loadTexture("close-icon");
            this.setMainButton(this.menuButtonSprite);
            this.balanceMenu.toggleAutoSpinEnabledButton(gm.autoSpinEnabled);
            this.balanceMenu.show();
        }
        else {
            this.menuButtonSprite.loadTexture("menu-options");
            this.setMainButton(this.menuButtonSprite);
            this.balanceMenu.hide();
        }
    },

    onMenuButtonClicked: function () {
        console.error("log");
        if (!this.balanceMenu.isOpen) {
            this.toggleMenu(true);
        }
        else {
            this.toggleMenu(false);
        }
    },

    updateBalanceUI: function (cash, coins, cashBet, cashWin, skipAnimation) {
        if (skipAnimation) {
            this.cashValue = cash;
            this.cashValueText.text = cash.numberFormat(this.decimalCount);
            this.coinsValue = coins;
            this.coinsValueText.text = coins.numberFormat(0);
            this.cashBetValue = cashBet;
            this.cashBetValueText.text = cashBet.numberFormat(this.decimalCount);
            this.cashWinValue = cashWin;
            this.cashWinValueText.text = cashWin.numberFormat(this.decimalCount);
        }
        else {
            if (cash != undefined && cash !== this.cashValue) {
                this.startValueChangeTween(this.cashValueProgress, this.cashValueText, this.cashValue, cash, this, true);
                this.cashValue = cash;
            }
            if (coins != undefined && coins !== this.coinsValue) {
                this.startValueChangeTween(this.coinsValueProgress, this.coinsValueText, this.coinsValue, coins, this, false);
                this.coinsValue = coins;
            }
            if (cashBet != undefined && cashBet !== this.cashBetValue) {
                this.startValueChangeTween(this.cashBetValueProgress, this.cashBetValueText, this.cashBetValue, cashBet, this, true);
                this.cashBetValue = cashBet;
            }
            if (cashWin != undefined && cashWin !== this.cashWinValue) {
                this.startValueChangeTween(this.cashWinValueProgress, this.cashWinValueText, this.cashWinValue, cashWin, this, true);
                this.cashWinValue = cashWin;
            }
        }
    },

    updateBetUI: function (betLevel, winLines, coinValue) {
        this.coinBet.text = coinValue.numberFormat(this.decimalCount);
        this.betLevel.text = betLevel.numberFormat(0);
        //this.winLineLevel.text = winLines.numberFormat(this.decimalCount);
        this.balanceMenu.updateBetUI(betLevel, winLines, coinValue);
    },

    updateSpinValue: function (spinLeft) {
        this.balanceMenu.updateSpinValue(spinLeft);
    },

    startValueChangeTween: function (valueProgressObj, textObject, startValue, endValue, context, allowDecimal) {
        game.tweens.removeFrom(valueProgressObj, false);
        valueProgressObj.step = 0;
        var tween = game.add.tween(valueProgressObj).to({ step: 1 }, 250, Phaser.Easing.Linear.Out, true);
        tween.onUpdateCallback(this.valueUpdate, { text: textObject, startValue: startValue, endValue: endValue, context: context, allowDecimal: allowDecimal });
        tween.onComplete.add(this.valueComplete, { text: textObject, endValue: endValue, context: context, allowDecimal: allowDecimal });
    },

    valueUpdate: function (twn, percent) {
        this.startValue = this.startValue == undefined ? 0 : this.startValue;
        this.text.setText(game.math.linear(this.startValue, this.endValue, percent).numberFormat(this.allowDecimal ? this.context.decimalCount : 0));
    },

    valueComplete: function () {
        this.text.setText(this.endValue.numberFormat(this.allowDecimal ? this.context.decimalCount : 0));
    }
}