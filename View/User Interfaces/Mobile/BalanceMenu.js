﻿BalanceMenu = function () {
}


BalanceMenu.prototype = {
    blackBackground: null,
    isOpen: false,
    uiGroup: null,

    titleBet: null,
    titleSpin: null,
    betLevelText: null,
    betLevelStaticText: null,
    lineLevelText: null,
    lineLevelStaticText: null,
    coinValueText: null,
    coinValueStaticText: null,
    maxBtnClicked: false,
    arrow_left: null,

    mute: false,
    spinSettingMenu: null,
    BetSettingMenu: null,

    outSpinRatCounterText: null,
    outSpinRatCounter: null,
    maxBetBtn: null,
    maxBetText: null,

    arrow_right: null,


    arrow_right_lineLevel: null,
    arrow_right_coin: null,
    arrow_right_Bg: null,


    arrow_left_Bg: null,

    arrow_right_BgSpin: null,
    arrow_rightSpin: null,
    arrow_leftSpin: null,
    arrow_left_BgSpin: null,
    autoSpinBtnClicked: false,
    greyBackgrounds: null,
    greyBarBgHeight: 90,
    bottomHeight: null,
    buttonWidth: 165,
    rowCount: 6,
    decimalCount: 2,
    menuGroups: null,
    betsettings: null,
    lowerMenuBg: null,
    house_icon: null,
    spinsettings: null,
    paytable: null,
    sound: null,
    rowHeight: null,
    autoSpin: null,

    onAutoSpinToggled: new Phaser.Signal(),

    show: function () {
        this.isOpen = true;
        this.uiGroup.visible = true;
    },

    hide: function () {
        this.isOpen = false;
        this.uiGroup.visible = false;
    },

    positionUI: function (bottomHeight) {
        this.bottomHeight = bottomHeight;

        if (!this.uiGroup) this.uiGroup = game.add.group();

        vm.onGameScaled.remove(this.repositionUi, this);
        vm.onGameScaled.add(this.repositionUi, this);

        this.menuGroups = new Array(this.rowCount + 3);
        this.greyBackgroundsBet = new Array(this.rowCount);
        this.greyBackgroundsSpin = new Array(this.rowCount);
        this.spinSettingMenu = new Array(this.rowCount);
        this.BetSettingMenu = new Array(this.rowCount);
        this.arrow_right_Bg = new Array(this.rowCount);
        this.arrow_left_Bg = new Array(this.rowCount);
        this.arrow_left = new Array(this.rowCount);
        this.arrow_right = new Array(this.rowCount);
        this.lowerMenuBg = new Array(this.rowCount);
        var menuHeight = game.height - this.bottomHeight;
        this.rowHeight = (menuHeight / this.rowCount) + 20;
        var i;
        for (i = 0; i < this.BetSettingMenu.length; i++) {
            this.BetSettingMenu[i] = game.add.group();
            this.BetSettingMenu[i].y = i * this.rowHeight + 4;
        }

        for (i = 0; i < this.spinSettingMenu.length; i++) {
            this.spinSettingMenu[i] = game.add.group();
            this.spinSettingMenu[i].y = i * this.rowHeight + 4;
        }

        for (i = 0; i < this.menuGroups.length; i++) {
            this.menuGroups[i] = game.add.group();
            //this.menuGroups[i].y = this.BetSettingMenu[i].y * this.rowHeight + 4;
        }
        var heightFactor = 65;


        this.betsettings = game.add.sprite(400, game.height - (this.greyBarBgHeight + heightFactor), "icon-betsettings");
        this.betsettings.scale.setTo(0.1);
        this.betsettings.anchor.setTo(0.5);
        this.betsettings.inputEnabled = true;
        this.betsettings.events.onInputDown.add(function () {
            this.hideGroups(true, false, false, false, false);
        }, this);

        this.house_icon = game.add.sprite(this.rowHeight / 2, game.height - (this.greyBarBgHeight + heightFactor), "house-icon");
        this.house_icon.scale.setTo(0.5);
        this.house_icon.anchor.setTo(0.5);
        this.house_icon.inputEnabled = false;
        this.house_icon.events.onInputDown.add(function () {
            this.hideGroups(false, false, false, false, true);
        }, this);

        this.spinsettings = game.add.sprite(this.betsettings.x + (this.rowHeight + 5), game.height - (this.greyBarBgHeight + heightFactor), "icon-spinsettings");
        this.spinsettings.scale.setTo(0.1);
        this.spinsettings.anchor.setTo(0.5);
        this.spinsettings.inputEnabled = true;
        this.spinsettings.events.onInputDown.add(function () {
            this.hideGroups(false, true, false, false, false);
        },
            this);

        this.maxBetBtn = game.add.sprite(0, game.height - (this.greyBarBgHeight + 465), "mobileBtn");
        this.maxBetBtn.anchor.setTo(0.5);
        this.maxBetBtn.frame = 0;

        this.autoSpinBtn = game.add.sprite(0, game.height - (this.greyBarBgHeight + 465), "mobileBtn");
        this.autoSpinBtn.anchor.setTo(0.5);
        this.autoSpinBtn.frame = 1;
        this.autoSpinBtnText = game.add.text(this.autoSpinBtn.x, this.autoSpinBtn.y + 2, languageManager.langJSON.autoSpinOFF, { font: "30px Helvetica", fill: "#ffffff" });
        this.autoSpinBtnText.anchor.setTo(0.5);

        this.autoSpinBtn.inputEnabled = true;
        this.autoSpinBtn.events.onInputDown.add(this.autoSpinBtnDown, this);

        this.maxBetText = game.add.text(this.maxBetBtn.x, this.maxBetBtn.y + 2, languageManager.langJSON.maxBtnText, { font: "30px Helvetica", fill: "#ffffff" });
        this.maxBetText.anchor.setTo(0.5);

        this.paytable = game.add.sprite(this.spinsettings.x + (this.rowHeight + 5), game.height - (this.greyBarBgHeight + heightFactor), "paytable");
        this.paytable.anchor.setTo(0.5);
        this.paytable.inputEnabled = true;


        this.sound = game.add.sprite(this.paytable.x + (this.rowHeight + 5), game.height - (this.greyBarBgHeight + heightFactor), "sound");
        this.sound.anchor.setTo(0.5);
        this.sound.inputEnabled = true;
        // this.sound.events.onInputDown.add(this.onSoundIsDown,this);

        this.blackBackground = game.add.graphics(0, 0);
        this.blackBackground.beginFill(0x000000);
        this.blackBackground.drawRect(0, 0, game.width, game.height);
        this.blackBackground.endFill();

        this.titleBet = game.add.text(gameWidth / 2, this.rowHeight / 6, languageManager.langJSON.betSettings, { fill: "#ffffff" });
        this.titleBet.anchor.setTo(0.5);

        this.titleSpin = game.add.text(gameWidth / 2, this.rowHeight / 6, languageManager.langJSON.spinSetting, { fill: "#ffffff" });
        this.titleSpin.anchor.setTo(0.5);

        this.betLevelText = game.add.text(gameWidth - 300, this.rowHeight - 20, "1", { fill: "#ffffff" });
        this.betLevelStaticText = game.add.text(200, this.rowHeight - 20, languageManager.langJSON.mobileBetLevel, { fill: "#ffffff" });

        this.autoSpin = game.add.text(200, this.rowHeight - 20, languageManager.langJSON.autoSpinToggle, { fill: "#ffffff" });

        this.coinValueText = game.add.text(gameWidth - 300, this.rowHeight - 20, "3", { fill: "#ffffff" });
        this.coinValueStaticText = game.add.text(200, this.rowHeight - 20, languageManager.langJSON.mobileCoinValue, { fill: "#ffffff" });

        this.outSpinRatCounterText = game.add.text(200, this.rowHeight - 20, languageManager.langJSON.autoSpinValue, { fill: "#ffffff" });
        this.outSpinRatCounter = game.add.text(gameWidth - 300, this.rowHeight - 20, "3", { fill: "#ffffff" });

        for (i = 0; i < 3; i++) {
            this.greyBackgroundsBet[i] = game.add.graphics(0, 0);
            this.greyBackgroundsBet[i].beginFill(0x282828);
            this.greyBackgroundsBet[i].drawRect(0, (i * 5) + 50, game.width, this.rowHeight);
            this.greyBackgroundsBet[i].endFill();

            this.greyBackgroundsSpin[i] = game.add.graphics(0, 0);
            this.greyBackgroundsSpin[i].beginFill(0x282828);
            this.greyBackgroundsSpin[i].drawRect(0, (i * 5) + 50, game.width, this.rowHeight);
            this.greyBackgroundsSpin[i].endFill();
        }
        for (i = 0; i < 2; i++) {
            //
            this.arrow_left[i] = game.add.sprite(this.betLevelText.x - 150, ((this.greyBackgroundsBet[i].y + this.rowHeight) / 2) + (i * 5) + 50, "arrow_left");
            this.arrow_left[i].scale.setTo(0.08);
            this.arrow_left[i].anchor.setTo(0.5);
            this.arrow_right[i] = game.add.sprite(this.betLevelText.x + 200, ((this.greyBackgroundsBet[i].y + this.rowHeight) / 2) + (i * 5) + 50, "arrow_right");
            this.arrow_right[i].anchor.setTo(0.5);
            this.arrow_right[i].scale.setTo(0.08);

            this.arrow_right_Bg[i] = game.add.graphics(0, 0);
            this.arrow_right_Bg[i].beginFill(0x7e7e7e);
            this.arrow_right_Bg[i].drawRect(this.arrow_right[i].x - 40, (i * 5) + 50, this.rowHeight, this.rowHeight);
            this.arrow_right_Bg[i].endFill();
            this.arrow_left_Bg[i] = game.add.graphics(0, 0);
            this.arrow_left_Bg[i].beginFill(0x7e7e7e);
            this.arrow_left_Bg[i].drawRect(this.arrow_left[i].x - 35, (i * 5) + 50, this.rowHeight, this.rowHeight);
            this.arrow_left_Bg[i].endFill();
        }


        this.arrow_rightSpin = game.add.sprite(this.outSpinRatCounter.x + 200, ((this.greyBackgroundsSpin[1].y + this.rowHeight) / 2) + (i * 5) + 50, "arrow_right");
        this.arrow_rightSpin.scale.setTo(0.08);
        this.arrow_rightSpin.anchor.setTo(0.5);
        this.arrow_right_BgSpin = game.add.graphics(0, 0);
        this.arrow_right_BgSpin.beginFill(0x7e7e7e);
        this.arrow_right_BgSpin.drawRect(this.arrow_rightSpin.x - 40, (5) + 50, this.rowHeight, this.rowHeight);
        this.arrow_right_BgSpin.endFill();

        this.arrow_leftSpin = game.add.sprite(this.outSpinRatCounter.x - 150, ((this.greyBackgroundsSpin[1].y + this.rowHeight) / 2) + (i * 5) + 50, "arrow_left");
        this.arrow_leftSpin.scale.setTo(0.08);
        this.arrow_leftSpin.anchor.setTo(0.5);
        this.arrow_left_BgSpin = game.add.graphics(0, 0);
        this.arrow_left_BgSpin.beginFill(0x7e7e7e);
        this.arrow_left_BgSpin.drawRect(this.arrow_leftSpin.x - 35, (5) + 50, this.rowHeight, this.rowHeight);
        this.arrow_left_BgSpin.endFill();
        var j;
        for (j = 0; j < 5; j++) {
            this.lowerMenuBg[j] = game.add.graphics(0, 0);
            this.lowerMenuBg[j].beginFill(0xffffff);
            if (j === 0)
                this.lowerMenuBg[j].drawRect((this.betsettings.x - 35) * (j), (this.betsettings.y - (this.rowHeight / 2)), this.rowHeight, this.rowHeight);
            else {
                this.lowerMenuBg[j].drawRect((this.betsettings.x - 180) + (j * (this.rowHeight + 5)), (this.betsettings.y - (this.rowHeight / 2)), this.rowHeight, this.rowHeight);
            }
            this.lowerMenuBg[j].endFill();
            this.lowerMenuBg[j].alpha = 0.4;
        }


        this.BetSettingMenu[0].addMultiple([this.titleBet, this.greyBackgroundsBet[0], this.betLevelStaticText, this.arrow_left_Bg[0], this.arrow_left[0], this.betLevelText, this.arrow_right_Bg[0], this.arrow_right[0]]);
        this.BetSettingMenu[1].addMultiple([this.greyBackgroundsBet[1], this.coinValueStaticText, this.arrow_left_Bg[1], this.arrow_left[1], this.coinValueText, this.arrow_right_Bg[1], this.arrow_right[1]]);
        this.BetSettingMenu[2].addMultiple([this.greyBackgroundsBet[2], this.maxBetBtn, this.maxBetText]);

        for (i = 0; i < 3; i++) {
            this.menuGroups[i].add(this.BetSettingMenu[i]);
        }
        this.spinSettingMenu[0].addMultiple([this.titleSpin, this.greyBackgroundsSpin[0], this.autoSpin, this.autoSpinBtn, this.autoSpinBtnText]);
        this.spinSettingMenu[1].addMultiple([this.greyBackgroundsSpin[1], this.outSpinRatCounterText, this.arrow_left_BgSpin, this.arrow_leftSpin, this.outSpinRatCounter, this.arrow_right_BgSpin, this.arrow_rightSpin]);
        this.spinSettingMenu[2].addMultiple([this.greyBackgroundsSpin[2]]);
        for (j = 0, i = 3; j < 3, i < 6; i++, j++) {
            this.menuGroups[i].add(this.spinSettingMenu[j]);
        }

        this.menuGroups[this.menuGroups.length - 1].addMultiple([this.lowerMenuBg[0], this.house_icon, this.lowerMenuBg[1], this.betsettings, this.lowerMenuBg[2], this.spinsettings, this.lowerMenuBg[3], this.paytable, this.lowerMenuBg[4], this.sound]);
        this.uiGroup.addMultiple([this.blackBackground]);
        for (i = 0; i < this.menuGroups.length; i++) {
            this.uiGroup.add(this.menuGroups[i]);
        }
        this.uiGroup.visible = false;
        this.hideGroups(true, false, false, false, false);
        this.repositionUi();
    },

    onSoundIsDown: function (mute) {
        if (mute) {
            this.sound.loadTexture("mute");
        }
        else {
            this.sound.loadTexture("sound");
        }
    },

    //spinSettingShow: function () {
    //    console.log("spinsetting clicked");
    //    this.hidGroups(false, true, false, false, false);
    //},

    setMaxBetEvents: function () {
        this.maxBetBtn.events.onInputDown.add(this.onMaxBetDown, this);
        //this.maxBetBtn.events.onInputOver.add(this.onMaxBetDown, this);
        this.maxBetBtn.events.onInputUp.add(this.onMaxBetUp, this);
        //this.maxBetBtn.events.onInputOut.add(this.onMaxBetUp, this);

        this.maxBetText.events.onInputDown.add(this.onMaxBetDown, this);
        //this.maxBetText.events.onInputOver.add(this.onMaxBetDown, this);
        this.maxBetText.events.onInputUp.add(this.onMaxBetUp, this);
        //this.maxBetText.events.onInputOut.add(this.onMaxBetUp, this);
    },
    hideGroups: function (isBet, isSpin, isPayTble, isSound, isHome) {
        if (isSpin) {
            for (var i = 0; i < this.BetSettingMenu.length; i++) {
                this.BetSettingMenu[i].visible = false;
                this.spinSettingMenu[i].visible = true;
            }
            this.hidbtnBgs(isBet, isSpin, isHome);
        }
        else if (isBet) {
            for (var i = 0; i < this.BetSettingMenu.length; i++) {
                this.spinSettingMenu[i].visible = false;
                this.BetSettingMenu[i].visible = true;
            }
            this.hidbtnBgs(isBet, isSpin, isPayTble, isSound, isHome);
        }

        else if (isHome) {
            for (var i = 0; i < this.BetSettingMenu.length; i++) {
                this.spinSettingMenu[i].visible = false;
                this.BetSettingMenu[i].visible = false;
            }
            this.hidbtnBgs(isBet, isSpin, isPayTble, isSound, isHome);
        }
    },

    hidbtnBgs: function (isBet, isSpin, isHome) {
        if (isSpin) {
            this.lowerMenuBg[2].visible = false;
            this.lowerMenuBg[1].visible = true;
            this.lowerMenuBg[0].visible = true;
            this.lowerMenuBg[3].visible = true;
            this.lowerMenuBg[4].visible = true;
        }
        else if (isBet) {
            this.lowerMenuBg[2].visible = true;
            this.lowerMenuBg[1].visible = false;
            this.lowerMenuBg[0].visible = true;
            this.lowerMenuBg[3].visible = true;
            this.lowerMenuBg[4].visible = true;
        }

        else if (isHome) {
            this.lowerMenuBg[2].visible = true;
            this.lowerMenuBg[1].visible = true;
            this.lowerMenuBg[0].visible = false;
            this.lowerMenuBg[3].visible = true;
            this.lowerMenuBg[4].visible = true;
        }
    },

    toggleAutoSpinEnabledButton: function (active) {
        if (active) {
            this.autoSpinBtn.frame = 0;
            this.autoSpinBtnText.setText(languageManager.langJSON.autoSpinON);
            this.autoSpinBtnText.fill = "#0x282828";
            this.autoSpinBtnClicked = true;
        }
        else {
            this.autoSpinBtn.frame = 1;
            this.autoSpinBtnText.setText(languageManager.langJSON.autoSpinOFF);
            this.autoSpinBtnText.fill = "#FFFFFF";
            this.autoSpinBtnClicked = false;
        }
        this.onAutoSpinToggled.dispatch();
    },

    autoSpinBtnDown: function () {
        this.toggleAutoSpinEnabledButton(!this.autoSpinBtnClicked);
    },
    onMaxBetDown: function () {
        this.maxBetBtn.frame = 0;
        this.maxBetText.fill = "#0x282828";
        this.maxBtnClicked = true;
    },

    onMaxBetUp: function () {
        this.maxBetBtn.frame = 1;
        this.maxBetText.fill = "#FFFFFF";
        this.maxBtnClicked = false;
    },

    updateBetUI: function (betLevel, winLines, coinValue) {
        this.betLevelText.text = betLevel.numberFormat(0);
        //this.lineLevelText.text = winLines.numberFormat(this.decimalCount);
        this.coinValueText.text = coinValue.numberFormat(this.decimalCount);
    },
    updateSpinValue: function (spinLeft) {
        if (spinLeft === -1) {
            this.outSpinRatCounter.text = "∞";
        }
        else {
            this.outSpinRatCounter.text = spinLeft;
        }
    },

    repositionUi: function () {
        if (!this.betLevelText || !this.betLevelText.game) return;

        for (var i = 0; i < 2; i++) {
            this.arrow_right[i].x = game.width - this.buttonWidth / 4.5 - game.scalingOffset - 70;
            this.arrow_left[i].x = game.width - this.buttonWidth / 4.5 - game.scalingOffset - 395;

            this.arrow_right[i].y = ((this.greyBackgroundsBet[i].y + this.rowHeight) / 2) + (i * 5) + 50;
            this.arrow_left[i].y = ((this.greyBackgroundsBet[i].y + this.rowHeight) / 2) + (i * 5) + 50;

            this.arrow_right_Bg[i].graphicsData[0].shape.x = game.width - this.buttonWidth - game.scalingOffset;
            this.arrow_left_Bg[i].graphicsData[0].shape.x = game.width - this.buttonWidth - game.scalingOffset - 320;

            this.arrow_right_Bg[i].graphicsData[0].shape.y = (i * 5) + 50;
            this.arrow_left_Bg[i].graphicsData[0].shape.y = (i * 5) + 50;
        }
        this.arrow_rightSpin.x = game.width - this.buttonWidth / 4.5 - game.scalingOffset - 70;
        this.arrow_right_BgSpin.graphicsData[0].shape.x = game.width - this.buttonWidth - game.scalingOffset;

        this.arrow_leftSpin.x = game.width - this.buttonWidth / 4.5 - game.scalingOffset - 440;
        this.arrow_left_BgSpin.graphicsData[0].shape.x = game.width - this.buttonWidth - game.scalingOffset - 365;

        this.lowerMenuBg[0].graphicsData[0].shape.x = game.scalingOffset;
        this.lowerMenuBg[0].graphicsData[0].shape.y = game.height - (this.greyBarBgHeight + 125);
        this.lowerMenuBg[1].graphicsData[0].shape.y = game.height - (this.greyBarBgHeight + 125);
        this.lowerMenuBg[2].graphicsData[0].shape.y = game.height - (this.greyBarBgHeight + 125);
        this.lowerMenuBg[3].graphicsData[0].shape.y = game.height - (this.greyBarBgHeight + 125);
        this.lowerMenuBg[4].graphicsData[0].shape.y = game.height - (this.greyBarBgHeight + 125);

        this.betLevelText.x = game.width - this.buttonWidth / 4.5 - game.scalingOffset - 270;
        this.betLevelText.y = this.rowHeight - 20;

        this.coinValueText.x = game.width - this.buttonWidth / 4.5 - game.scalingOffset - 270;
        this.coinValueText.y = this.rowHeight - 20;

        this.outSpinRatCounter.x = game.width - this.buttonWidth / 4.5 - game.scalingOffset - 270;
        this.outSpinRatCounter.y = this.rowHeight - 20;

        this.house_icon.x = game.scalingOffset + (this.rowHeight / 2);
        this.house_icon.y = game.height - (this.greyBarBgHeight + 65);
        this.sound.y = game.height - (this.greyBarBgHeight + 65);
        this.paytable.y = game.height - (this.greyBarBgHeight + 65);
        this.spinsettings.y = game.height - (this.greyBarBgHeight + 65);
        this.betsettings.y = game.height - (this.greyBarBgHeight + 65);
        this.maxBetBtn.x = game.width - game.scalingOffset - this.maxBetBtn.width / 2 - 50;
        this.autoSpinBtn.x = game.width - game.scalingOffset - this.autoSpinBtn.width / 2 - 50;
        this.maxBetText.x = this.maxBetBtn.x;
        this.autoSpinBtnText.x = this.autoSpinBtn.x;
    }
}