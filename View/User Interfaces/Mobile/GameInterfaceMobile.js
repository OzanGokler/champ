﻿GameInterfaceMobile = function () {
    vm.onGameScaled.add(this.repositionUi, this);

    Object.defineProperty(this, "IsSpinButtonActive", {
        get: function () { return (this.spinButton.inputEnabled && this.spinButton.input.enabled) || (this.stopButton.inputEnabled && this.stopButton.input.enabled); }
    });

    Object.defineProperty(this, "IsSpinButtonDragged", {
        get: function () {
            return this.spinButton.input.isDragged && this.spinButton.input.dragStartPoint.distance(this.spinButton.position) > 5;
        }
    });
}

GameInterfaceMobile.prototype.firstLayer = null;
GameInterfaceMobile.prototype.overlayLayer = null;
GameInterfaceMobile.prototype.isInitialized = false;
GameInterfaceMobile.prototype.spinButtonGroup = null;
GameInterfaceMobile.prototype.logo = null;
GameInterfaceMobile.prototype.spinButton = null;
GameInterfaceMobile.prototype.stopButton = null;
GameInterfaceMobile.prototype.autoSpinText = null;

GameInterfaceMobile.prototype.disabledAlpha = 0.1;
GameInterfaceMobile.prototype.spinLeft = 0.1;

GameInterfaceMobile.prototype.autoSpinTextStyle = { font: "30px Helvetica", fill: "#000000", wordWrap: false, align: "center" };

GameInterfaceMobile.prototype.positionUI = function () {
    if (!this.balanceUI) this.balanceUI = new BalanceUIMobile();
    this.overlayLayer = game.add.group();
    this.firstLayer = game.add.group();
    this.spinButtonGroup = game.add.group();

    this.logo = game.add.sprite(game.width / 2 + 130, 50, "gameLogo");
    this.logo.anchor.set(0.5);
    this.logo.scale.set(0.7);

    this.spinButton = game.add.sprite(0, game.height / 2, "icon-playbutton");
    this.spinButton.anchor.setTo(0.5);
    this.spinButton.scale.setTo(1);
    this.spinButton.x = game.width - game.scalingOffset - this.spinButton.width / 2 - 50;

    this.stopButton = game.add.sprite(this.spinButton.x, game.height / 2, "icon-stopbutton");
    this.stopButton.anchor.setTo(0.5);
    this.stopButton.scale.setTo(0.6);
    this.stopButton.visible = false;

    this.autoSpinText = game.add.text(this.spinButton.x, game.height / 2, "20", this.autoSpinTextStyle);
    this.autoSpinText.anchor.setTo(0.5, 0.5);
    this.autoSpinText.visible = false;


    this.spinButtonGroup.addMultiple([this.spinButton, this.stopButton, this.autoSpinText]);
    this.balanceUI.positionUI();
   
    this.overlayLayer.addMultiple([this.spinButtonGroup, this.balanceUI.layer]);

    this.isInitialized = true;
    this.repositionUi();

    this.balanceUI.balanceMenu.onAutoSpinToggled.add(this.onAutoSpinToggled, this);

    gm.autoSpinIndex = 1;
    gm.SpinsLeft = gm.getAutoSpinValue();
}


GameInterfaceMobile.prototype.onAutoSpinToggled = function () {
    gm.autoSpinEnabled = this.balanceUI.balanceMenu.autoSpinBtnClicked;
    this.updateSpinUI(gm.getAutoSpinValue());
}

GameInterfaceMobile.prototype.spinButtonDragUpdate = function () {
    var maxX = game.width - game.scalingOffset - this.spinButton.width / 2 - 50;
    var minX = game.scalingOffset + this.spinButton.width / 2 + 50;
    var minY = 100;
    var maxY = game.height - 200;

    if (this.spinButton.x > maxX) this.spinButton.x = maxX;
    else if (this.spinButton.x < minX) this.spinButton.x = minX;

    if (this.spinButton.y > maxY) this.spinButton.y = maxY;
    else if (this.spinButton.y < minY) this.spinButton.y = minY;

    this.stopButton.position.x = this.spinButton.position.x;
    this.stopButton.position.y = this.spinButton.position.y;
    this.autoSpinText.position.x = this.spinButton.position.x; // - 15;
    this.autoSpinText.position.y = this.spinButton.position.y;
}

GameInterfaceMobile.prototype.repositionUi = function () {
    if (!this.isInitialized) return;
    this.spinButtonDragUpdate();
}

GameInterfaceMobile.prototype.bringToTop = function () {
    if (this.firstLayer) game.world.bringToTop(this.firstLayer);
    if (this.overlayLayer) game.world.bringToTop(this.overlayLayer);
}

GameInterfaceMobile.prototype.toggleMuteButton = function (isMuted) {
    if (!isMuted) {
        this.balanceUI.balanceMenu.onSoundIsDown(false);
    }
    else {
        this.balanceUI.balanceMenu.onSoundIsDown(true);
    }
}

GameInterfaceMobile.prototype.showPayTable = function () {
    //Todo: 1- Abud review my changes, try to understand why I made them ;) Ask if you don't but try hard first :)
    this.toggleAllButtons(false);
    this.balanceUI.toggleMenu(false);
    payTableBoard.show();}




GameInterfaceMobile.prototype.updateSpinUI = function (spinsLeft) {
    console.log("updateSpinUI: " + spinsLeft + ":" + gm.CanSkip);
    console.log(gm.CurrentSpinState);
    this.spinLeft = spinsLeft;
    this.toggleAutoSpin(true);

    if (!spinsLeft) {
        //Autospin
        
        this.spinButton.visible = true;
        this.stopButton.visible = false;
        this.autoSpinText.setText(spinsLeft);
        if (gm.CurrentSpinState === gm.spinStates.idle ||
            gm.CurrentSpinState === gm.spinStates.spinned) {
            game.tweens.removeFrom(this.spinButtonGroup, false);
            game.add.tween(this.spinButtonGroup).to({ alpha: this.IsSpinButtonActive ? 0.75 : this.disabledAlpha }, 250, Phaser.Easing.Linear.In, true);
        }
    }
    else if (spinsLeft === -1) {
        //Infinite

        this.spinButton.visible = !gm.isAutoSpinning;
        this.stopButton.visible = gm.isAutoSpinning;
        this.autoSpinText.setText("8");

        if (!gm.isAutoSpinning) {
            game.tweens.removeFrom(this.spinButtonGroup, false);
            game.add.tween(this.spinButtonGroup).to({ alpha: this.IsSpinButtonActive ? 0.75 : this.disabledAlpha }, 250, Phaser.Easing.Linear.In, true);
        }
    }
    else {
        //Some other numer of autospins.
        console.log("SPIN LEFT++++" + spinsLeft);
        console.log("SPIN LEFTaaa++++" + gm.isAutoSpinning);
        this.spinButton.visible = !gm.isAutoSpinning;
        this.stopButton.visible = gm.isAutoSpinning;
        this.autoSpinText.setText(spinsLeft);

        if (gm.isAutoSpinning) {
            game.tweens.removeFrom(this.spinButtonGroup, false);
            game.add.tween(this.spinButtonGroup).to({ alpha: this.IsSpinButtonActive ? 1 : this.disabledAlpha }, 250, Phaser.Easing.Linear.In, true);
        }
        else {
            if (gm.CanSkip) {
                game.tweens.removeFrom(this.spinButtonGroup, false);
                game.add.tween(this.spinButtonGroup).to({ alpha: this.IsSpinButtonActive ? 0.75 : this.disabledAlpha }, 250, Phaser.Easing.Linear.In, true);
            }
        }
    }
    this.autoSpinText.visible = gm.autoSpinEnabled && spinsLeft > 0;
    if (gm.autoSpinIndex === 0) gm.autoSpinIndex = 1;
    this.balanceUI.updateSpinValue(gm.getAutoSpinValue());
}

GameInterfaceMobile.prototype.updateBetUI = function (betLevel, winLines, coinValue) {
    this.balanceUI.updateBetUI(betLevel, winLines, coinValue);
}

GameInterfaceMobile.prototype.toggleSpinButton = function (activate, animate, visualOnly) {
    this.toggleButton(this.spinButtonGroup, activate, animate, visualOnly);
}

GameInterfaceMobile.prototype.toggleFullScreenButton = function (activate, animate, visualOnly) {
    this.toggleButton(this.balanceUI.fullscreenBtn, game.scale.compatibility.supportsFullScreen && activate, animate, visualOnly);
    this.balanceUI.toggleFullScreenButton(game.scale.isFullScreen);
}

GameInterfaceMobile.prototype.toggleAllButtons = function (activate, animate, visualOnly) {
    animate = animate === undefined ? true : animate;

    this.toggleSpinButtons(activate, animate, visualOnly);

    this.toggleButton(this.balanceUI.balanceMenu.maxBetText, activate, animate, visualOnly);
    this.toggleButton(this.balanceUI.balanceMenu.maxBetBtn, activate, animate, visualOnly);
    if (!activate) {
        this.balanceUI.balanceMenu.onMaxBetUp();
    }
    this.toggleBetButtons(activate, animate, visualOnly);
    this.balanceUI.toggleMenuButton(activate, animate, visualOnly);
    this.balanceUI.toggleBalanceMenuButtons(activate, animate, visualOnly);
    this.toggleFullScreenButton(activate, animate, visualOnly);
}

GameInterfaceMobile.prototype.toggleButton = function (button, activate, animate, visualOnly) {
    game.tweens.removeFrom(button, false);
    animate = animate === undefined ? true : animate;
    var targetAlpha = activate ? 1 : this.disabledAlpha;
    if (animate) game.add.tween(button).to({ alpha: targetAlpha }, 250, Phaser.Easing.Linear.In, true);
    else button.alpha = targetAlpha;
    if (!visualOnly && button.input) {
        if (button instanceof Phaser.Group) {
            button.forEach(function (target) { target.input.enabled = this; }, activate);
            button.input.enabled = activate; //To be able to properly check its status without loooping the children.
        }
        else button.input.enabled = activate;
    }
}

GameInterfaceMobile.prototype.toggleBetButtons = function (activate, animate, visualOnly) {
    //Bet Level
    var tempBool = activate && gameUI.betLevelMax !== undefined && gm.betLevelValue < gameUI.betLevelMax;
    this.toggleButton(this.balanceUI.balanceMenu.arrow_right_Bg[0], tempBool, animate, visualOnly);
    this.toggleButton(this.balanceUI.balanceMenu.arrow_right[0], tempBool, animate, visualOnly);
    tempBool = activate && gameUI.betLevelMax !== undefined && gm.betLevelValue > 1;
    this.toggleButton(this.balanceUI.balanceMenu.arrow_left_Bg[0], tempBool, animate, visualOnly);
    this.toggleButton(this.balanceUI.balanceMenu.arrow_left[0], tempBool, animate, visualOnly);

    //Coin
    tempBool = activate && gm.coinValues !== null && gm.coinValueIndex < gm.coinValues.length - 1;
    this.toggleButton(this.balanceUI.balanceMenu.arrow_right_Bg[1], tempBool, animate, visualOnly);
    this.toggleButton(this.balanceUI.balanceMenu.arrow_right[1], this.balanceUI.balanceMenu.arrow_right_Bg[1].input.enabled, animate, visualOnly);
    tempBool = activate && gm.coinValues !== null && gm.coinValueIndex > 0;
    this.toggleButton(this.balanceUI.balanceMenu.arrow_left_Bg[1], tempBool, animate, visualOnly);
    this.toggleButton(this.balanceUI.balanceMenu.arrow_left[1], this.balanceUI.balanceMenu.arrow_left_Bg[1].input.enabled, animate, visualOnly);
}

GameInterfaceMobile.prototype.toggleSpinButtons = function (activate, animate, visualOnly) {
    //Bet UI Buttons
    this.toggleSpinButton(activate, animate, visualOnly);
    this.toggleAutoSpin(activate, animate, visualOnly);
}
GameInterfaceMobile.prototype.toggleAutoSpin = function (activate, animate, visualOnly) {
    var tempBool = activate && gm.autoSpinValues !== null && gm.autoSpinIndex < gm.autoSpinValues.length - 1;
    this.toggleButton(this.balanceUI.balanceMenu.arrow_right_BgSpin, tempBool, animate, visualOnly);
    this.toggleButton(this.balanceUI.balanceMenu.arrow_rightSpin, tempBool, animate, visualOnly);

    tempBool = activate && gm.autoSpinValues !== null && gm.autoSpinIndex > 1;
    this.toggleButton(this.balanceUI.balanceMenu.arrow_left_BgSpin, tempBool, animate, visualOnly);
    this.toggleButton(this.balanceUI.balanceMenu.arrow_leftSpin, tempBool, animate, visualOnly);
}

//addMuteClickEvent
GameInterfaceMobile.prototype.addMuteClickEvent = function (enabled, callback, context) {
    this.initializeInput(this.balanceUI.balanceMenu.sound, enabled, callback, context,true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.sound);
}

//addPayTableClickEvent here
GameInterfaceMobile.prototype.addPayTableClickEvent = function (enabled, callback, context) {
    this.initializeInput(this.balanceUI.balanceMenu.paytable, enabled, callback, context,true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.paytable);
}

GameInterfaceMobile.prototype.addFullScreenClickEvent = function (enabled, callback, context) {
    this.initializeInput(this.balanceUI.fullscreenBtn, enabled, callback, context,true);
    //audioManager.addClickSound(this.balanceUI.fullscreenBtn);

    this.toggleFullScreenButton(game.scale.compatibility.supportsFullScreen);
}

//addAutoSpinClickEvent
GameInterfaceMobile.prototype.addAutoSpinClickEvent = function (enabled, callback, context) {
    this.initializeInput(this.autoSpinText, enabled, callback, context, true);
}
//addAutoSpinIncreaseButtonEvent
GameInterfaceMobile.prototype.addAutoSpinIncreaseButtonEvent = function (enabled, callback, context) {
    this.initializeInput(this.balanceUI.balanceMenu.arrow_right_BgSpin, enabled, callback, context,true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_right_BgSpin);

    this.initializeInput(this.balanceUI.balanceMenu.arrow_rightSpin, enabled, callback, context,true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_rightSpin);
}
//addAutoSpinDecreaseButtonEvent
GameInterfaceMobile.prototype.addAutoSpinDecreaseButtonEvent = function (enabled, callback, context) {
    this.initializeInput(this.balanceUI.balanceMenu.arrow_left_BgSpin, enabled, callback, context,true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_left_BgSpin);

    this.initializeInput(this.balanceUI.balanceMenu.arrow_leftSpin, enabled, callback, context,true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_leftSpin);
}


//addBetMaxClickEvent
GameInterfaceMobile.prototype.addBetMaxClickEvent = function (enabled, callback, context) {
    this.initializeInput(this.balanceUI.balanceMenu.maxBetBtn, enabled, callback, context, true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.maxBetBtn);

    this.initializeInput(this.balanceUI.balanceMenu.maxBetText, enabled, callback, context, true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.maxBetText);

    this.balanceUI.balanceMenu.setMaxBetEvents();
}
//addSpinClickEvent
GameInterfaceMobile.prototype.addSpinClickEvent = function (enabled, callback, context) {
    this.initializeInput(this.spinButtonGroup, enabled, callback, context, true);
    //audioManager.addClickSound(this.spinButtonGroup);

    this.spinButton.input.enableDrag(false, false);
    this.spinButton.events.onDragUpdate.add(this.spinButtonDragUpdate, this);
}
//addBetLevelIncreaseButtonEvent
GameInterfaceMobile.prototype.addBetLevelIncreaseButtonEvent = function (enabled, callback, context) {
    this.initializeInput(this.balanceUI.balanceMenu.arrow_right_Bg[0], enabled, callback, context, true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_right_Bg[0]);

    this.initializeInput(this.balanceUI.balanceMenu.arrow_right[0], enabled, callback, context, true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_right[0]);
}
//addBetLevelDecreaseButtonEvent
GameInterfaceMobile.prototype.addBetLevelDecreaseButtonEvent = function (enabled, callback, context) {
    this.initializeInput(this.balanceUI.balanceMenu.arrow_left_Bg[0], enabled, callback, context, true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_left_Bg[0]);

    this.initializeInput(this.balanceUI.balanceMenu.arrow_left[0], enabled, callback, context, true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_left[0]);
}
////addBetWinLinesIncreaseButtonEvent
//GameInterfaceMobile.prototype.addBetWinLinesIncreaseButtonEvent = function (enabled, callback, context)
//{
//    this.initializeInput(this.balanceUI.balanceMenu.arrow_right_Bg[1], enabled, callback, context);
//    audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_right_Bg[1]);
//}
////addBetWinLinesDecreaseButtonEvent
//GameInterfaceMobile.prototype.addBetWinLinesDecreaseButtonEvent = function (enabled, callback, context)
//{
//    this.initializeInput(this.balanceUI.balanceMenu.arrow_left_Bg[1], enabled, callback, context);
//    audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_left_Bg[1]);
//}
//addCoinIncreaseButtonEvent
GameInterfaceMobile.prototype.addCoinIncreaseButtonEvent = function (enabled, callback, context) {
    this.initializeInput(this.balanceUI.balanceMenu.arrow_right_Bg[1], enabled, callback, context, true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_right_Bg[1]);

    this.initializeInput(this.balanceUI.balanceMenu.arrow_right[1], enabled, callback, context, true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_right[1]);
}
//addCoinDecreaseButtonEvent
GameInterfaceMobile.prototype.addCoinDecreaseButtonEvent = function (enabled, callback, context) {
    this.initializeInput(this.balanceUI.balanceMenu.arrow_left_Bg[1], enabled, callback, context, true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_left_Bg[1]);

    this.initializeInput(this.balanceUI.balanceMenu.arrow_left[1], enabled, callback, context, true);
    //audioManager.addClickSound(this.balanceUI.balanceMenu.arrow_left[1]);
}

//initializeInput
//GameInterfaceMobile.prototype.initializeInput = function (item, enabled, method, context) {
//    if (item instanceof Phaser.Group) {
//        item.forEach(function (target) { this.context.initializeInput(target, this.enabled, this.callbackMethod, this.callbackContext); },
//        { context: this, enabled: enabled, callbackMethod: method, callbackContext: context });
//    }
//    else {
//        item.inputEnabled = enabled;
//        item.events.onInputUp.removeAll();
//        item.events.onInputDown.removeAll();
//        item.events.onInputUp.add(method, context);
//    }
//},

GameInterfaceMobile.prototype.initializeInput = function (item, enabled, method, context, hasSound) {
    if (item instanceof Phaser.Group) {
        item.forEach(function (target) { this.context.initializeInput(target, this.enabled, this.callbackMethod, this.callbackContext); },
        { context: this, enabled: enabled, callbackMethod: method, callbackContext: context });
    }
    else {
        item.inputEnabled = enabled;
        item.events.onInputUp.removeAll();
        item.events.onInputDown.removeAll();
        item.events.onInputUp.add(method, context);
        if (hasSound) audioManager.addClickUpDownSound(item);
    }
}
GameInterfaceMobile.prototype.updateBalanceUI = function (updateMoneyWon) {
        updateMoneyWon = updateMoneyWon === undefined ? true : updateMoneyWon;
        this.balanceUI.updateBalanceUI(gm.CurrentRealMoney, gm.currentCredits, gm.TotalBetInRealMoney, updateMoneyWon ? gm.CumulativeRealMoneyWon : undefined);
}

GameInterfaceMobile.prototype.showCoinWin = function (data, delay) {
    this.balanceUI.showCoinWin(data, delay);
}

GameInterfaceMobile.prototype.onSpinStopClicked = function () {

}

GameInterfaceMobile.prototype.turnAnimPlay = function () {

}
GameInterfaceMobile.prototype.decreaseCoinBetButton = function () {
    this.toggleButton(this.balanceUI.balanceMenu.arrow_right[0], gm.coinValueIndex > 0, true);
}
GameInterfaceMobile.prototype.Mute = function () {
    this.balanceUI.balanceMenu.sound.frame = game.sound.mute ? 0 : 1;
}


GameInterfaceMobile.prototype.hideBtnSpin = function () {
    this.spinButton.visible = false;
}