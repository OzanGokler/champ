﻿GameInterfaceDesktop = function () {
    Object.defineProperty(this, "IsSpinButtonActive", {
        get: function () { return this.btnSpinStop.inputEnabled && this.btnSpinStop.input.enabled; }
    });
}

GameInterfaceDesktop.prototype.btnLevelText = null;
GameInterfaceDesktop.prototype.coinText = null;
GameInterfaceDesktop.prototype.autoSpinText = null;
GameInterfaceDesktop.prototype.levelText = null;
GameInterfaceDesktop.prototype.coinValueText = null;
GameInterfaceDesktop.prototype.btnPaytable = null;
GameInterfaceDesktop.prototype.btnBetLevelDecrease = null;
GameInterfaceDesktop.prototype.btnBetLevelIncrease = null;
GameInterfaceDesktop.prototype.btnCoinDecrease = null;
GameInterfaceDesktop.prototype.btnCoinIncrease = null;
GameInterfaceDesktop.prototype.betUiGroup = null;
GameInterfaceDesktop.prototype.btnAudio = null;
GameInterfaceDesktop.prototype.btnSpinStop = null;
GameInterfaceDesktop.prototype.betLevelText = null;
GameInterfaceDesktop.prototype.uiGroup = null;
GameInterfaceDesktop.prototype.winLinesText = null;
GameInterfaceDesktop.prototype.btnBetMax = null;
GameInterfaceDesktop.prototype.needsRestore = false;
GameInterfaceDesktop.prototype.interface_bg = null;
GameInterfaceDesktop.prototype.turnIcon = null;
GameInterfaceDesktop.prototype.turnIconAnim = null;
GameInterfaceDesktop.prototype.btnWinLinesIncrease = null;
GameInterfaceDesktop.prototype.btnWinLinesDecrease = null;
GameInterfaceDesktop.prototype.balanceUI = null;
GameInterfaceDesktop.prototype.fistLayer = null;
GameInterfaceDesktop.prototype.overlayLayer = null;




GameInterfaceDesktop.prototype.positionUI=function() {
    var uiScale = 0.85;
    if (!this.balanceUI) this.balanceUI = new BalanceUI();
    this.interface_bg = game.add.sprite(game.width / 2 + 20, game.height / 2 + 285, "interface");
    this.interface_bg.anchor.setTo(0.5);
    this.interface_bg.scale.setTo(0.8);

    this.btnPaytable = game.add.bitmapText(319, 605, "badaboomAutospin", languageManager.langJSON.payTable, 28);
    this.btnPaytable.anchor.setTo(0.5);

    this.betLevelText = game.add.bitmapText(407, 608, "badaboomAutospin", "", 64);
    this.betLevelText.scale.setTo(0.6);
    this.betLevelText.anchor.set(0.5);

    this.coinText = game.add.bitmapText(753, 607, "badaboomAutospin", "", 60);
    this.coinText.scale.setTo(0.6);
    this.coinText.anchor.set(0.5);

    this.btnBetLevelIncrease = game.add.sprite(this.betLevelText.x + 40, this.betLevelText.y, "arrow_right");
    this.btnBetLevelIncrease.anchor.set(0.5);
    this.btnBetLevelIncrease.scale.setTo(uiScale);

    this.btnBetMax = game.add.bitmapText(880, 605, "badaboomAutospin", languageManager.langJSON.maxBet, 30);
    this.btnBetMax.anchor.setTo(0.5);

    this.btnBetLevelDecrease = game.add.sprite(this.betLevelText.x - 33, this.betLevelText.y, "arrow_left");
    this.btnBetLevelDecrease.anchor.set(0.5);
    this.btnBetLevelDecrease.scale.setTo(0.75);

    this.btnCoinDecrease = game.add.sprite(this.coinText.x - 63, this.coinText.y, "arrow_left");
    this.btnCoinDecrease.anchor.set(0.5);
    this.btnCoinDecrease.scale.setTo(0.7);

    this.btnCoinIncrease = game.add.sprite(this.coinText.x + 73, this.coinText.y, "arrow_right");
    this.btnCoinIncrease.anchor.setTo(0.5);

    //this.btnWinLinesIncrease = game.add.bitmapText(316, 170, "chalkFont", "+", 53);
    //this.btnWinLinesIncrease.anchor.set(0.5);

    //this.btnWinLinesDecrease = game.add.bitmapText(196, 175, "chalkFont", "-", 53);
    //this.btnWinLinesDecrease.anchor.set(0.5);

    this.betUiGroup = game.add.group();

    this.betUiGroup.addMultiple([
        this.btnPaytable,
        this.betLevelText,
        this.btnBetLevelIncrease,
        this.btnBetLevelDecrease,
        this.coinText,
        this.btnCoinIncrease,
        this.btnCoinDecrease
    ]);

    this.levelText = game.add.bitmapText(410, 573, "badaboomInterface", languageManager.langJSON.level, 24);
    this.levelText.anchor.set(0.5);
    this.levelText.align = "center";
    this.levelText.angle = 5;

    this.coinValueText = game.add.bitmapText(753, 573, "badaboomInterface", languageManager.langJSON.coinValue, 24);;
    this.coinValueText.anchor.set(0.5);
    this.coinValueText.align = "center";
    this.coinValueText.angle = -5;

    //this.winLinesText = game.add.bitmapText(255, 175, "chalkFont", "", 60);
    //this.winLinesText.anchor.set(0.5);

    this.autoSpinText = game.add.bitmapText(615, 608, "badaboomAutospin", languageManager.langJSON.autoSpin, 24);
    this.autoSpinText.anchor.set(0.5);
    this.autoSpinText.align = "center";

    this.btnAudio = game.add.sprite(955, 605, "audioButton");
    this.btnAudio.scale.setTo(0.7);
    this.btnAudio.anchor.setTo(0.5);
    this.btnAudio.frame = game.sound.mute ? 0 : 1;


    this.btnSpinStop = game.add.bitmapText(535, 605, "badaboomAutospin", languageManager.langJSON.spin, 48);
    this.btnSpinStop.anchor.set(0.5);

    this.turnIcon = game.add.sprite(618, 607, "turnIcon");
    this.turnIcon.frame = 0;
    this.turnIcon.anchor.setTo(0.5);
    this.turnIconAnim = this.turnIcon.animations.add("turnIconAnim");
    this.turnIconAnim.onComplete.addOnce(function () {
        this.turnIcon.frame = 0;
    }, this);

    this.uiGroup = game.add.group();
    this.uiGroup.add(this.interface_bg);
    this.uiGroup.add(this.btnSpinStop);
    this.uiGroup.add(this.btnBetMax);
    this.uiGroup.add(this.turnIcon);
    this.uiGroup.add(this.btnAudio);
    this.uiGroup.add(this.autoSpinText);
    this.uiGroup.add(this.levelText);
    this.uiGroup.add(this.coinValueText);
    this.firstLayer = game.add.group();

    this.uiGroup.position.y += 5;
    this.balanceUI.positionUI();
    
    this.overlayLayer = game.add.group();

    gm.autoSpinEnabled = true;
}

GameInterfaceDesktop.prototype.addFullScreenClickEvent = function (enabled, callback, context) {
}

//addPayTableClickEvent
GameInterfaceDesktop.prototype.addPayTableClickEvent = function (enabled, callback, context) {
    this.initializeInput(this.btnPaytable, enabled, callback, context, true);

}
//addMuteClickEvent
GameInterfaceDesktop.prototype.addMuteClickEvent = function (enabled, callback, context) {
    this.initializeInput(this.btnAudio, enabled, callback, context, true);

}
//addAutoSpinClickEvent
GameInterfaceDesktop.prototype.addAutoSpinClickEvent = function (enabled, callback, context) {
    this.initializeInput(this.autoSpinText, enabled, callback, context, true);

}

GameInterfaceDesktop.prototype.addAutoSpinIncreaseButtonEvent = function(enabled, callback, context) {
}

GameInterfaceDesktop.prototype.addAutoSpinDecreaseButtonEvent = function(enabled, callback, context) {
}

GameInterfaceDesktop.prototype.addBetMaxClickEvent = function (enabled, callback, context) {
    console.log("btnBetMax");
    this.initializeInput(this.btnBetMax, enabled, callback, context, true);

}
//addSpinClickEvent
GameInterfaceDesktop.prototype.addSpinClickEvent = function (enabled, callback, context) {
    console.log("btnspin");
    this.initializeInput(this.btnSpinStop, enabled, callback, context, true);
    this.btnSpinStop.events.onInputDown.add(this.onSpinStopClicked,this);

}
//addBetLevelIncreaseButtonEvent
GameInterfaceDesktop.prototype.addBetLevelIncreaseButtonEvent = function (enabled, callback, context) {
    console.log("btnBetLevelIncrease");
    this.initializeInput(this.btnBetLevelIncrease, enabled, callback, context, true);

}
//addBetLevelDecreaseButtonEvent
GameInterfaceDesktop.prototype.addBetLevelDecreaseButtonEvent = function (enabled, callback, context) {
    console.log("btnBetLevelDecrease");
    this.initializeInput(this.btnBetLevelDecrease, enabled, callback, context, true);

}

//addCoinIncreaseButtonEvent
GameInterfaceDesktop.prototype.addCoinIncreaseButtonEvent = function (enabled, callback, context) {
    console.log("btnCoinIncrease");
    this.initializeInput(this.btnCoinIncrease, enabled, callback, context, true);

}
//addCoinDecreaseButtonEvent
GameInterfaceDesktop.prototype.addCoinDecreaseButtonEvent = function (enabled, callback, context) {
    console.log("btnCoinDecrease");
    this.initializeInput(this.btnCoinDecrease, enabled, callback, context,true);

}

GameInterfaceDesktop.prototype.initializeInput = function (item, enabled, method, context, hasSound) {
    item.inputEnabled = enabled;
    item.events.onInputUp.removeAll();
    item.events.onInputDown.removeAll();
    item.events.onInputUp.add(method, context);
    if (hasSound) audioManager.addClickUpDownSound(item);
}



GameInterfaceDesktop.prototype.updateSpinUI= function (value)
{
    debugUtils.log("GameUI - updateSpinUI");
    
    if (value === 0)
    {
        //this.btnAutospinMinus.visible = false;
        //this.btnAutospinPlus.visible = false;
        this.autoSpinText.fontSize = 22;
        this.autoSpinText.setText(languageManager.langJSON.autoSpin);
        this.btnSpinStop.setText(languageManager.langJSON.spin);

        this.btnSpinStop.setText(
            gm.CurrentSpinState === gm.spinStates.idle ||
            gm.CurrentSpinState === gm.spinStates.spinned ?
            languageManager.langJSON.spin : languageManager.langJSON.skip);
    }
    else if (value === -1)
    {
        this.btnSpinStop.setText(gm.isAutoSpinning ? languageManager.langJSON.stop : languageManager.langJSON.spin);
        this.autoSpinText.fontSize = 13;
        this.autoSpinText.setText(languageManager.langJSON.infinite);
        //this.btnAutospinPlus.visible = false;
    }
    else
    {
        this.btnSpinStop.setText(gm.isAutoSpinning ? languageManager.langJSON.stop : languageManager.langJSON.spin);
        //this.btnAutospinMinus.visible = true;
        //this.btnAutospinPlus.visible = true;
        this.autoSpinText.setText(value);
        this.autoSpinText.fontSize = 45;
    }
    gm.IsAutoSpinEnabled = gm.autoSpinIndex > 0;
}

GameInterfaceDesktop.prototype.updateBetUI = function (betLevel, winLines, coinValue) {
    debugUtils.log("GameUI - updateBetUI");
    this.betLevelText.setText(betLevel);
    //this.winLinesText.setText(winLines);
    this.coinText.setText(coinValue.toFixed(2));
}

GameInterfaceDesktop.prototype.toggleButton = function (button, activate, animate, visualOnly) {
    game.tweens.removeFrom(button, false);
    animate = animate === undefined ? true : animate;

    //TODO: Uncomment here if we ant to have button transaprency back.
    //var targetAlpha = activate ? 1 : 0.15;
    //if (animate) game.add.tween(button).to({ alpha: targetAlpha }, 250, Phaser.Easing.Linear.In, true);
    //else button.alpha = targetAlpha;

    if (!visualOnly) button.inputEnabled = activate;
}

GameInterfaceDesktop.prototype.toggleSpinButton = function (activate, animate, visualOnly) {
    console.log("toggleSpinButton: " + activate + ":" + animate + ":" + visualOnly);
    this.toggleButton(this.btnSpinStop, activate, animate, visualOnly);
}

GameInterfaceDesktop.prototype.toggleSpinButtons = function (activate, animate, visualOnly) {
    console.log("cont2: " + activate + ":" + animate + ":" + visualOnly);
    //Bet UI Buttons
    this.toggleSpinButton(activate, animate, visualOnly);
    this.toggleButton(this.btnBetMax, activate, animate, visualOnly);
    this.toggleButton(this.autoSpinText, activate, animate, visualOnly);

}

GameInterfaceDesktop.prototype.toggleAllButtons= function (activate, animate, visualOnly)
{
    animate = animate === undefined ? true : animate;

    //this.toggleBetUiButtons(activate, animate, visualOnly);
    this.toggleSpinButtons(activate, animate.visualOnly);
    this.toggleButton(this.betLevelText, activate, animate, visualOnly);
    this.toggleButton(this.coinText, activate, animate, visualOnly);
    //this.toggleButton(this.winLinesText, activate, animate, visualOnly);
    this.toggleButton(this.btnPaytable, activate, animate, visualOnly);
    //this.toggleButton(this.btnAudio, activate, animate, visualOnly);
    this.toggleBetButtons(activate, animate, visualOnly);
    this.toggleBetMaxButton(activate, animate, visualOnly);

    ////Todo: bunlar kaldırılcak
    //this.btnWinLinesIncrease.visible = false;
    //this.btnWinLinesDecrease.visible = false;
    ////this.autoSpinText.visible = false;
    //this.winLinesText.visible = false;
}

GameInterfaceDesktop.prototype.toggleBetUiButtons= function (activate, animate, visualOnly)
{
    activate = activate;

    //Bet UI Buttons
    this.toggleButton(this.btnSpinStop, activate, animate, visualOnly);
    this.toggleButton(this.btnBetMax, activate, animate, visualOnly);
    this.toggleButton(this.autoSpinText, activate, animate, visualOnly);
    //this.toggleButton(this.btnAutospinMinus, activate, animate, visualOnly);
    //this.toggleButton(this.btnAutospinPlus, activate, animate, visualOnly);
}

GameInterfaceDesktop.prototype.toggleBetMaxButton= function (activate, animate, visualOnly)
{
    this.toggleButton(this.btnBetMax, activate, animate, visualOnly);
}

// Update bet buttons
GameInterfaceDesktop.prototype.toggleBetButtons= function (activate, animate, visualOnly)
{
    this.toggleButton(this.btnBetLevelIncrease, activate && gameUI.betLevelMax !== undefined && gm.betLevelValue < gameUI.betLevelMax, animate, visualOnly);
    //this.toggleButton(this.btnWinLinesIncrease, activate && this.winLinesMax !== undefined && gm.winLinesValue < this.winLinesMax, animate, visualOnly);
    this.toggleButton(this.btnCoinIncrease, activate && gm.coinValues !== null && gm.coinValueIndex < gm.coinValues.length - 1, animate, visualOnly);

    this.toggleButton(this.btnBetLevelDecrease, activate && gameUI.betLevelMax !== undefined && gm.betLevelValue > 1, animate, visualOnly);
    //this.toggleButton(this.btnWinLinesDecrease, activate && this.winLinesMax !== undefined && gm.winLinesValue > 1, animate, visualOnly);
    this.toggleButton(this.btnCoinDecrease, activate && gm.coinValues !== null && gm.coinValueIndex > 0, animate, visualOnly);
}

GameInterfaceDesktop.prototype.showPayTable = function () {
    this.btnPaytable.frame = 0;
    this.needsRestore = true;
    payTableBoard.show();
}

GameInterfaceDesktop.prototype.updateBalanceUI = function (updateMoneyWon) {
    updateMoneyWon = updateMoneyWon === undefined ? true : updateMoneyWon;
    this.balanceUI.updateBalanceUI(gm.CurrentRealMoney, gm.currentCredits, gm.TotalBetInRealMoney, updateMoneyWon ? gm.CumulativeRealMoneyWon : undefined);
    console.trace();
}

GameInterfaceDesktop.prototype.onSpinStopClicked = function() {
    game.add.tween(this.btnSpinStop.scale).to({ x: 0.75, y: 0.75 }, 150, Phaser.Easing.Linear.In, true, 0, 0, true);
}

GameInterfaceDesktop.prototype.turnAnimPlay = function() {
    this.turnIconAnim.play();
}

GameInterfaceDesktop.prototype.decreaseCoinBetButton=function() {
    this.toggleButton(this.btnCoinDecrease, gm.coinValueIndex > 0, true);
}

GameInterfaceDesktop.prototype.Mute=function() {
    this.btnAudio.frame = game.sound.mute ? 0 : 1;
}

GameInterfaceDesktop.prototype.hideBtnSpin=function() {
    
}

