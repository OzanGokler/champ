﻿
function TextureOverlay() { }

TextureOverlay.prototype = {
    fadeSprite: null,
    defaultSpriteKey: "whiteOverlay",

    fade: function (fadeColor, fadeSpriteName, targetAlpha, fromAlpha, speed, start) {
        speed = speed === undefined ? 500 : speed;
        start = start === undefined ? true : start;

        this.loadFadeSprite(fadeSpriteName ? fadeSpriteName : this.defaultSpriteKey, fromAlpha);
        this.fadeSprite.inputEnabled = true;
        var fadeTween = game.add.tween(this.fadeSprite);
        this.initParams(fadeColor);
        fadeTween.to({ alpha: targetAlpha }, speed, Phaser.Easing.Linear.In, start).onComplete.addOnce(function () {
            if (targetAlpha === 0) this.fadeSprite.visible = false;
        }, this);
        return fadeTween;
    },

    initParams: function (fadeColor) {
        this.fadeSprite.width = game.width;
        this.fadeSprite.height = game.height;
        this.fadeSprite.tint = fadeColor;
        this.fadeSprite.visible = true;
        game.world.bringToTop(this.fadeSprite);
    },

    loadFadeSprite: function (fadeSpriteName, fromAlpha) {
        if (!this.fadeSprite) {
            this.fadeSprite = game.add.sprite(0, 0, fadeSpriteName);
            if (fromAlpha != undefined) this.fadeSprite.alpha = fromAlpha;
            else this.fadeSprite.alpha = 0;
        }
        else {
            //Recover alpha value if needed.
            var newAlpha;
            if (fromAlpha != undefined) newAlpha = fromAlpha;
            else newAlpha = this.fadeSprite.alpha;
            //Re-add item to the scene if cleared. (It might be cleaned while changing scenes.)
            if (!this.fadeSprite.game) {
                this.fadeSprite.destroy();
                this.fadeSprite = game.add.sprite(0, 0, fadeSpriteName);
            }

            //Only load texture if needed.
            if (fadeSpriteName !== this.fadeSprite.key) this.fadeSprite.loadTexture(fadeSpriteName);

            //Apply recovered alpha if it exists.
            this.fadeSprite.alpha = newAlpha;
        }
    }
}

textureOverlay = new TextureOverlay();