﻿var BalanceUI = function () { };

BalanceUI.prototype = {
    cashValueText: null,
    coinsValueText: null,
    cashBetValueText: null,
    cashWinValueText: null,

    cashText: null,
    coinsText: null,
    cashBetText: null,
    cashWinText: null,

    cashValue: 0,
    coinsValue: 0,
    cashBetValue: 0,
    cashWinValue: 0,

    cashValueProgress: { step: 0 },
    coinsValueProgress: { step: 0 },
    cashBetValueProgress: { step: 0 },
    cashWinValueProgress: { step: 0 },
    groupLayout: new GroupHorizontalLayout(175, 200),
    layer: null,
    blackBarTopBg: null,
    blackBarBg: null,
    balanceBarHeight: 30,
    textGroup: null,

    groupLayout: new GroupHorizontalLayout(175, 200),

    decimalCount: 2,
    style: { font: "14px Helvetica", fill: "#ffffff", wordWrap: false },

    positionUI: function () {
        if (!this.layer || !this.layer.game) this.layer = game.add.group();
        if (!this.textGroup || !this.textGroup.game) this.textGroup = game.add.group();
        //No need to recreate stuff, just return.
        if (this.blackBarTopBg && this.blackBarTopBg.game) return;

        this.blackBarTopBg = game.add.graphics(0, 0);
        this.blackBarTopBg.beginFill(0x282828);
        this.blackBarTopBg.drawRect(0, game.height - this.balanceBarHeight, game.width, this.balanceBarHeight / 2);

        this.blackBarBg = game.add.graphics(0, 0);
        this.blackBarBg.beginFill(0x212121);
        this.blackBarBg.drawRect(0, game.height - this.balanceBarHeight / 2, game.width, this.balanceBarHeight / 2);

        var textY = (game.height - this.balanceBarHeight / 2) + 1;

        this.cashText = game.add.text(0, textY, languageManager.langJSON.balanceUICash, this.style);
        this.cashText.anchor.setTo(0.5, 0.5);
        this.coinsText = game.add.text(0, textY, languageManager.langJSON.balanceUICoins, this.style);
        this.coinsText.anchor.setTo(0.5, 0.5);
        this.cashBetText = game.add.text(0, textY, languageManager.langJSON.balanceUIBet, this.style);
        this.cashBetText.anchor.setTo(0.5, 0.5);
        this.cashWinText = game.add.text(0, textY, languageManager.langJSON.balanceUIWin, this.style);
        this.cashWinText.anchor.setTo(0.5, 0.5);

        this.cashValueText = game.add.text(0, textY, this.cashValue.numberFormat(this.decimalCount), this.style);
        this.cashValueText.anchor.setTo(0, 0.5);
        this.coinsValueText = game.add.text(0, textY, this.coinsValue.numberFormat(this.decimalCount), this.style);
        this.coinsValueText.anchor.setTo(0, 0.5);
        this.cashBetValueText = game.add.text(0, textY, this.cashBetValue.numberFormat(this.decimalCount), this.style);
        this.cashBetValueText.anchor.setTo(0, 0.5);
        this.cashWinValueText = game.add.text(0, textY, this.cashWinValue.numberFormat(this.decimalCount), this.style);
        this.cashWinValueText.anchor.setTo(0, 0.5);


        this.textGroup.add(this.cashText);
        this.textGroup.add(this.coinsText);
        this.textGroup.add(this.cashBetText);
        this.textGroup.add(this.cashWinText);

        this.textGroup.add(this.cashValueText);
        this.textGroup.add(this.coinsValueText);
        this.textGroup.add(this.cashBetValueText);
        this.textGroup.add(this.cashWinValueText);

        this.blackBarTopBg.position.setTo(0, 0);
        this.blackBarBg.position.setTo(0, 0);

        this.layer.add(this.blackBarTopBg);
        this.layer.add(this.blackBarBg);
        this.layer.add(this.textGroup);

        this.updateBalanceUI(gm.CurrentRealMoney, gm.currentCredits, gm.TotalBetInRealMoney, gm.CumulativeRealMoneyWon, true);

        vm.onGameScaled.remove(this.repositionUi, this);
        vm.onGameScaled.add(this.repositionUi, this);
        this.repositionUi();
    },

    repositionUi: function () {
        this.groupLayout.positionItems(this.textGroup, 2);
    },

    updateBalanceUI: function (cash, coins, cashBet, cashWin, skipAnimation) {
        console.log("CASH" + cashWin);
        if (skipAnimation) {
            this.cashValue = cash;
            this.cashValueText.text = cash.numberFormat(this.decimalCount);
            this.coinsValue = coins;
            this.coinsValueText.text = coins.numberFormat(0);
            this.cashBetValue = cashBet;
            this.cashBetValueText.text = cashBet.numberFormat(this.decimalCount);
            this.cashWinValue = cashWin;
            this.cashWinValueText.text = cashWin.numberFormat(this.decimalCount);
        }
        else {
            if (cash != undefined && cash !== this.cashValue) {
                this.startValueChangeTween(this.cashValueProgress, this.cashValueText, this.cashValue, cash, this, true);
                this.cashValue = cash;
            }
            if (coins != undefined && coins !== this.coinsValue) {
                this.startValueChangeTween(this.coinsValueProgress, this.coinsValueText, this.coinsValue, coins, this, false);
                this.coinsValue = coins;
            }
            if (cashBet != undefined && cashBet !== this.cashBetValue) {
                this.startValueChangeTween(this.cashBetValueProgress, this.cashBetValueText, this.cashBetValue, cashBet, this, true);
                this.cashBetValue = cashBet;
            }
            if (cashWin != undefined && cashWin !== this.cashWinValue) {
                this.startValueChangeTween(this.cashWinValueProgress, this.cashWinValueText, this.cashWinValue, cashWin, this, true);
                this.cashWinValue = cashWin;
            }
        }
    },

    startValueChangeTween: function (valueProgressObj, textObject, startValue, endValue, context, allowDecimal) {
        game.tweens.removeFrom(valueProgressObj, false);
        valueProgressObj.step = 0;
        var tween = game.add.tween(valueProgressObj).to({ step: 1 }, 250, Phaser.Easing.Linear.Out, true);
        tween.onUpdateCallback(this.valueUpdate, { text: textObject, startValue: startValue, endValue: endValue, context: context, allowDecimal: allowDecimal });
        tween.onComplete.add(this.valueComplete, { text: textObject, endValue: endValue, context: context, allowDecimal: allowDecimal });
    },

    valueUpdate: function (twn, percent) {
        this.startValue = this.startValue == undefined ? 0 : this.startValue;
        this.text.setText(game.math.linear(this.startValue, this.endValue, percent).numberFormat(this.allowDecimal ? this.context.decimalCount : 0));
    },

    valueComplete: function () {
        this.text.setText(this.endValue.numberFormat(this.allowDecimal ? this.context.decimalCount : 0));
    }
}