﻿var LoadingUI = function () { };

LoadingUI.prototype = {
    _introVideo: null,

    defaultAssetsPath: "Assets",
    imageDirName: "Images",
    imagesDir: "",
    isAudioLoaded: false,
    isPreloadComplete: false,
    _isIntroStarted: false,
    _skipButton: null,

    loadTextFiles: function ()
    {
        game.load.json("language", loaderHelper.getAssetPath("Assets/Text Assets/lang" + languageManager.language + ".json"));
    },

    loadScripts: function ()
    {
        game.load.script("pool", loaderHelper.getAssetPath("Model/Pool.js"));
        game.load.script("completableBase", loaderHelper.getAssetPath("Model/SubStates/Completable.js"));

        game.load.script("bonusGameSubState", loaderHelper.getAssetPath("Model/SubStates/BonusGameSubState.js"));
        game.load.script("bigWinSubState", loaderHelper.getAssetPath("Model/SubStates/BigWinSubState.js"));
        game.load.script("expandingWildSubState", loaderHelper.getAssetPath("Model/SubStates/ExpandingWildSubState.js"));
        game.load.script("multiplierSubState", loaderHelper.getAssetPath("Model/SubStates/MultiplierSubState.js"));
        game.load.script("bonusTriggerSubState", loaderHelper.getAssetPath("Model/SubStates/BonusTriggerSubState.js"));
        game.load.script("winLineSubState", loaderHelper.getAssetPath("Model/SubStates/WinLineSubState.js"));
        game.load.script("winLineHelper", loaderHelper.getAssetPath("Model/WinLineHelper.js"));
        game.load.script("bonusBoard", loaderHelper.getAssetPath("View/BonusBoard.js"));
        game.load.script("dialogPopUp", loaderHelper.getAssetPath("View/DialogPopUp.js"));
        game.load.script("tweenUtils", loaderHelper.getAssetPath("Model/TweenUtils.js"));
        game.load.script("textureOverlay", loaderHelper.getAssetPath("View/TextureOverlay.js"));
        game.load.script("boardPopUp", loaderHelper.getAssetPath("View/BoardPopUp.js"));
        game.load.script("introductionUI", loaderHelper.getAssetPath("View/IntroductionUI.js"));
        game.load.script("gameUI", loaderHelper.getAssetPath("View/GameUI.js"));
        game.load.script("gameManager", loaderHelper.getAssetPath("Model/GameManager.js"));
        game.load.script("groupHorizontalLayout", loaderHelper.getAssetPath("Model/GroupHorizontalLayout.js"));
        game.load.script("bonusGameManager", loaderHelper.getAssetPath("Model/BonusGameManager.js"));
        game.load.script("balanceScript", loaderHelper.getAssetPath("View/BalanceUI.js"));
        game.load.script("serverManager", loaderHelper.getAssetPath("Model/ServerManager.js"));
        game.load.script("gameInterfaceDesktop", loaderHelper.getAssetPath("View/User Interfaces/Desktop/GameInterfaceDesktop.js"));
        game.load.script("gameInterfaceMobile", loaderHelper.getAssetPath("View/User Interfaces/Mobile/GameInterfaceMobile.js"));
        game.load.script("payTableUI", loaderHelper.getAssetPath("View/PayTableBoard.js"));
        game.load.script("slugger", loaderHelper.getAssetPath("Model/CharacterAnimator.js"));
        game.load.script("balanceUIMobile", loaderHelper.getAssetPath("View/User Interfaces/Mobile/BalanceUIMobile.js"));
        game.load.script("balanceMenu", loaderHelper.getAssetPath("View/User Interfaces/Mobile/BalanceMenu.js"));
    },
    addGameStates: function ()
    {
        game.state.add(gameStates.main, GameUI);
    },

    loadImages: function ()
    {

        if (!Phaser.Device.desktop) {
            game.load.image("gameLogo", loaderHelper.getAssetPath("Assets/Images/gameLogo-mobile.png"));
            game.load.image("icon-playbutton", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/icon-playbutton.png"));
            game.load.image("icon-stopbutton", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/icon-stopbutton.png"));
            game.load.image("arrow_left", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/arrow_left.png"));
            game.load.image("arrow_right", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/arrow_right.png"));
            game.load.image("house-icon", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/house-icon.png"));
            game.load.image("icon-betsettings", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/icon-betsettings.png"));
            game.load.image("icon-spin", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/icon-spin.png"));
            game.load.image("icon-spinsettings", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/icon-spinsettings.png"));
            game.load.image("paytable", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/paytable.png"));
            game.load.image("sound", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/sound.png"));
            game.load.image("mute", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/mute.png"));
            game.load.image("maximize-icon", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/maximize-icon.png"));
            game.load.image("minimize-icon", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/minimize-icon.png"));
            game.load.image("menu-options", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/menu-options.png"));
            game.load.image("sloth-machine-with-prize", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/sloth-machine-with-prize.png"));
            game.load.image("close-icon", loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/close-icon.png"));
            game.load.atlasJSONArray("mobileBtn",
                loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/mobileBtn.png"),
                loaderHelper.getAssetPath("Assets/Images/GameScreen/Mobile/mobileBtn.json"));
        }
        else {
            game.load.image("winlineHover", loaderHelper.getAssetPath("Assets/Images/GameScreen/winline-indicator.png"));
            game.load.image("skipButton", loaderHelper.getAssetPath("Assets/Images/LoadingScreen/skipButton.png"));
            game.load.image("gameLogo", loaderHelper.getAssetPath("Assets/Images/gameLogo-desktop.png"));
            game.load.image("bgBetUI", loaderHelper.getAssetPath("Assets/Images/GameScreen/bg_betUI.png"));
            game.load.atlasJSONArray("btnPaytable",
                loaderHelper.getAssetPath("Assets/Images/GameScreen/btnPaytable.png"),
                loaderHelper.getAssetPath("Assets/Images/GameScreen/btnPaytable.json"));
            game.load.atlasJSONArray("btnAudio",
                loaderHelper.getAssetPath("Assets/Images/GameScreen/btnAudio.png"),
                loaderHelper.getAssetPath("Assets/Images/GameScreen/btnAudio.json"));
            game.load.atlasJSONArray("betUIButtons",
                loaderHelper.getAssetPath("Assets/Images/GameScreen/betUIButtons.png"),
                loaderHelper.getAssetPath("Assets/Images/GameScreen/betUIButtons.json"));
        }
        //Loading
        game.load.image("skipButton", loaderHelper.getAssetPath("Assets/Images/LoadingScreen/skipButton.png"));

        //Game
        game.load.image("background", loaderHelper.getAssetPath("Assets/Images/GameScreen/bg_game.jpg"));
        game.load.image("systemMessageBoard", loaderHelper.getAssetPath("Assets/Images/GameScreen/systemMessageBoard.png"));
        game.load.image("cascade", loaderHelper.getAssetPath("Assets/Images/GameScreen/cascade.png"));
        game.load.image("railFront", loaderHelper.getAssetPath("Assets/Images/GameScreen/railFront.png"));
        game.load.image("interface", loaderHelper.getAssetPath("Assets/Images/GameScreen/interface.png"));
        game.load.image("whiteOverlay", loaderHelper.getAssetPath("Assets/Images/white.png"));
        game.load.image("arrow_left", loaderHelper.getAssetPath("Assets/Images/GameScreen/arrow_left.png"));
        game.load.image("arrow_right", loaderHelper.getAssetPath("Assets/Images/GameScreen/arrow_right.png"));
        game.load.image("flare", loaderHelper.getAssetPath("Assets/Images/GameScreen/flare.png"));
        game.load.image("lightStreak", loaderHelper.getAssetPath("Assets/Images/GameScreen/lightStreak.png"));
        game.load.image("bigStar", loaderHelper.getAssetPath("Assets/Images/GameScreen/bigStar.png"));
        game.load.image("smallStar", loaderHelper.getAssetPath("Assets/Images/GameScreen/smallStar.png"));
        game.load.atlasJSONArray("holdButton",
           loaderHelper.getAssetPath("Assets/Images/Symbols/HoldButton.png"),
           loaderHelper.getAssetPath("Assets/Images/Symbols/HoldButton.json"));

        game.load.atlasJSONArray("audioButton",
           loaderHelper.getAssetPath("Assets/Images/GameScreen/audioButton.png"),
           loaderHelper.getAssetPath("Assets/Images/GameScreen/audioButton.json"));

        game.load.atlasJSONArray("lampParticles",
            loaderHelper.getAssetPath("Assets/Images/GameScreen/lampParticlesSprites.png"),
            loaderHelper.getAssetPath("Assets/Images/GameScreen/lampParticlesSprites.json"));
        
        //BigWin
        game.load.image("bigWin", loaderHelper.getAssetPath("Assets/Images/GameScreen/BigWin/bigWin.png"));
        game.load.atlasJSONArray("moneySplash1",
            loaderHelper.getAssetPath("Assets/Images/GameScreen/BigWin/moneySplash1.png"),
            loaderHelper.getAssetPath("Assets/Images/GameScreen/BigWin/moneySplash1.json"));
        game.load.atlasJSONArray("moneySplash2",
            loaderHelper.getAssetPath("Assets/Images/GameScreen/BigWin/moneySplash2.png"),
            loaderHelper.getAssetPath("Assets/Images/GameScreen/BigWin/moneySplash2.json"));
        game.load.atlasJSONArray("moneySplash3",
            loaderHelper.getAssetPath("Assets/Images/GameScreen/BigWin/moneySplash3.png"),
            loaderHelper.getAssetPath("Assets/Images/GameScreen/BigWin/moneySplash3.json"));
        game.load.atlasJSONArray("moneySplash4",
            loaderHelper.getAssetPath("Assets/Images/GameScreen/BigWin/moneySplash4.png"),
            loaderHelper.getAssetPath("Assets/Images/GameScreen/BigWin/moneySplash4.json"));

        for (var i = 0; i < 3; i++)
        {
            var name = "spotlight" + (i + 1);
            game.load.atlasJSONArray(name,
              loaderHelper.getAssetPath("Assets/Images/GameScreen/" + name + ".png"),
              loaderHelper.getAssetPath("Assets/Images/GameScreen/" + name + ".json"));
        }
        //game.load.image("btnPaytable", loaderHelper.getAssetPath("Assets/Images/GameScreen/btn_paytable.png"));
        //game.load.image("btnSpin", loaderHelper.getAssetPath("Assets/Images/GameScreen/spin.png"));
        //game.load.image("btnMaxbet", loaderHelper.getAssetPath("Assets/Images/GameScreen/maxbet.png"));
        game.load.image("challangeTheChamp", loaderHelper.getAssetPath("Assets/Images/GameScreen/ChallangeTheChamp.png"));

        game.load.atlasJSONArray("turnIcon",
            loaderHelper.getAssetPath("Assets/Images/GameScreen/turnIcon.png"),
            loaderHelper.getAssetPath("Assets/Images/GameScreen/turnIcon.json"));

        game.load.atlasJSONArray("okButton",
            loaderHelper.getAssetPath("Assets/Images/GameScreen/okButton.png"),
            loaderHelper.getAssetPath("Assets/Images/GameScreen/okButton.json"));


        //Win Lines
        game.load.image("winline1-3", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/winline1-3.png"));
        game.load.image("winline4", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/winline4.png"));
        game.load.image("winline5", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/winline5.png"));
        game.load.image("winline6-7", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/winline6-7.png"));
        game.load.image("winline8-9", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/winline8-9.png"));
        game.load.image("winline10-11", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/winline10-11.png"));
        game.load.image("winline12-13", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/winline12-13.png"));
        game.load.image("winline14", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/winline14.png"));
        game.load.image("winline15", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/winline15.png"));
        game.load.image("winline16-17", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/winline16-17.png"));
        game.load.image("winline18-19", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/winline18-19.png"));
        game.load.image("winline20", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/winline20.png"));
        game.load.image("symbolWinFrame", loaderHelper.getAssetPath("Assets/Images/GameScreen/Winlines/symbolWinFrame.png"));

        //Symbols
        game.load.atlasJSONArray(symbols.Thunder,
            loaderHelper.getAssetPath("Assets/Images/Symbols/apollo.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/apollo.json"));
        game.load.atlasJSONArray(symbols.Thunder + "anim",
            loaderHelper.getAssetPath("Assets/Images/Symbols/apolloAnim.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/apolloAnim.json"));

        game.load.atlasJSONArray(symbols.TheGentleman,
            loaderHelper.getAssetPath("Assets/Images/Symbols/british.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/british.json"));
        game.load.atlasJSONArray(symbols.TheGentleman + "anim",
            loaderHelper.getAssetPath("Assets/Images/Symbols/britishAnim.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/britishAnim.json"));

        game.load.atlasJSONArray(symbols.Hurricane,
            loaderHelper.getAssetPath("Assets/Images/Symbols/drago.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/drago.json"));
        game.load.atlasJSONArray(symbols.Hurricane + "anim",
           loaderHelper.getAssetPath("Assets/Images/Symbols/dragoAnim.png"),
           loaderHelper.getAssetPath("Assets/Images/Symbols/dragoAnim.json"));

        game.load.atlasJSONArray(symbols.TheChamp,
            loaderHelper.getAssetPath("Assets/Images/Symbols/sparring.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/sparring.json"));
        game.load.atlasJSONArray(symbols.TheChamp + "anim",
            loaderHelper.getAssetPath("Assets/Images/Symbols/sparring.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/sparring.json"));

        game.load.atlasJSONArray(symbols.BELT,
            loaderHelper.getAssetPath("Assets/Images/Symbols/championBelt.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/championBelt.json"));
        game.load.atlasJSONArray(symbols.BELT + "anim",
            loaderHelper.getAssetPath("Assets/Images/Symbols/championBeltAnim.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/championBeltAnim.json"));


        game.load.atlasJSONArray(symbols.ExpandingWild,
            loaderHelper.getAssetPath("Assets/Images/Symbols/SluggerWild.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/SluggerWild.json"));
        game.load.atlasJSONArray(symbols.ACE,
            loaderHelper.getAssetPath("Assets/Images/Symbols/A.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/A.json"));
        game.load.atlasJSONArray(symbols.JACK,
            loaderHelper.getAssetPath("Assets/Images/Symbols/J.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/J.json"));
        game.load.atlasJSONArray(symbols.KING,
            loaderHelper.getAssetPath("Assets/Images/Symbols/K.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/K.json"));
        game.load.atlasJSONArray(symbols.QUEEN,
            loaderHelper.getAssetPath("Assets/Images/Symbols/Q.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/Q.json"));
        game.load.atlasJSONArray(symbols.TEN,
            loaderHelper.getAssetPath("Assets/Images/Symbols/Ten.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/Ten.json"));
        game.load.atlasJSONArray("expandingWildAnim",
            loaderHelper.getAssetPath("Assets/Images/Symbols/expandingWildAnim.png"),
            loaderHelper.getAssetPath("Assets/Images/Symbols/expandingWildAnim.json"));

        ////Special Symbols
        //game.load.atlasJSONArray(symbols.WildStar,
        //    loaderHelper.getAssetPath("Assets/Images/Symbols/WildStar.png"),
        //    loaderHelper.getAssetPath("Assets/Images/Symbols/WildStar.json"));
        

        game.load.image("whiteOverlay", loaderHelper.getAssetPath("Assets/Images/white.png"));

        //bonus screen
        game.load.image("bonusBg", loaderHelper.getAssetPath("Assets/Images/BonusScreen/bonusBg.png"));
        game.load.image("clickmeKO", loaderHelper.getAssetPath("Assets/Images/BonusScreen/clickmeKO.png"));
        game.load.image("areaOutlines", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/areaOutlines.png"));
        game.load.image("belly", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/belly.png"));
        game.load.image("head", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/head.png"));
        game.load.image("leftArm", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/leftArm.png"));
        game.load.image("leftBoob", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/leftBoob.png"));
        game.load.image("leftGlove", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/leftGlove.png"));
        game.load.image("leftLeg", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/leftLeg.png"));
        game.load.image("pantsLeft", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/pantsLeft.png"));
        game.load.image("pantsRight", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/pantsRight.png"));
        game.load.image("rightArm", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/rightArm.png"));
        game.load.image("rightBoob", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/rightBoob.png"));
        game.load.image("rightGlove", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/rightGlove.png"));
        game.load.image("rightLeg", loaderHelper.getAssetPath("Assets/Images/BonusScreen/lids/rightLeg.png"));
        game.load.image("scoreboard", loaderHelper.getAssetPath("Assets/Images/BonusScreen/scoreboard.png"));
        game.load.image("crowdStill", loaderHelper.getAssetPath("Assets/Images/BonusScreen/crowdStill.png"));


        //introductionUI

        game.load.image("stars", loaderHelper.getAssetPath("Assets/Images/GameScreen/stars.png"));
        game.load.image("boxer", loaderHelper.getAssetPath("Assets/Images/GameScreen/boxer.png"));
        game.load.image("introductionBG", loaderHelper.getAssetPath("Assets/Images/GameScreen/introductionBG.jpg"));
        game.load.image("championBelt", loaderHelper.getAssetPath("Assets/Images/GameScreen/championBelt.png"));

        //slugger animations
        game.load.atlasJSONArray("hitBack1",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/hitsBack1.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/hitsBack1.json"));
        game.load.atlasJSONArray("hitBack2",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/hitsBack2.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/hitsBack2.json"));
        game.load.atlasJSONArray("hitBack3",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/hitsBack3.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/hitsBack3.json"));
        game.load.atlasJSONArray("hitBack4",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/hitsBack4.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/hitsBack4.json"));


        game.load.atlasJSONArray("idle",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/idle.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/idle.json"));

        game.load.atlasJSONArray("face1",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/face1.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/face1.json"));
        game.load.atlasJSONArray("face2",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/face2.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/face2.json"));

        game.load.atlasJSONArray("knockOut1",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/knockOut1.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/knockOut1.json"));
        game.load.atlasJSONArray("knockOut2",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/knockOut2.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/knockOut2.json"));
        game.load.atlasJSONArray("knockOut3",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/knockOut3.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/knockOut3.json"));
        game.load.atlasJSONArray("knockOut4",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/knockOut4.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/knockOut4.json"));

        game.load.atlasJSONArray("stomach1",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/stomach1.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/stomach1.json"));
        game.load.atlasJSONArray("stomach2",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/stomach2.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/stomach2.json"));

        game.load.atlasJSONArray("misses1",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/misses1.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/misses1.json"));
        game.load.atlasJSONArray("misses2",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/misses2.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/slugger/misses2.json"));

        //Crowd
        game.load.atlasJSONArray("crowdcheer",
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/crowd/crowdcheer.png"),
           loaderHelper.getAssetPath("Assets/Images/BonusScreen/crowd/crowdcheer.json"));

        game.load.atlasJSONArray("crowdshout",
          loaderHelper.getAssetPath("Assets/Images/BonusScreen/crowd/crowdshout.png"),
          loaderHelper.getAssetPath("Assets/Images/BonusScreen/crowd/crowdshout.json"));


        //PT_Backgraund
        game.load.image("arrowLeft", loaderHelper.getAssetPath("Assets/Images/SettingsScreen/ptBackground/arrowLeft.png"));
        game.load.image("arrowRight", loaderHelper.getAssetPath("Assets/Images/SettingsScreen/ptBackground/arrowRight.png"));
        game.load.image("payTableWindow", loaderHelper.getAssetPath("Assets/Images/SettingsScreen/ptBackground/paytableWindow.png"));
        game.load.image("paytableExit", loaderHelper.getAssetPath("Assets/Images/SettingsScreen/ptBackground/exit.png"));
        game.load.image("paytableExitHover", loaderHelper.getAssetPath("Assets/Images/SettingsScreen/ptBackground/exitHover.png"));


        //PT1


        //PT2
        game.load.image("paytableLights", loaderHelper.getAssetPath("Assets/Images/SettingsScreen/PT2/paytableLights.png"));

        //PT3
        

        //PT4
        game.load.image("PT_Slugger", loaderHelper.getAssetPath("Assets/Images/SettingsScreen/PT4/sluggerClickExplanation.png"));


        //PT5
        game.load.image("PT_Winlines", loaderHelper.getAssetPath("Assets/Images/SettingsScreen/PT5/winlines.png"));


        //no cache forced to prevent caching. Firefox and some problems are 
        //having issue with bitmap fonts.
        game.load.bitmapFont("ObelixProWinAmountWinline",
            loaderHelper.getAssetPath("Assets/Fonts/ObelixProWinAmountWinline.png", false),
            loaderHelper.getAssetPath("Assets/Fonts/ObelixProWinAmountWinline.fnt", false));

        game.load.bitmapFont("ObelixProBonusGameLight",
            loaderHelper.getAssetPath("Assets/Fonts/ObelixProBonusGameLight.png", false),
            loaderHelper.getAssetPath("Assets/Fonts/ObelixProBonusGameLight.fnt", false));

        game.load.bitmapFont("ObelixProPaytable",
            loaderHelper.getAssetPath("Assets/Fonts/ObelixProPaytable.png", false),
            loaderHelper.getAssetPath("Assets/Fonts/ObelixProPaytable.fnt", false));

        game.load.bitmapFont("ObelixProPaytableHeader",
           loaderHelper.getAssetPath("Assets/Fonts/ObelixProPaytableHeader.png", false),
           loaderHelper.getAssetPath("Assets/Fonts/ObelixProPaytableHeader.fnt", false));

        game.load.bitmapFont("scoreFont",
            loaderHelper.getAssetPath("Assets/Fonts/fallBackFont.png", false),
            loaderHelper.getAssetPath("Assets/Fonts/fallBackFont.fnt", false));

        game.load.bitmapFont("lcdFont",
            loaderHelper.getAssetPath("Assets/Fonts/fallBackFont.png", false),
            loaderHelper.getAssetPath("Assets/Fonts/fallBackFont.fnt", false));

        game.load.bitmapFont("chalkFont",
            loaderHelper.getAssetPath("Assets/Fonts/fallBackFont.png", false),
            loaderHelper.getAssetPath("Assets/Fonts/fallBackFont.fnt", false));

        game.load.bitmapFont("badaboomAutospin",
            loaderHelper.getAssetPath("Assets/Fonts/BadaboomAutospin.png", false),
            loaderHelper.getAssetPath("Assets/Fonts/BadaboomAutospin.fnt", false));

        game.load.bitmapFont("badaboomInterface",
            loaderHelper.getAssetPath("Assets/Fonts/BadaboomInterface.png", false),
            loaderHelper.getAssetPath("Assets/Fonts/BadaboomInterface.fnt", false));

    },
    loadVideos: function ()
    {
        if (Phaser.Device.desktop)
        {
            game.load.video("introVideo", loaderHelper.getAssetPath("Assets/Videos/intro_cinematic2.mp4"));
            game.load.video("bonusToMain", loaderHelper.getAssetPath("Assets/Videos/BonusToMain-SD.mp4"));
            game.load.video("mainToBonus", loaderHelper.getAssetPath("Assets/Videos/MainToBonus-SD.mp4"));
        }
    },
    preload: function ()
    {
        audioManager.init();
        audioManager.playAmbientSound();
        debugUtils.log("LoadingUI - preloading");

        this.imagesDir = this.defaultAssetsPath + "/" + this.imageDirName;

        this.loadTextFiles();
        this.loadScripts();
        //this.loadAudio();
        this.loadImages();
        this.loadVideos();

        vm.onGameScaled.addOnce(this.positionUI, this);
        vm.activateCustomScaling();

    },

    positionUI: function ()
    {
        this.bg = game.add.sprite(0, 0, "loadingBg");
        this.bg.width = game.width;
        this.bg.height = game.height;


        var myLogo = game.add.sprite(game.width / 2, -500, "loadingLogoReveal");
        myLogo.anchor.setTo(0.5);

        var logoTargetY = game.height / 2 - 50;

        var loadingShadow = game.add.sprite(game.width / 2, game.height / 2 + 150, "loadingShadow");
        loadingShadow.anchor.setTo(0.5);
        loadingShadow.alpha = 0;
        loadingShadow.scale.x = 0.25;

        //Loading bar:
        var loadingBar = game.add.sprite(myLogo.x - myLogo.width / 2, logoTargetY, "loading");
        loadingBar.y += (myLogo.height / 2) + (loadingBar.height / 2) + 10;
        loadingBar.anchor.setTo(0);
        this.load.setPreloadSprite(loadingBar);
        loadingBar.visible = false;

        var loadingBarEmpty = game.add.sprite(loadingBar.x, loadingBar.y, "loadingBarEmpty");
        loadingBarEmpty.anchor.setTo(loadingBar.anchor.x, loadingBar.anchor.y);
        loadingBarEmpty.alpha = 0;
        loadingBarEmpty.scale.x = 0.25;

        game.add.tween(loadingShadow).to({ alpha: 1 }, 150, Phaser.Easing.Linear.In, true);

        var logoOverthrowTween = game.add.tween(myLogo).to({ y: logoTargetY + 50 }, 150, Phaser.Easing.Linear.In, true);
        logoOverthrowTween.onComplete.addOnce(function () {
            var logoEaseBackTween = game.add.tween(myLogo).to({ y: logoTargetY }, 50, Phaser.Easing.Linear.In, true);

            logoEaseBackTween.onComplete.addOnce(function () {
                var anim = myLogo.animations.add("idle");
                anim.onComplete.addOnce(function () {
                    var loadingLogoShine = game.add.sprite(game.width / 2 + 88, game.height / 2 - 50, "loadingLogoShine");
                    loadingLogoShine.anchor.setTo(0.5);
                    loadingLogoShine.animations.add("idle").play();
                }, this);
                anim.play();
                game.add.tween(loadingShadow.scale).to({ x: 1 }, 50, Phaser.Easing.Linear.In, true);

                game.add.tween(loadingBarEmpty).to({ alpha: 1 }, 250, Phaser.Easing.Linear.In, true);
                var loadingBarScaleTween = game.add.tween(loadingBarEmpty.scale).to({ x: 1 }, 150, Phaser.Easing.Linear.In, true);
                loadingBarScaleTween.onComplete.addOnce(function () {
                    loadingBar.visible = true;
                }, this);
            }, this);
        }, this);

        game.world.bringToTop(loadingBar);
    },

    create: function ()
    {      
        debugUtils.log("LoadingUI - create");
        game.scale.onSizeChange.add(this._refreshLayout, this);
        game.stage.disableVisibilityChange = true;
        this.addGameStates();
        languageManager.cacheCurrentLanguage();
        this.isPreloadComplete = true;
        debugUtils.log("LoadingUI - end");        
    },

    _refreshLayout: function ()
    {
        if (this._skipButton)
        {
            console.log(game.scalingOffset);
            this._skipButton.position.setTo(game.width - 18 - game.scalingOffset, 18);
            this._skipButton.x -= this._skipButton.width * 0.5;
            this._skipButton.y += this._skipButton.height * 0.5;
        }
    },

    playIntroVideo: function ()
    {
        this._isIntroStarted = true;
        var fadeInTween = textureOverlay.fade(0xffffff, undefined, 1, 0, 350, true);
        fadeInTween.onComplete.addOnce(function ()
        {
            this._introVideo = game.add.video("introVideo");
            this._introVideo.touchLocked = false;
            this._introVideo.play(false);
            this._introVideo.onComplete.add(this.videoCompleted, this);

            this._introVideo.addToWorld(0, 0, 0, 0);

            var spinHotKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
            spinHotKey.onDown.add(this.skipVideo, this);

            this._skipButton = game.add.sprite(game.width - 18 - game.scalingOffset, 18, "skipButton");
            this._skipButton.x -= this._skipButton.width * 0.5;
            this._skipButton.y += this._skipButton.height * 0.5;
            this._skipButton.inputEnabled = true;
            this._skipButton.anchor.setTo(0.5);
            this._skipButton.events.onInputOver.add(function () { this.scale.setTo(1.15); }, this._skipButton);
            this._skipButton.events.onInputOut.add(function () { this.scale.setTo(1); }, this._skipButton);
            this._skipButton.events.onInputUp.addOnce(this.skipVideo, this);

            audioManager.addClickUpDownSound(this._skipButton);
            textureOverlay.fade(0xffffff, undefined, 0, 1, 350, true);
        }, this);

    },
    update: function ()
    {
        //console.log("this.isAudioLoaded: " + this.isAudioLoaded);
        //console.log("this.isPreloadComplete: " + this.isPreloadComplete);
        //console.log("this._isIntroStarted: " + this._isIntroStarted);

        if (!this.isAudioLoaded)
        {
            this.isAudioLoaded = this.cache.isSoundReady("audioSprite");
        }
        else if (this.isPreloadComplete && !this._isIntroStarted)
        {
            //if (Phaser.Device.desktop) this.playIntroVideo();
            //else this.skipVideo();
            this.skipVideo();
        }
    },

    skipVideo: function ()
    {
        if (this._introVideo) this._introVideo.stop();
        this.videoCompleted();
    },

    videoCompleted: function ()
    {
        gm.gameReady();
        //game.stage.disableVisibilityChange = false;
    }

};