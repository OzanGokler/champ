﻿function DialogPopUp() { }

DialogPopUp.prototype._langJSON = undefined;
DialogPopUp.prototype._title = undefined;
DialogPopUp.prototype._desc = undefined;
DialogPopUp.prototype._deco1 = undefined;
DialogPopUp.prototype._deco2 = undefined;
DialogPopUp.prototype._closeButton = undefined;
DialogPopUp.prototype._okButton = undefined;

DialogPopUp.prototype.show = function (titleLangKey, descLangKey, autoHide, onClickCallback, context)
{

    this._title = game.make.bitmapText(game.width / 2, 228, "ObelixProPaytable", titleLangKey ? languageManager.langJSON[titleLangKey] : "", 24);
    this._title.anchor.setTo(0.5, 0.5);
    this._title.maxWidth = 500;
    this._title.align = "center";

    this._desc = game.add.bitmapText(game.width / 2, 340, "chalkFont", descLangKey ? languageManager.langJSON[descLangKey] : "", 22);
    this._desc.tint = 0x000000;
    this._desc.anchor.setTo(0.5, 0.5);
    this._desc.maxWidth = 500;
    this._desc.align = "center";

    this._okButton = game.add.button(game.width / 2, 380, "okButton", this.okButtonClicked, this, 2, 0, 1);
    this._okButton.anchor.setTo(0.5);
    //this._okButton.onInputOver.add(over, this);
    //this._okButton.onInputOut.add(out, this);
    //this._okButton.onInputUp.add(up, this);

    //this._deco1 = game.add.sprite(game.width / 2 - 335, 225, "bigWinDeco");
    //this._deco1.anchor.setTo(0.5, 0.5);

    //this._deco2 = game.add.sprite(game.width / 2 + 335, 225, "bigWinDeco");
    //this._deco2.anchor.setTo(0.5, 0.5);

    this._closeButton = game.add.sprite(game.width - 355, 231, "paytableExit");
    this._closeButton.anchor.setTo(0.5, 0.5);
    this._closeButton.inputEnabled = true;
    this._closeButton.events.onInputOver.add(function () { this.scale.setTo(1.15); }, this._closeButton);
    this._closeButton.events.onInputOut.add(function () { this.scale.setTo(1); }, this._closeButton);

    boardPopUp.addToBoard(this._title);
    boardPopUp.addToBoard(this._desc);
    boardPopUp.addToBoard(this._okButton);
    //boardPopUp.addToBoard(this._deco1);
    //boardPopUp.addToBoard(this._deco2);
    boardPopUp.addToBoard(this._closeButton);
    if (onClickCallback && context) boardPopUp.onDismiss.addOnce(onClickCallback, context);
    this._closeButton.events.onInputDown.addOnce(boardPopUp.hide, boardPopUp);
    boardPopUp.show(autoHide, true, true, "systemMessageBoard");
}

DialogPopUp.prototype.okButtonClicked = function()
{
    boardPopUp.hide();
}

var dialogPopUp = new DialogPopUp();