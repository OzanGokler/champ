﻿var PreloaderUI = function () { };

PreloaderUI.prototype = {
    isAudioLoaded: false,
    preload: function ()
    {
        game.stage.disableVisibilityChange = true;

        //Sripts needs to be accessed in loadingUI should be loaded here.
        game.load.audiosprite("audioSprite",
            [loaderHelper.getAssetPath("Assets/Audio/audioAtlas.m4a"),
            loaderHelper.getAssetPath("Assets/Audio/audioAtlas.ogg"),
            loaderHelper.getAssetPath("Assets/Audio/audioAtlas.mp3"),
            loaderHelper.getAssetPath("Assets/Audio/audioAtlas.ac3")],
            loaderHelper.getAssetPath("Assets/Audio/audioAtlas.json"));

        game.load.script("audioManager", loaderHelper.getAssetPath("Model/AudioManager.js"));
        game.load.script("languageManager", loaderHelper.getAssetPath("Model/LanguageManager.js"));
        game.load.script("inputController", loaderHelper.getAssetPath("Model/InputController.js"));
        game.load.script("symbol", loaderHelper.getAssetPath("Model/Symbol.js"));
        game.load.script("symbols", loaderHelper.getAssetPath("Model/Symbols.js"));

        game.load.image("loading", loaderHelper.getAssetPath("Assets/Images/LoadingScreen/loadingBar.png"));
        game.load.image("loadingBarEmpty", loaderHelper.getAssetPath("Assets/Images/LoadingScreen/loadingBarEmpty.png"));
        game.load.image("loadingShadow", loaderHelper.getAssetPath("Assets/Images/LoadingScreen/loadingShadow.png"));
        game.load.image("loadingBg", loaderHelper.getAssetPath("Assets/Images/LoadingScreen/loadingBg.jpg"));

        game.load.atlasJSONArray("loadingLogoReveal",
                loaderHelper.getAssetPath("Assets/Images/LoadingScreen/loadingLogoReveal.png"),
                loaderHelper.getAssetPath("Assets/Images/LoadingScreen/loadingLogoReveal.json"));

        game.load.atlasJSONArray("loadingLogoShine",
                loaderHelper.getAssetPath("Assets/Images/LoadingScreen/loadingLogoShine.png"),
                loaderHelper.getAssetPath("Assets/Images/LoadingScreen/loadingLogoShine.json"));

        game.load.script(gameStates.loading, loaderHelper.getAssetPath("View/LoadingUI.js"));
    },

    update: function ()
    {
        if (!this.isAudioLoaded)
        {
            this.isAudioLoaded = this.cache.isSoundReady("audioSprite");
            if (this.isAudioLoaded)
            {
                game.state.add(gameStates.loading, LoadingUI);
                game.state.start(gameStates.loading);
                console.log("start: " + gameStates.loading);
            }
        }
    }
}