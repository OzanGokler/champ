﻿var IntroductionUI = function () { };

IntroductionUI.prototype.spotLight = null;
IntroductionUI.prototype.championBelt = null;
IntroductionUI.prototype.btnOK = null;
IntroductionUI.prototype.boxer = null;
IntroductionUI.prototype.stars = null;
IntroductionUI.prototype.introductionBG = null;
IntroductionUI.prototype.bIsOpen = true;
//IntroductionUI.prototype.titleText = null;
//IntroductionUI.prototype.expandingWildText_title = null;
//IntroductionUI.prototype.expandingWildText_left = null;
//IntroductionUI.prototype.expandingWildText_right = null;
//IntroductionUI.prototype.championshipBonusText_title = null;
//IntroductionUI.prototype.championshipBonusText = null;
IntroductionUI.prototype.uiGroup = null;
IntroductionUI.prototype.multiElementsGroup = null;
IntroductionUI.prototype.fadeTween = null;





IntroductionUI.prototype._positionUI = function () {




    //this.uiGroup.add(this.introductionBG);
    //this.uiGroup.add(this.titleText);
    //this.uiGroup.add(this.expandingWildText_title);
    //this.uiGroup.add(this.expandingWildText_left);
    //this.uiGroup.add(this.expandingWildText_right);
    //this.uiGroup.add(this.championshipBonusText_title);
    //this.uiGroup.add(this.championshipBonusText);
    this.bIsOpen = true;
    this.introductionBg = game.add.sprite(game.width / 2, vm.ActiveGameHeight / 2, "introductionBG");
    this.introductionBg.scale.setTo(1);
    this.introductionBg.anchor.setTo(0.5);


    //this.spotLight = game.add.sprite(650, 200 , "spotlight");
    //this.spotLight.scale.setTo(1);
    //this.spotLight.frame = 0;
    //this.spotLight.anchor.setTo(0.5);

    this.stars = game.add.sprite(game.width / 2 - 100, 300, "stars");
    this.stars.scale.setTo(0.4);
    this.stars.anchor.setTo(0.5);

    this.boxer = game.add.sprite(this.stars.position.x - 110, 300, "boxer");
    this.boxer.scale.setTo(0.4);
    this.boxer.anchor.setTo(0.5);



    //this.btnOk = game.add.sprite(gameWidth/2, 565, "okButton");
    //this.btnOk.frame = 0;
    //this.btnOk.scale.setTo(1);
    //this.btnOk.anchor.setTo(0.5);

    this.multiElementsGroup = game.add.group();
    for (var i = 0; i < 3; i++) {
        this.championBelt = game.add.sprite(680 + i * 110, 470, "championBelt");
        this.championBelt.scale.setTo(0.22);
        this.championBelt.anchor.setTo(0.5);
        this.multiElementsGroup.add(this.championBelt);
        this.spotLight = game.add.sprite(755 + i * 45, 300, "spotlight" + (i + 1));
        this.spotLight.scale.setTo(0.55);
        this.spotLight.frame = 1;
        this.spotLight.anchor.setTo(0.5);
        this.multiElementsGroup.add(this.spotLight);
    }






    this.btnOk = game.add.button(game.width / 2, 565, "okButton", this.click_btnOk, this, 2, 0, 1);

    this.uiGroup = game.add.group();
    this.uiGroup.add(this.introductionBg);
    this.uiGroup.add(this.boxer);
    this.uiGroup.add(this.stars);
    this.uiGroup.add(this.multiElementsGroup);
    this.uiGroup.add(this.btnOk);
    this.btnOk.anchor.setTo(0.5);
    
    //this.btnOk.events.onInputOver.add(function () { this.btnOk.frame=1; }, this);
    //this.btnOk.events.onInputDown.addOnce(this.click_btnOk, this);

}

IntroductionUI.prototype.click_btnOk = function () {
    console.log("ClickedOK")
    this.fadeTween = game.add.tween(this.uiGroup).to({ alpha: 0 }, 750, Phaser.Easing.Linear.In, true);
    this.btnOk.inputEnabled = false;
    this.bIsOpen = false;
}


var introductionUI = new IntroductionUI();



