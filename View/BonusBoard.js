﻿var BonusBoard = function () { };

BonusBoard.prototype._title = null;
BonusBoard.prototype.show = function (onClickCallback, context)
{
    this._title = game.make.bitmapText(game.width / 2, 300, "chalkFont", "", 160);
    this._title.anchor.setTo(0.5, 0.5);

    if (onClickCallback && context) boardPopUp.onDismiss.addOnce(onClickCallback, context);
    boardPopUp.show(true);
    boardPopUp.addToBoard(this._title);

    game.time.events.add(250, function ()
    {
        game.time.events.repeat(95,languageManager.langJSON.bonusGame.length, this.displayNextBonusLetter,
        { textObject: this._title, message:languageManager.langJSON.bonusGame, counter: 1 });
    }, this);
}
BonusBoard.prototype.displayNextBonusLetter = function ()
{
    this.textObject.text = this.message.substr(0, this.counter++);
}

var bonusBoard = new BonusBoard();