﻿var PayTableBoard = function () { };

console.log("PayTableBoard");

PayTableBoard.prototype = {
    _currentIndex: 0,
    //PT_Backgraund
    btn_leftArrow: null,
    btn_rightArrow: null,
    btn_close: null,

    //PT1
    pt1_HeaderText: null,
    pt1SparringText: null,
    pt1DragoText: null,
    pt1ApolloText: null,
    pt1BritishText: null,
    pt1Atext: null,
    pt1Ktext: null,
    pt1Qtext: null,
    pt1Jtext: null,
    pt1Tentext: null,
    apollo: null,
    british: null,
    drago: null,
    A: null,
    K: null,
    Q: null,
    J: null,
    ten: null,


    //PT2
    pt2_HeaderText: null,
    pt2Text1: null,
    pt2Text2: null,
    pt2Text3: null,
    pt2Text4: null,
    pt2Light: null,
    pt2Slugger: null,
    pt2Star: null,
    pt2examples: null,
    expandingWild: null,

    //PT3 old
    //pt3_HeaderText: null,
    //pt3Text0: null,
    //pt3Text1: null,
    //pt3Text2: null,
    //pt3Text3: null,
    //pt3Text4: null,
    //pt3Text5: null,
    //pt3Belt0: null,
    //pt3Belt1: null,
    //pt3Belt2: null,
    //pt3spotLight: null,

    //PT3
    pt3_HeaderText: null,
    pt3Text1: null,
    pt3Text2: null,
    pt3Text3: null,
    pt3Text4: null,
    pt3Text5: null,
    pt3Text6: null,
    pt3Slugger: null,
    pt3LightExplanation: null,
    pt3Arrow: null,
    pt3Belt0: null,
    pt3Belt1: null,
    pt3Belt2: null,

    //PT4
    pt4text: null,
    pt4Winline: null,

    //PT5
    pt5_HeaderText: null,
    pt5Text1: null,
    pt5Text2: null,
    pt5Text3: null,
    pt5Text4: null,

    parts: new Array(5),

    _symbolDescTextSize: 30,

    _pages: new Array(),

    _betLevels: undefined,

    show: function (onClickCallback, context)
    {
        this._currentIndex = 0;
        this._pages.splice(0, this._pages.length);
        inputController.addInputLock();
        this._positionUI();
        this._initializeInputs(onClickCallback, context);
        gm.requestCurrentBet(this._onBetLevelLoaded, this, true);
        boardPopUp.onDismiss.addOnce(this.onDismiss, this);
    },
    onDismiss: function () {
        if (!Phaser.Device.desktop) {
            gameUI.Interface.toggleAllButtons(true);
        }
    },

    _onBetLevelLoaded: function (betLevels)
    {
        this._betLevels = betLevels;
        gm.requestPayTable(this._onPayTableLoaded, this);
    },

    _onPayTableLoaded: function (confs)
    {
        var texts = new Array(10);

        for (var l = 0; l < texts.length; l++)
        {
            texts[l] = "";
        }

        for (var j = 0; j < confs[0].length; j++)
        {
            for (var k = 4; k >= 0; k--)
            {
                if (confs[k][j] > 0)
                {
                    texts[j] += (k + 1) + "     "  + confs[k][j] /** this._betLevels.coinValue*/ * this._betLevels.betLevel;
                    if (k > 0) texts[j] += "\n";
                }
            }         
        }
        this.pt1SparringText.text = texts[0];
        this.pt1DragoText.text = texts[1];
        this.pt1ApolloText.text = texts[2];
        this.pt1BritishText.text = texts[3];
        this.pt1Atext.text = texts[4];
        this.pt1Ktext.text = texts[5];
        this.pt1Qtext.text = texts[6];
        this.pt1Jtext.text = texts[7];
        this.pt1Tentext.text = texts[8];
      

        inputController.removeInputLock();
        boardPopUp.show(false, true, true, "payTableWindow");
        
    },

    _positionUI: function ()
    {
        var langJSON = game.cache.getJSON("language");

        for (var i = 0; i < 5; i++) {
            this.parts[i] = game.add.group();
        }
        //PT_Backgraund

        this.btn_leftArrow = game.add.sprite(280, 350, "arrowLeft");
        this.btn_rightArrow = game.add.sprite(890, 350, "arrowRight");
        this.btn_close = game.add.sprite(900, 105, "paytableExit");


        boardPopUp.addToBoard(this.btn_leftArrow);
        boardPopUp.addToBoard(this.btn_rightArrow);
        boardPopUp.addToBoard(this.btn_close);


        //PT1
        this.pt1_HeaderText = game.add.bitmapText(game.width / 2, 120, "ObelixProPaytableHeader", langJSON.payTable, 20);
        this.pt1_HeaderText.anchor.setTo(0.5);
        boardPopUp.addToBoard(this.pt1_HeaderText);

        this.pt1SparringText = game.add.bitmapText(430, 135, "ObelixProPaytable", "", 20);
        boardPopUp.addToBoard(this.pt1SparringText);

        this.pt1DragoText = game.add.bitmapText(430, 255, "ObelixProPaytable", "", 20);
        boardPopUp.addToBoard(this.pt1DragoText);

        this.pt1ApolloText = game.add.bitmapText(430, 358, "ObelixProPaytable", "", 20);
        boardPopUp.addToBoard(this.pt1ApolloText);

        this.pt1BritishText = game.add.bitmapText(430, 463, "ObelixProPaytable", "", 20);
        boardPopUp.addToBoard(this.pt1BritishText);

        this.pt1Atext = game.add.bitmapText(715, 162, "ObelixProPaytable", "", 16);
        boardPopUp.addToBoard(this.pt1Atext);

        this.pt1Ktext = game.add.bitmapText(715, 240, "ObelixProPaytable", "", 16);
        boardPopUp.addToBoard(this.pt1Ktext);

        this.pt1Qtext = game.add.bitmapText(715, 322, "ObelixProPaytable", "", 16);
        boardPopUp.addToBoard(this.pt1Qtext);

        this.pt1Jtext = game.add.bitmapText(715, 402, "ObelixProPaytable", "", 16);
        boardPopUp.addToBoard(this.pt1Jtext);

        this.pt1Tentext = game.add.bitmapText(715, 483, "ObelixProPaytable", "", 16);
        boardPopUp.addToBoard(this.pt1Tentext);

        this.sparring = game.add.sprite(330, 120, "SparringPartner");
        this.sparring.frame = 1;
        this.sparring.scale.setTo(0.45);
        boardPopUp.addToBoard(this.sparring);

        this.drago = game.add.sprite(330, 230, "DragoRussian");
        this.drago.frame = 1;
        this.drago.scale.setTo(0.45);
        boardPopUp.addToBoard(this.drago);

        this.apollo = game.add.sprite(330, 340, "Apollo");
        this.apollo.frame = 1;
        this.apollo.scale.setTo(0.45);
        boardPopUp.addToBoard(this.apollo);

        this.british = game.add.sprite(327, 450, "BritishFighter");
        this.british.frame = 1;
        this.british.scale.setTo(0.45);
        boardPopUp.addToBoard(this.british);

        this.A = game.add.sprite(640, 160, "A");
        this.A.frame = 1;
        this.A.scale.setTo(0.35);
        boardPopUp.addToBoard(this.A);

        this.K = game.add.sprite(640, 240, "K");
        this.K.frame = 1;
        this.K.scale.setTo(0.35);
        boardPopUp.addToBoard(this.K);

        this.Q = game.add.sprite(640, 320, "Q");
        this.Q.frame = 1;
        this.Q.scale.setTo(0.35);
        boardPopUp.addToBoard(this.Q);

        this.J = game.add.sprite(640, 400, "J");
        this.J.frame = 1;
        this.J.scale.setTo(0.35);
        boardPopUp.addToBoard(this.J);

        this.ten = game.add.sprite(640, 480, "Ten");
        this.ten.frame = 1;
        this.ten.scale.setTo(0.35);
        boardPopUp.addToBoard(this.ten);

        this.parts[0].add(this.sparring);
        this.parts[0].add(this.drago);
        this.parts[0].add(this.apollo);
        this.parts[0].add(this.british);
        this.parts[0].add(this.A);
        this.parts[0].add(this.K);
        this.parts[0].add(this.Q);
        this.parts[0].add(this.J);
        this.parts[0].add(this.ten);
        this.parts[0].add(this.pt1_HeaderText);
        this.parts[0].add(this.pt1ApolloText);
        this.parts[0].add(this.pt1BritishText);
        this.parts[0].add(this.pt1DragoText);
        this.parts[0].add(this.pt1SparringText);
        this.parts[0].add(this.pt1Atext);
        this.parts[0].add(this.pt1Ktext);
        this.parts[0].add(this.pt1Qtext);
        this.parts[0].add(this.pt1Jtext);
        this.parts[0].add(this.pt1Tentext);

        //PT2 
        this.pt2_HeaderText = game.add.bitmapText(game.width/2, 120, "ObelixProPaytableHeader", langJSON.explanation, 18);
        this.pt2_HeaderText.anchor.setTo(0.5);
        boardPopUp.addToBoard(this.pt2_HeaderText);

        this.pt2Text1 = game.add.bitmapText(430, 150, "ObelixProPaytable", langJSON.pt2Text1, 12);
        boardPopUp.addToBoard(this.pt2Text1);

        this.pt2Text2 = game.add.bitmapText(350, 180, "ObelixProPaytable", langJSON.pt2Text2, 12);
        boardPopUp.addToBoard(this.pt2Text2);

        //this.pt2Text3 = game.add.bitmapText(350, 525, "ObelixProPaytable", langJSON.pt2Text3, 12);
        //boardPopUp.addToBoard(this.pt2Text3);

        this.pt2Text4 = game.add.bitmapText(350, 260, "ObelixProPaytable", langJSON.pt2Text4, 12);
        boardPopUp.addToBoard(this.pt2Text4);

        //this.pt2Slugger = game.add.sprite(610, 230, "SluggerWild");
        //this.pt2Slugger.frame = 1;
        //this.pt2Slugger.scale.setTo(0.6);
        //boardPopUp.addToBoard(this.pt2Slugger);

        this.expandingWild = game.add.sprite(730, 143, "expandingWildAnim");
        this.expandingWild.frame = 88;
        this.expandingWild.scale.setTo(0.89);
        boardPopUp.addToBoard(this.expandingWild);


        this.pt2Light = game.add.sprite(370, 375, "paytableLights");
        this.pt2Light.frame = 1;
        //this.pt2Light.scale.setTo(0.6);
        boardPopUp.addToBoard(this.pt2Light);


        this.parts[1].add(this.pt2_HeaderText);
        this.parts[1].add(this.pt2Text1);
        this.parts[1].add(this.pt2Text2);
        //this.parts[1].add(this.pt2Text3);
        this.parts[1].add(this.pt2Text4);
        //this.parts[1].add(this.pt2Slugger);
        this.parts[1].add(this.pt2Light);
        this.parts[1].add(this.expandingWild);

        ////PT3 burası hakkında ekranı   
        //this.pt3_HeaderText = game.add.bitmapText(game.width / 2, 120, "ObelixProPaytableHeader", langJSON.pt3TextHeader, 28);
        //this.pt3_HeaderText.anchor.setTo(0.5);
        //boardPopUp.addToBoard(this.pt3_HeaderText);

        //this.pt3Text0 = game.add.bitmapText(360, 160, "ObelixProPaytable", langJSON.pt2Text1, 12);
        //boardPopUp.addToBoard(this.pt3Text0);

        //this.pt3Text1 = game.add.bitmapText(305, 185, "ObelixProPaytable", langJSON.pt3Text1, 12);
        //boardPopUp.addToBoard(this.pt3Text1);

        //this.pt3Text2 = game.add.bitmapText(315, 425, "ObelixProPaytable", langJSON.pt3Text2, 12);
        //boardPopUp.addToBoard(this.pt3Text2);

        //this.pt3Text3 = game.add.bitmapText(305, 450, "ObelixProPaytable", langJSON.pt3Text3, 12);
        //boardPopUp.addToBoard(this.pt3Text3);

        //this.pt3Text4 = game.add.bitmapText(605, 185, "ObelixProPaytable", langJSON.pt3Text4, 12);
        //boardPopUp.addToBoard(this.pt3Text4);

        //this.pt3Belt0 = game.add.sprite(655, 453, symbols.BELT);
        //this.pt3Belt0.frame = 1;
        //this.pt3Belt0.anchor.setTo(0.5);
        //this.pt3Belt0.scale.setTo(0.4);
        //boardPopUp.addToBoard(this.pt3Belt0);

        //this.pt3Belt1 = game.add.sprite(755, 453, symbols.BELT);
        //this.pt3Belt1.frame = 1;
        //this.pt3Belt1.anchor.setTo(0.5);
        //this.pt3Belt1.scale.setTo(0.4);
        //boardPopUp.addToBoard(this.pt3Belt1);

        //this.pt3Belt2 = game.add.sprite(855, 453, symbols.BELT);
        //this.pt3Belt2.frame = 1;
        //this.pt3Belt2.anchor.setTo(0.5);
        //this.pt3Belt2.scale.setTo(0.4);
        //boardPopUp.addToBoard(this.pt3Belt2);

        //this.pt3spotLight = game.add.sprite(655, 265, "paytableLights");
        //this.pt3spotLight.scale.setTo(0.4);
        //this.pt3spotLight.scale.setTo(0.8);
        //boardPopUp.addToBoard(this.pt3spotLight);
        
        //this.parts[2].add(this.pt3_HeaderText);
        //this.parts[2].add(this.pt3Text0);
        //this.parts[2].add(this.pt3Text1);
        //this.parts[2].add(this.pt3Text2);
        //this.parts[2].add(this.pt3Text3);
        //this.parts[2].add(this.pt3Text4);
        //this.parts[2].add(this.pt3Belt0);
        //this.parts[2].add(this.pt3Belt1);
        //this.parts[2].add(this.pt3Belt2);
        //this.parts[2].add(this.pt3spotLight);


        ////PT3
        this.pt3_HeaderText = game.add.bitmapText(game.width / 2, 120, "ObelixProPaytableHeader", langJSON.explanation, 18);
        this.pt3_HeaderText.anchor.setTo(0.5);
        boardPopUp.addToBoard(this.pt3_HeaderText);

        this.pt3Text1 = game.add.bitmapText(game.width / 2, 145, "ObelixProPaytable", langJSON.pt3Text1, 12);
        this.pt3Text1.anchor.setTo(0.5);
        boardPopUp.addToBoard(this.pt3Text1);

        this.pt3Text2 = game.add.bitmapText(370, 300, "ObelixProPaytable", langJSON.pt3Text2, 12);
        boardPopUp.addToBoard(this.pt3Text2);

        this.pt3Text3 = game.add.bitmapText(370, 397, "ObelixProPaytable", langJSON.pt3Text3, 12);
        boardPopUp.addToBoard(this.pt3Text3);

        this.pt3Text4 = game.add.bitmapText(370, 465, "ObelixProPaytable", langJSON.pt3Text4, 12);
        boardPopUp.addToBoard(this.pt3Text4);

        this.pt3Slugger = game.add.sprite(660, 420, "PT_Slugger");
        this.pt3Slugger.anchor.setTo(0.5);

        //this.pt3Text5 = game.add.bitmapText(745, 315, "ObelixProPaytable", langJSON.pt3Text5, 12);
        //boardPopUp.addToBoard(this.pt3Text5);

        //this.pt3Text6 = game.add.bitmapText(760, 345, "ObelixProPaytable", langJSON.pt3Text6, 12);
        //boardPopUp.addToBoard(this.pt3Text6);

        this.pt3Belt0 = game.add.sprite(game.width / 2, 210, symbols.BELT);
        this.pt3Belt0.frame = 1;
        this.pt3Belt0.anchor.setTo(0.5);
        this.pt3Belt0.scale.setTo(0.5);
        boardPopUp.addToBoard(this.pt3Belt0);

        this.pt3Belt1 = game.add.sprite(game.width / 2 - 120, 210, symbols.BELT);
        this.pt3Belt1.frame = 1;
        this.pt3Belt1.anchor.setTo(0.5);
        this.pt3Belt1.scale.setTo(0.5);
        boardPopUp.addToBoard(this.pt3Belt1);

        this.pt3Belt2 = game.add.sprite(game.width / 2 + 120, 210, symbols.BELT);
        this.pt3Belt2.frame = 1;
        this.pt3Belt2.anchor.setTo(0.5);
        this.pt3Belt2.scale.setTo(0.5);
        boardPopUp.addToBoard(this.pt3Belt2);

        this.parts[2].add(this.pt3_HeaderText);
        this.parts[2].add(this.pt3Text1);
        this.parts[2].add(this.pt3Text2);
        this.parts[2].add(this.pt3Text3);
        this.parts[2].add(this.pt3Text4);
        this.parts[2].add(this.pt3Slugger);
        //this.parts[2].add(this.pt3Text5);
        //this.parts[2].add(this.pt3Text6);
        this.parts[2].add(this.pt3Belt0);
        this.parts[2].add(this.pt3Belt1);
        this.parts[2].add(this.pt3Belt2);

        //PT4
        this.pt4Winline = game.add.sprite(game.width / 2, vm.ActiveGameHeight / 2 + 20, "PT_Winlines");
        this.pt4Winline.anchor.setTo(0.5);
        this.pt4Text = game.add.bitmapText(game.width / 2 - 50, 110, "ObelixProPaytable", langJSON.winLine, 20);
        boardPopUp.addToBoard(this.pt4Text);

        this.parts[3].add(this.pt4Text);
        this.parts[3].add(this.pt4Winline);

        //PT5
        this.pt5_HeaderText = game.add.bitmapText(game.width / 2, 120, "ObelixProPaytableHeader", langJSON.pt5_HeaderText, 28);
        this.pt5_HeaderText.anchor.setTo(0.5);
        boardPopUp.addToBoard(this.pt5_HeaderText);

        this.pt5Text1 = game.add.bitmapText(350, 180, "ObelixProPaytable", langJSON.pt5Text1, 12);
        boardPopUp.addToBoard(this.pt5Text1);

        this.pt5Text2 = game.add.bitmapText(350, 215, "ObelixProPaytable", langJSON.pt5Text2, 12);
        boardPopUp.addToBoard(this.pt5Text2);

        this.pt5Text3 = game.add.bitmapText(350, 265, "ObelixProPaytable", langJSON.pt5Text3, 12);
        boardPopUp.addToBoard(this.pt5Text3);

        this.pt5Text4 = game.add.bitmapText(350, 300, "ObelixProPaytable", langJSON.pt5Text4, 12);
        boardPopUp.addToBoard(this.pt5Text4);

        this.parts[4].add(this.pt5_HeaderText);
        this.parts[4].add(this.pt5Text1);
        this.parts[4].add(this.pt5Text2);
        this.parts[4].add(this.pt5Text3);
        this.parts[4].add(this.pt5Text4);


        for (var j = 0; j < this.parts.length; j++) {
            this.parts[j].visible = (j === 0);
            boardPopUp.addToBoard(this.parts[j]);
        }

        
        //game.world.bringToTop(boardPopUp.board);
    },

    _positionSymbol: function (x, y, key, scale)
    {
        scale = scale === undefined ? 0.7 : scale;
        var symbol = game.add.sprite(x, y, key);
        symbol.frame = 1;
        symbol.scale.setTo(scale);
        return symbol;
    },

    _initializeInputs: function (onClickCallback, context)
    {
        this.btn_leftArrow.inputEnabled = true;
        this.btn_leftArrow.events.onInputDown.add(function () { this.changePage(false) }, this);

        this.btn_rightArrow.inputEnabled = true;
        this.btn_rightArrow.events.onInputDown.add(function () { this.changePage(true) }, this);

        this.btn_close.inputEnabled = true;
        this.btn_close.events.onInputDown.add(this._onCloseClicked, this);

        if (onClickCallback && context) boardPopUp.onClosed.addOnce(onClickCallback, context);

        audioManager.addClickUpDownSound(this.btn_leftArrow, this.btn_rightArrow, this.btn_close);
    },

    _onCloseClicked: function ()
    {
        textureOverlay.fade(0x000000, undefined, 0, undefined, 350, true);
        boardPopUp.hide();
        game.world.bringToTop(boardPopUp.board);
    },

    _addPage: function ()
    {
        this.parts.push(game.add.group());
    },

    _addToLastPage: function (item)
    {
        this.parts[this.parts.length - 1].add(item);
    },

    changePage: function (isRightArrow)
    {
        if (isRightArrow)
        {
            this._currentIndex++;
            if (this._currentIndex === this.parts.length) this._currentIndex = 0;
        }
        else
        {
            this._currentIndex--;
            if (this._currentIndex === -1) this._currentIndex = this.parts.length - 1;
        }
        this.showPage(this._currentIndex);
    },

    showPage: function (pageIndex)
    {
        for (var j = 0; j < this.parts.length; j++)
        {
            this.parts[j].visible = (j === pageIndex);
        }
    },

    toggleAll: function (visibleState)
    {
        for (var j = 0; j < this._pages.length; j++)
        {
            this._pages[j].visible = visibleState;
        }
    }
};

var payTableBoard = new PayTableBoard();