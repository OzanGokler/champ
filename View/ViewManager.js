﻿var baseWidth = 1200;
var baseHeight = 675;

var gameWidth = 1200;
var gameHeight = 675;
var backgroundColor = "#000000";
var game = new Phaser.Game(gameWidth, gameHeight, Phaser.CANVAS, "kitchenGame");
var gameStates = { main: "gameUI", bonus: "bonusUI", preloader: "preloaderUI", loading: "loading" };

function ViewManager() {
    Object.defineProperty(this, "ActiveGameHeight", {
        get: function () { return this._activeGameHeight; },
    });
}

ViewManager.prototype = {
    _previousWidth: -1,
    _previousHeight: -1,
    _resizingDisplay: null,
    _invalidDisplay: null,
    _reAddTime: -1,
    _previousScalingOffset: -1,
    _activeGameHeight: -1,
    _isPortrait: undefined,
    onGameScaled: new Phaser.Signal(),

    setOrientation: function () {
        //Maintain aspect-ratio : SHOW_ALL
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        //game.scale.forceOrientation(true, false);

        game.scale.pageAlignVertically = true;
        game.scale.pageAlignHorizontally = true;

        game.scale.refresh();
    },

    goFullScreen: function (failCallback, failContext, successCallback, successContext) {
        console.log("goFullScreen");
        game.scale.onFullScreenError.removeAll();
        game.scale.onFullScreenChange.removeAll();
        game.scale.onFullScreenError.add(failCallback, failContext);
        game.scale.onFullScreenChange.add(successCallback, successContext);
        game.scale.startFullScreen(true);

        game.scale.refresh();
    },

    stopFullScreen: function (successCallback, successContext) {
        console.log("stopFullScreen");
        game.scale.onFullScreenChange.removeAll();
        game.scale.onFullScreenChange.add(successCallback, successContext);
        game.scale.stopFullScreen(true);

        game.scale.refresh();
    },

    updateBetUI: function (betLevel, winLines, coinValue) {
        console.log("viewManager - vmUpdateBetUI");

        gameUI.updateBetUI(betLevel, winLines, coinValue);
    },

    startState: function (currentState, clearWorld, clearCache, arg1, arg2) {
        console.log("viewManager - startState: " + currentState);
        clearWorld = clearWorld || true;
        clearCache = clearCache || false;
        game.state.start(currentState, clearWorld, clearCache, arg1, arg2);
    },

    _preCrop: function () {
        if (!this._resizingDisplay) this._resizingDisplay = $("#resizing")[0];
        //console.log("preCrop");
        this._resizingDisplay.style.display = "block";
        game.stage.visible = false;
        game.scale.minHeight = 0;
        game.scale.maxHeight = 0;
        game.scale.minWidth = 0;
        game.scale.maxWidth = 0;
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.refresh();

        game.world.setBounds(0, 0, 0, 0);
        if (this._reAddTime === -1) {
            this._reAddTime = game.time.now;
        }
        if (game.time.now - this._reAddTime > 500) {
            this._onSizeChanged();
            game.scale.setResizeCallback(vm._cropGame, vm);
            vm._cropGame();
            console.log("pre crop finishes, hide resizing");
        }
    },

    _onSizeChanged: function () {
        console.log("_onSizeChanged: " + this._isPortrait);
        console.log(window.innerHeight + ":" + window.innerWidth);
        //Portrait mode
        if (!Phaser.Device.desktop && window.innerHeight > window.innerWidth) {
            if (!this._isPortrait) {
                console.log("Switched to portrait mode");
                gameHeight = baseHeight * 2;
                game.scale.setGameSize(gameWidth, gameHeight);
                this._activeGameHeight = game.height / 2;
                this._isPortrait = true;
            }
        }
        else {
            if (this._isPortrait || this._isPortrait === undefined) {
                console.log("Switched to landscape mode");
                gameHeight = baseHeight;
                game.scale.setGameSize(gameWidth, gameHeight);
                this._activeGameHeight = game.height;
                this._isPortrait = false;
            }
        }
    },

    //Very important magic.
    _cropGame: function () {
        if (!this._resizingDisplay) this._resizingDisplay = $("#resizing")[0];

        this._reAddTime = -1;
        this._resizingDisplay.style.display = "none";
        game.stage.visible = true;
        //Maximum offset to leave for each side.
        //Left side is clipped while right continues out of screen.
        var maxOffset = 220;
        var offset = 0;
        //Used to calculate width from height or vice verso by multiplying or dividing.
        var whRatio = (gameWidth / gameHeight);
        //Used to scale maxOffset value in extreme resolutions.
        var minSuppportedWidth = 900;

        //Initial width height calculation.
        var height = window.innerHeight;
        var width = height * whRatio;
        //Calculate initial offset
        offset = width - window.innerWidth;
        //Ignore negative offsets
        if (offset < 0) offset = 0;

        //If the height of the window is bigger than its width or we have offset that is higher than our max scalingOffset value 
        //We have to activate portrait or squarish scaling mode.
        if (offset > maxOffset) {
            console.log("inversed scale: " + offset + "(" + maxOffset + ")");
            //Scale maxOffset up or down depending on if we are smalller than our min supported width or not.
            if (window.innerWidth < minSuppportedWidth) offset = maxOffset * (window.innerWidth / minSuppportedWidth);
            else offset = maxOffset / (window.innerWidth / minSuppportedWidth);

            //Use width as reference to calculate height since we are in a screen that is portrait or square.
            width = window.innerWidth + offset;
            height = width / whRatio;
        }
        else console.log("normal scale");

        //console.log(this._previousWidth + ":" + window.innerWidth);

        //Reset view and do a complete redraw if we have scaled more than 10 pixels.
        //Phaser sometimes does not update bounds value properly. Resetting size to a very small number(_preCrop) and rescaling solves that issue.
        if (Math.abs(this._previousWidth - window.innerWidth) > 10 || Math.abs(this._previousHeight - window.innerHeight) > 10) {
            if (this._previousWidth !== -1) {
                game.scale.setResizeCallback(vm._preCrop, vm);
                vm._preCrop();
            }
            this._previousWidth = window.innerWidth;
            this._previousHeight = window.innerHeight;
            return;
        }

        //Set height, width and offset values.
        game.scale.minHeight = height;
        game.scale.maxHeight = height;
        game.scale.minWidth = width;
        game.scale.maxWidth = width;
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.refresh();

        //Used to clip left side of the game.
        game.world.setBounds(offset * (game.scale.scaleFactor.x / 2), 0, width, height);

        //This value ise used to calculate some dynamic positions in game.
        game.scalingOffset = offset * game.scale.scaleFactor.x / 2;
        if (this._previousScalingOffset !== game.scalingOffset) {
            console.log("scalingOffset updated: " + game.scalingOffset);
            this._previousScalingOffset = game.scalingOffset;
            this.onGameScaled.dispatch(this._isOrientationRecentlyChanged);
        }
        this._previousScalingOffset = game.scalingOffset;
    },

    activateCustomScaling: function () {
        this._onSizeChanged();
        vm.setOrientation();
        game.scale.setResizeCallback(vm._cropGame, vm);
        vm._cropGame();
        vm.onGameScaled.dispatch();
    }
}

vm = new ViewManager();

Main = function () { };

Main.prototype = {
    preload: function () {
        //TODO: Always make sure to comment below lines before pushing your changes. 
        //Force desktop mode.
        //Phaser.Device.desktop = false;
        //console.log("NOT DESKTOP");        //Video? What is Video?
        //Phaser.Device.mp4Video = false;
        //Allow FullScreen
        //game.scale.compatibility.supportsFullScreen =false;
        //game.add.plugin(Phaser.Plugin.Debug);

        game.time.desiredFps = 30;

        game.load.json("debugSettings", "Assets/Text Assets/debugSettings.json?v=" + Date.now());

        game.load.script(gameStates.preloader, "View/PreloaderUI.js?v=" + Date.now());
    },

    create: function () {
        debugUtils.debugSettings = game.cache.getJSON("debugSettings");

        if (!debugUtils.debugSettings.showLogs) {
            console.log = function () { };
            console.warn = function () { };
            console.error = function () { };
            console.trace = function () { };
        }
        if (debugUtils.debugSettings.timeFactor > 0) game.time.slowMotion = 1 / debugUtils.debugSettings.timeFactor;
        game.scale.pageAlignVertically = true;
        game.scale.pageAlignHorizontally = true;
        game.scale.maxWidth = 1600 * 2;
        game.scale.maxHeight = 900 * 2;
        console.log("viewManager - create");

        game.state.add(gameStates.preloader, PreloaderUI);
        game.state.start(gameStates.preloader);
    }
};

game.state.add("Main", Main);
game.state.start("Main");