﻿var BoardPopUp = function ()
{
    this.board = game.add.group();
    this.board.y = -300;
    this.state = this.states.CLOSED;
};

BoardPopUp.prototype = {
    board: null,
    _isVisible: false,
    states: { CLOSED: 0, OPENED: 1, CLOSING: 2, SHOWING: 3 },
    state: undefined,
    _introTween: undefined,
    _onTapEvent: undefined,
    _autoHideEvent: undefined,
    _cancellableIntroTweens: new Array(),
    _autoHide: false,
    _dismissOnEmptyClick: false,
    _dismissOnBoardClick: false,
    _fadeTexture: null,
    _dismissHotKey: null,
    _dismissHotkeyEvent:null,
    _boardBackground: null,

    onOpened: new Phaser.Signal(),
    onClosed: new Phaser.Signal(),
    onDismiss: new Phaser.Signal(),

    show: function (autoHide, dismissOnEmptyClick, dismissOnBoardClick,boardName)
    {
        if (this.state === this.states.CLOSED)
        {
            this._fadeTexture = textureOverlay.fade(0x000000, undefined, 0.4, 0, 350, true).target;
            this._autoHide = autoHide;
            this._dismissOnEmptyClick = dismissOnEmptyClick === undefined ? true : dismissOnEmptyClick;
            this._dismissOnBoardClick = dismissOnBoardClick === undefined ? true : dismissOnBoardClick;
            this.state = this.states.SHOWING;
            this._isVisible = true;
            this.board.visible = true;
            this._positionUI(boardName);
            this._initializeInputs();
        }
    },

    hide: function ()
    {
        if (this.state === this.states.SHOWING || this.state === this.states.OPENED)
        {
            textureOverlay.fade(0x000000, undefined, 0, undefined, 350, true);
            game.world.bringToTop(this.board);

            this._fadeTexture.events.onInputDown.remove(this._onTapEvent);
            this._boardBackground.events.onInputDown.removeAll();
            game.tweens.remove(this._introTween);
            if (this._autoHideEvent) game.time.events.remove(this._autoHideEvent);

            this._dismissHotKey.onDown.remove(this._dismissHotkeyEvent);

            for (var i = 0; i < this._cancellableIntroTweens.length; i++)
            {
                game.tweens.remove(this._cancellableIntroTweens[i]);
            }
            this._cancellableIntroTweens.splice(0, this._cancellableIntroTweens.length);

            var outTween = game.add.tween(this.board).to({ y: -1000 }, 500, Phaser.Easing.Linear.Out, true);
            outTween.onComplete.addOnce(function ()
            {
                this.state = this.states.CLOSED;
                this.board.visible = false;

                this.onOpened.removeAll();
                this.onDismiss.removeAll();
                this.onClosed.dispatch();
                this.onClosed.removeAll();

                this.board.destroy(true);
                console.log("BOARD DESTROY");
                
            }, this);
        }
        gameUI.Interface.toggleAllButtons(true);
    },

    addToBoard: function (newItem)
    {
        this.prepareGroup();
        this.board.add(newItem);
    },

    removeFromBoard: function (newItem)
    {
        this.board.remove(newItem);
    },

    _positionUI: function (boardName)
    {
        this.prepareGroup();
        
        this._boardBackground = game.add.sprite(game.width / 2, vm.ActiveGameHeight / 2, boardName);
        this._boardBackground.anchor.setTo(0.5, 0.5);
        this.board.add(this._boardBackground);
        this.board.sendToBack(this._boardBackground);

        this.board.scale.setTo(0.8, 0.8);
        this.board.position.setTo(140, -500);

        game.tweens.removeFrom(this.board, true);
        this._introTween = game.add.tween(this.board).to({ y: 90 }, 1250, Phaser.Easing.Bounce.Out, true);
        this._introTween.onComplete.addOnce(function ()
        {
            this.state = this.states.OPENED;
            this._boardBackground.inputEnabled = true;
            this.onOpened.dispatch();
        }, this);
        if (this._autoHide)
        {
            this._autoHideEvent = game.time.events.add(2000, function ()
            {
                this.hide();
                this._dismiss();
            }, this);
        }
        this._cancellableIntroTweens.push(this._introTween);
        game.world.bringToTop(this.board);
    },

    prepareGroup: function ()
    {
        if (!this.board || !this.board.game)
        {
            this.board = game.add.group();
            this.board.visible = false;
        }
    },

    _initializeInputs: function ()
    {
        this._boardBackground.inputEnabled = false;
        if (this._dismissOnEmptyClick)
        {
            this._onTapEvent = this._fadeTexture.events.onInputDown.addOnce(this._dismiss, this).getListener();
        }
        if (this._dismissOnBoardClick)
        {
            this._boardBackground.events.onInputDown.add(this._dismiss, this);
        }

        this._dismissHotKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this._dismissHotkeyEvent = this._dismissHotKey.onDown.addOnce(this._dismiss, this).getListener();
    },

    _dismiss: function ()
    {
        boardPopUp.hide();
        this.onDismiss.dispatch();
    }
};

boardPopUp = new BoardPopUp();