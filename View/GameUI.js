﻿function GameUI() {
    gameUI = this;
    if (Phaser.Device.desktop) this._interface = new GameInterfaceDesktop();
    else this._interface = new GameInterfaceMobile();
    Object.defineProperty(this, "Interface", {
        get: function () { return this._interface; }
    });
};

GameUI.prototype = {
    bg: null,
    cascade: null,
    frontRail: null,
    _interface: null,
    bg_betUI: null,
    winLinesText: null,
    winLine: null,

    rowCount: 3,
    columnCount: 5,
    symbolsCache: null,
    piecePool: new Pool(50),
    flarePool: new Pool(20),
    lightStreakPool: new Pool(20),
    lightStreakTween: null,
    startPaddingX: 345,
    startPaddingY: 290,
    symbolsPaddingX: 130,
    symbolsPaddingY: 130,

    setCompletedCounter: 0,

    startHeight: -500,
    exitHeight: game.height + 500,
    fallDuration: 325,
    dropDuration: 325,
    symbolScale: 0.68,

    darkOverlay: null,
    betUiGroup: null,
    defaultGroup: null,
    firstLayer: null,

    uiGroup: null,
    needAutoSpin: false,
    betLvelMax: null,
    winLinesMax: null,
    betLevelChangeValue: 1,
    winLineChangeValue: 1,
    isAutoSpinning: false,
    fadeInColor: null,
    fadeInTextureKey: "",
    spinCompletedQueue: new Array(),
    isFirstSpin: true,
    _normalSpinUiGroup: null,
    _winLineChangeDelay: null,
    _spotlights: new Array(),
    _flares: new Array(),
    _portraitMask: null,

    doFirstSpin: function(forceDummySpin) {
        forceDummySpin = forceDummySpin === undefined ? false : forceDummySpin;
        gm.isDummySpin = forceDummySpin || (!gm.canSpin());
        debugUtils.log("this.needsRestore " + this.needsRestore);
        debugUtils.log("gm.isDummySpin " + gm.isDummySpin);
        debugUtils.log("forceDummySpin " + forceDummySpin);
        if (this._interface.needsRestore) {
            this._interface.needsRestore = false;
            debugUtils.log("Restore");
            var spinArray = new Array(this.rowCount * this.columnCount);
            var counter = 0;
            for (var column = 0; column < this.columnCount; column++) {
                for (var row = 0; row < this.rowCount; row++) {
                    spinArray[counter] = this.symbolsCache[row][column].sprite.key;
                    counter++;
                }
            }
            this.spin(spinArray);
        } else this.requestNewSpin();
    },

    init: function(fadeColor, fadeTextureKey) {
        this.fadeInColor = fadeColor;
        this.fadeInTextureKey = fadeTextureKey;
    },

    create: function() {
        inputController.addInputLock();
        debugUtils.log("GameUI - create");
        if (gameUI === undefined) gameUI = this;

        gm.CurrentSpinState = gm.spinStates.idle;
        game.tweens.frameBased = true;
        this._positionUI();
        //this._interface.positionUI();
        this._initializeInputs();

        //balanceUI.setBalanceBar();
        this._updateDrawOrder(false);

        this._interface.updateSpinUI(gm.getAutoSpinValue());
        gm.requestToken(this.onTokenRequestCompleted, this);

        this._interface.toggleAllButtons(false, false);

        introductionUI._positionUI();

    },

    shutdown: function() {
        winLineHelper.dispose();
    },

    onTokenRequestCompleted: function() {
        debugUtils.log("onTokenRequestCompleted");
        gm.requestInitializationData(this._onInitDataLoaded, this);
    },

    _onInitDataLoaded: function(data) {
        debugUtils.log("_onInitDataLoaded");
        gm.coinValues = data.conf.CoinValues;

        if (gm.coinValueIndex == null) gm.coinValueIndex = data.conf.CoinValueDefaultIndex;
        if (gm.winLinesValue == null) gm.winLinesValue = data.conf.WinLineBetDefault;
        if (gm.betLevelValue == null) gm.betLevelValue = data.conf.BetLevelDefault;

        gm.bigWinRatio = data.conf.BigWinRatio;

        this.winLinesMax = data.conf.WinLineBetMax;
        this.betLevelMax = data.conf.BetLevelMax;
        gm.autoSpinValues = data.conf.AutoSpinValues;

        this._interface.updateBetUI(gm.betLevelValue, gm.winLinesValue, gm.coinValues[gm.coinValueIndex]);
        gm.currentCredits = data.credits;
        this._interface.updateBalanceUI(data.realMoney, data.credits, gm.TotalBetInRealMoney, gm.CumulativeRealMoneyWon);

        this._interface.updateSpinUI(gm.SpinsLeft);
        if (this.isFirstSpin) this.doFirstSpin(this.isFirstSpin);
        else this._onProcessSpinCompletedQueue();

        //gm.spinStates.bigWin.init(500);
        //gm.spinStates.bigWin.begin();
    },

    _positionUI: function() {
        //ekrana gorselleri yerlestirir.
        debugUtils.log("GameUI - _positionUI");

        var uiScale = 0.85;
        this.defaultGroup = game.add.group();
        this.uiGroup = game.add.group();
        this.bg = game.add.sprite(game.width / 2 - 100, vm.ActiveGameHeight / 2 + 120, "background");
        this.bg.width = game.width;
        this.bg.height = game.height;
        this.bg.anchor.setTo(0.5, 0.5);
        this.bg.scale.setTo(0.6);

        this.cascade = game.add.sprite(game.width / 2, vm.ActiveGameHeight / 2 - 5, "cascade");
        this.cascade.anchor.setTo(0.5);
        this.cascade.scale.setTo(uiScale);

        this.frontRail = game.add.sprite(game.width / 2 + 23, vm.ActiveGameHeight / 2 - 265, "railFront");
        this.frontRail.anchor.setTo(0.5);
        this.frontRail.scale.setTo(uiScale);

        this.defaultGroup.add(this.cascade);
        this.defaultGroup.add(this.frontRail);

        //To prevent recreation of this array on restore.
        if (this.symbolsCache === null) {
            this.symbolsCache = new Array(this.rowCount);
            //2 dimensional symbolsCache array is created
            for (var i = 0; i < this.symbolsCache.length; i++) {
                this.symbolsCache[i] = new Array(this.columnCount);
            }
        }

        //this.winLinesText = game.add.bitmapText(255, 175, "chalkFont", "", 60);
        //this.winLinesText.anchor.set(0.5);

        //this.btnWinLinesIncrease = game.add.bitmapText(316, 170, "chalkFont", "+", 53);
        //this.btnWinLinesIncrease.anchor.set(0.5);

        //this.btnWinLinesDecrease = game.add.bitmapText(196, 175, "chalkFont", "-", 53);
        //this.btnWinLinesDecrease.anchor.set(0.5);


        if (this.fadeInTextureKey !== undefined && this.fadeInColor !== undefined) {
            textureOverlay.fade(this.fadeInColor, this.fadeInTextureKey, 0, 1, 300).onComplete.addOnce(function() {
                inputController.removeInputLock();
            }, this);
        } else {
            inputController.removeInputLock();
        }

        for (var j = 0; j < 3; j++) {
            var spotlight = game.add.sprite(775 + j * 85, 20 + j * 7, "spotlight" + (j + 1));
            spotlight.scale.setTo(0.7);
            spotlight.frame = 0;
            spotlight.anchor.setTo(0.5);
            this._spotlights.push(spotlight);
            this.defaultGroup.add(spotlight);
        }

        //this.firstLayer = game.add.group();


        //for flare
        game.time.events.repeat(Phaser.Timer.SECOND * 2, Number.POSITIVE_INFINITY, this._spawnFlareEvent, this);
        game.time.events.repeat(Phaser.Timer.SECOND * 4, Number.POSITIVE_INFINITY, this._spawnFlareEvent, this);
        game.time.events.repeat(Phaser.Timer.SECOND * 7, Number.POSITIVE_INFINITY, this._spawnFlareEvent, this);
        this._interface.positionUI();

        this._portraitMask = game.add.graphics(0, 0);
        this._portraitMask.drawRect(0, 0, game.width, vm.ActiveGameHeight);
        this.defaultGroup.mask = this._portraitMask;
        this.bg.mask = this._portraitMask;


    },

    _spawnFlareEvent: function() {
        var ratio = 0.05;
        this._spawnFlare("flare",
            utils.getRandomArbitraryInRange(game.width * ratio, game.width * (1 - ratio)),
            utils.getRandomArbitraryInRange(game.height * ratio, game.height * (1 - ratio)));
    },

    gameUiOutTween: function() {
        game.add.tween(this.defaultGroup).to({ y: -800 }, 2500, Phaser.Easing.Quartic.Out, true);
        game.add.tween(this.uiGroup).to({ y: -800 }, 2500, Phaser.Easing.Quartic.Out, true);
        game.time.events.add(Phaser.Timer.SECOND * 1, function() {
            game.add.tween(this.bg).to({ y: 360 }, 2000, Phaser.Easing.Quartic.Out, true);
        }, this);
    },
    gameUiInTween: function (callback, context) {
        game.time.events.add(Phaser.Timer.SECOND * 0.5, function() {
            game.add.tween(this.defaultGroup).to({ y: 0 }, 2500, Phaser.Easing.Quartic.Out, true);

            var refTween = game.add.tween(this.uiGroup).to({ y: 0 }, 2500, Phaser.Easing.Quartic.Out, true);
            refTween.onComplete.addOnce(callback, context);
        }, this);
        game.time.events.add(Phaser.Timer.SECOND * 1, function() {
            game.add.tween(this.bg).to({ x: game.width / 2 - 100, y: vm.ActiveGameHeight / 2 + 120 }, 2000, Phaser.Easing.Quartic.Out, true);
        }, this);
    },


    _initializeInputs: function() {
        //click vs gibi eventleri ekler.
        debugUtils.log("GameUI - _initializeInputs");
        //this.initializeInput(this.btnPaytable, true, this.showPayTable, this, true);
        this._interface.addMuteClickEvent(true, this.onAudioToggle, this, true);
        this._interface.addPayTableClickEvent(true, this.showPayTable, this, true);

        //this.initializeInput(this.autoSpinText, true, gameUI.onAutoSpinClicked, gameUI, true, true);
        this._interface.addAutoSpinClickEvent(true, gm.increaseAutoSpin, gm);
        this._interface.addAutoSpinIncreaseButtonEvent(true, gm.increaseAutoSpin, gm);
        this._interface.addAutoSpinDecreaseButtonEvent(true, gm.decreaseAutoSpin, gm);
        this._interface.addSpinClickEvent(true, this.onSpinStopClicked, this, true, true);
        this._interface.addBetLevelIncreaseButtonEvent(true, function() { this.onBetLevelChanged(1) }, this);
        this._interface.addBetLevelDecreaseButtonEvent(true, function() { this.onBetLevelChanged(-1) }, this);
        this._interface.addCoinIncreaseButtonEvent(true, function() { this.onCoinChanged(1) }, this);
        this._interface.addCoinDecreaseButtonEvent(true, function() { this.onCoinChanged(-1) }, this);
        this._interface.addBetMaxClickEvent(true, function() { gm.requestBetMax(this._onBetMaxDataReceived, this); }, this);


        var spinHotKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        spinHotKey.onDown.add(this.onSpinStopClicked, this);
    },
    showPayTable: function() {
        this._interface.showPayTable();
    },

    initializeInput: function(item, enabled, method, context, hasSound, altSound) {
        item.inputEnabled = enabled;
        item.events.onInputUp.removeAll();
        item.events.onInputDown.removeAll();
        item.events.onInputUp.add(method, context);
        if (hasSound) {
            if (altSound) audioManager.addClickUpDownAltSound(item);
            else audioManager.addClickUpDownSound(item);
        }
    },

    onAudioToggle: function() {
        if (!game.sound.mute) audioManager.mute();
        else audioManager.unmute();
        //this._interface.btnAudio.frame = game.sound.mute ? 0 : 1;
        this.Interface.Mute();
    },

    _onProcessSpinCompletedQueue: function() {
        console.log(this.spinCompletedQueue);
        if (this.spinCompletedQueue.length > 0) {
            var completable = this.spinCompletedQueue.shift();
            completable.setOnCompleteEvent(this._onProcessSpinCompletedQueue, this);
            completable.begin();

            gm.CurrentSpinState = completable;
        } else if (gm.betValue > 0 && gm.cumulativeCreditsWon >= gm.betValue * gm.bigWinRatio) {
            gm.spinStates.bigWin.init(gm.cumulativeCreditsWon);
            gm.spinStates.bigWin.setOnCompleteEvent(this._fireOnQueueFinishedEvent, this);
            gm.spinStates.bigWin.begin();
        } else this._fireOnQueueFinishedEvent();

    },

    _fireOnQueueFinishedEvent: function() {
        //if (gm.freeSpinsLeft() <= 0) gm.clearCumulativeWonAmount(this._interface);
        gm.CurrentSpinState = gm.spinStates.spinned;
        this._onSpinCompletedQueueFinished(undefined);
    },

    onAutoSpinClicked: function() {
        gm.increaseAutoSpin();
    },

    onSpinStopClicked: function() {
        console.log("autospin index " + gm.autoSpinIndex);
        if (introductionUI.bIsOpen===true) return;
        if (!Phaser.Device.desktop && gameUI.Interface.balanceUI.balanceMenu.isOpen) return;
        audioManager.playMainMusic();
        //if (!this._interface.btnSpinStop.inputEnabled) return;
        if (!this.Interface.IsSpinButtonActive) return;
        if (!Phaser.Device.desktop && this.Interface.IsSpinButtonDragged) return;
        if (boardPopUp.state !== boardPopUp.states.CLOSED) return;
        //game.add.tween(this._interface.btnSpinStop.scale).to({ x: 0.75, y: 0.75 }, 150, Phaser.Easing.Linear.In, true, 0, 0, true);
        if (gm.isAutoSpinning) {
            if (gm.CurrentSpinState === gm.spinStates.spinning || gm.CurrentSpinState === gm.spinStates.dropping) {
                console.log("Auto spin stop scheduled.");

                this._interface.toggleSpinButton(false);
            } else {
                console.log("Auto spin stoped scheduled.");
                this.stopAutoSpin();
                return;
            }
        }
        switch (gm.CurrentSpinState) {
        case gm.spinStates.idle:
        case gm.spinStates.spinned:
            if (this.needAutoSpin) return;
            if (gm.IsAutoSpinEnabled) {
                gm.isAutoSpinning = true;
                gm.SpinsLeft = gm.getAutoSpinValue();
            }
            if (gm.currentCredits > gm.TotalBetInCredits) gm.currentCredits -= gm.TotalBetInCredits;
            this._interface.updateBalanceUI(false);
            this._validateAndSpin();
            audioManager.playSpinButton();
            this._interface.toggleAllButtons(false);
            if (!Phaser.Device.desktop) gameUI.Interface.balanceUI.toggleMenu(false);
            break;
        default:
            if (gm.isAutoSpinning) this.stopAutoSpin();
            break;
        }
    },

    _onBetValidatedForSpin: function(canChangeBet) {
        console.log(canChangeBet);
        if (canChangeBet) {
            gm.betValue = gm.betLevelValue /** gm.coinValues[gm.coinValueIndex]*/ * gm.winLinesValue;
            console.log("gm.betValue: " + gm.betValue);
            console.log("SPIN STATE " + gm.CurrentSpinState);
            if (gm.CurrentSpinState === gm.spinStates.spinned) {
                this.needAutoSpin = true;
                this.drop();
            } else this.requestNewSpin();
        } else {
            this.stopAutoSpin();
            this._interface.toggleAllButtons(true, true);
        }
    },

    stopAutoSpin: function() {
        gm.autoSpinIndex = 0;
        this.needAutoSpin = false;
        gm.isAutoSpinning = false;
        gm.SpinsLeft = 0;
        //this._interface.btnSpinStop.text = languageManager.langJSON.spin;
        //this._interface.toggleButton(this._interface.btnSpinStop, false, true);
        this._interface.toggleSpinButton(true, false);
        gm.IsAutoSpinEnabled = false;
    },

    _validateAndSpin: function() {
        gm.requestCanChangeBet(this._onBetValidatedForSpin, this, gm.betLevelValue, gm.coinValueIndex, gm.winLinesValue);
    },

    requestNewSpin: function() {
        gm.requestSpin(this._onSpinDataReceived, this, !gm.isDummySpin, gm.betLevelValue, gm.coinValueIndex, gm.winLinesValue, gm.isDummySpin);
    },


    onBetLevelChanged: function(value) {
        gm.requestSetBetValues(this.onBetLevelChangeResultReceived, this, gm.betLevelValue + value, gm.coinValueIndex, gm.winLinesValue);
    },

    onBetLevelChangeResultReceived: function(result) {
        if (!result) return;
        //Update bet level.
        gm.betLevelValue = result.betLevelIndex;
        if (gm.betLevelValue > this.betLevelMax) gm.betLevelValue = this.betLevelMax;
        else if (gm.betLevelValue <= 1) gm.betLevelValue = 1;
        this._interface.updateBetUI(gm.betLevelValue, gm.winLinesValue, gm.coinValues[gm.coinValueIndex]);
        gm.currentCredits = result.credits;
        this._interface.updateBalanceUI(result.realMoney, result.credits, gm.TotalBetInRealMoney, gm.CumulativeRealMoneyWon);

        this._interface.toggleBetButtons(true, true);

        //this._interface.toggleButton(this._interface.btnBetLevelDecrease, gm.betLevelValue > 1, true);
    },

    onWinLinesChanged: function(value, forcePreview) {
        game.time.events.removeAll(this._winLineChangeDelay);

        gm.winLinesValue += value;
        if (gm.winLinesValue > this.winLinesMax) gm.winLinesValue = this.winLinesMax;
        else if (gm.winLinesValue <= 1) gm.winLinesValue = 1;

        this._interface.updateBetUI(gm.betLevelValue, gm.winLinesValue, gm.coinValues[gm.coinValueIndex]);
        this._interface.updateBalanceUI(undefined, undefined, gm.TotalBetInRealMoney, gm.CumulativeRealMoneyWon);

        this._interface.toggleBetButtons(true, true);
        this._interface.toggleButton(this._interface.btnWinLinesDecrease, gm.winLinesValue > 1, true);

        this._winLineChangeDelay = game.time.events.add(500, function() {
            gm.requestSetBetValues(this.onWinLineChangeResultReceived, this, gm.betLevelValue, gm.coinValueIndex, gm.winLinesValue, forcePreview);
        }, this);
    },

    onWinLineChangeResultReceived: function(result, forcePreview) {
        if (!result) return;
        //Ask for all win lines with current selection to preview them.
        gm.requestWinLines(this.onShowWinLineResultReceived, this, gm.winLinesValue);
    },

    onShowWinLineResultReceived: function(winLines) {
        if (!winLines) return;
        var tween = winLineHelper.hideAllWinLines(undefined, true, true);
        if (tween)
            tween.onComplete.addOnce(function() {
                this.previewWinLines(winLines);
            }, this);
        else this.previewWinLines(winLines);
    },

    previewWinLines: function(winLines) {
        winLineHelper.hideAllWinLines(0, true, false);
        var tweenRef = undefined;
        for (var i = 0; i < winLines.length; i++) {
            var startRow = winLineHelper.getWinLineStartRow(winLines[i]);
            if (!tweenRef) tweenRef = winLineHelper.showWinLine(startRow, i, 500);
            else tweenRef = winLineHelper.showWinLine(startRow, i, 500);
        }
        if (tweenRef) {
            tweenRef.tween.onComplete.addOnce(function() {
                var hideTweenRef = winLineHelper.hideAllWinLines(750, true);
                if (hideTweenRef) hideTweenRef.onComplete.addOnce(inputController.removeInputLock, inputController);
            }, this);
        } else inputController.removeInputLock();
    },

    onCoinChanged: function(value) {
        gm.requestSetBetValues(this.onCoinValuehangeResultReceived, this, gm.betLevelValue, gm.coinValueIndex + value, gm.winLinesValue);
    },

    onCoinValuehangeResultReceived: function(result) {
        if (!result) return;
        gm.coinValueIndex = result.coinValueIndex;
        if (gm.coinValueIndex >= gm.coinValues.length) gm.coinValueIndex = gm.coinValues.length - 1;
        else if (gm.coinValueIndex <= 0) gm.coinValueIndex = 0;
        this._interface.updateBetUI(gm.betLevelValue, gm.winLinesValue, gm.coinValues[gm.coinValueIndex]);

        gm.currentCredits = result.credits;
        this._interface.updateBalanceUI(result.realMoney, result.credits, gm.TotalBetInRealMoney, gm.CumulativeRealMoneyWon);

        this._interface.toggleBetButtons(true);

        //this._interface.toggleButton(this._interface.btnCoinDecrease, gm.coinValueIndex > 0, true); 
        this._interface.decreaseCoinBetButton();
    },

    _onSpinDataReceived: function(response, serverData) {
        gm.isDummySpin = false;
        gm.spinReturnData = serverData;
        gm.clearCumulativeWonAmount(this._interface);

        if (gm.spinReturnData) {
            console.log(gm.spinReturnData);
            //TODO: compare old Champ Project
            gm.currentCredits = gm.spinReturnData.creditsBeforeWin;
            this._interface.updateBalanceUI(false);
            this.spin(response);
        } else {
            this._interface.toggleAllButtons(true, true);
        }
    },

    _onBetMaxDataReceived: function(serverData) {
        if (!serverData) return;
        gm.winLinesValue = serverData.winLine;
        gm.coinValueIndex = serverData.coinIndex;
        gm.betLevelValue = serverData.betLevel;
        gm.currentCredits = serverData.credits;
        this._interface.updateBalanceUI(serverData.realMoney, serverData.credits, gm.TotalBetInRealMoney, gm.CumulativeRealMoneyWon);

        this._interface.toggleBetButtons(true, true);
        gm.requestWinLines(this.onShowWinLineResultReceived, this, gm.winLinesValue);

        this._interface.updateBetUI(gm.betLevelValue, gm.winLinesValue, gm.coinValues[gm.coinValueIndex]);
    },

    _lightStreakStart: function(tweenStart) {
        this._spawnlightStreak("lightStreak", -50, -10, 0, -60, tweenStart);
        this._spawnlightStreak("lightStreak", -50, 200, 1000, -60, tweenStart);

        this._spawnlightStreak("lightStreak", game.width + 50, -10, 0, 60, tweenStart);
        this._spawnlightStreak("lightStreak", game.width + 50, 200, 1000, 60, tweenStart);
    },

    _spawnlightStreak: function(textureKey, posX, posY, delay, angle, tweenStart) {
        var lightStreakSprite = this.lightStreakPool.getFromPool(textureKey);

        if (!lightStreakSprite) {
            lightStreakSprite = game.add.sprite(posX, posY, textureKey);
            lightStreakSprite.anchor.setTo(0.5);
        } else {
            if (!lightStreakSprite.game) {
                lightStreakSprite.destroy();
                lightStreakSprite = game.add.sprite(posX, posY, textureKey);
            } else lightStreakSprite.reset(posX, posY);
        }

        lightStreakSprite.alpha = 0.5;
        lightStreakSprite.scale.setTo(0.5);
        lightStreakSprite.anchor.setTo(0.5, 0);

        //tweens here
        if (tweenStart === true) {
            this.lightStreakTween = game.add.tween(lightStreakSprite).to({ angle: angle }, 1000, Phaser.Easing.Linear.None, true, delay, 5);
            this.lightStreakTween.yoyo(true);
            this.lightStreakTween.onComplete.addOnce(function() {
                this.context.flarePool.addToPool(this.sprite);
            }, { context: this, sprite: lightStreakSprite });

        }
    },

    _spawnFlare: function(textureKey, posX, posY) {
        var flareSprite = this.flarePool.getFromPool(textureKey);

        if (!flareSprite) {
            flareSprite = game.add.sprite(posX, posY, textureKey);
            flareSprite.scale.setTo(0, 0);
            flareSprite.anchor.setTo(0.5);
        } else {
            if (!flareSprite.game) {
                flareSprite.destroy();
                flareSprite = game.add.sprite(posX, posY, textureKey);
            } else flareSprite.reset(posX, posY);
        }

        var tween0 = game.add.tween(flareSprite.scale).to({ x: 0.5, y: 0.5 }, 125, Phaser.Easing.Linear.Out, true);
        var tween1 = game.add.tween(flareSprite.scale).to({ x: 0, y: 0 }, 125, Phaser.Easing.Linear.Out, false);
        tween1.onComplete.addOnce(function() {
            this.context.flarePool.addToPool(this.sprite);
        }, { context: this, sprite: flareSprite });

        tween0.chain(tween1);
        this._interface.firstLayer.add(flareSprite);
    },

    _spawnSymbol: function(textureKey, posX, posY, column, row) {
        var newSprite = this.piecePool.getFromPool(textureKey);
        if (!newSprite) {
            newSprite = game.add.sprite(posX, posY, textureKey);
        } else {
            if (!newSprite.game) {
                newSprite.destroy();
                newSprite = game.add.sprite(posX, posY, textureKey);
            } else newSprite.reset(posX, posY);
        }

        newSprite.anchor.setTo(0.5, 0.5);
        newSprite.scale = { x: this.symbolScale, y: this.symbolScale * 1.1 };

        this.defaultGroup.add(newSprite);

        var newItem;
        switch (textureKey) {
        case symbols.ExpandingWild:
            newItem = new ExpandingWildSymbol(newSprite, column, row);
            break;
        case symbols.BELT:
            newItem = new ScatterBeltSymbol(newSprite, column, row);
            break;
        default:
            newItem = new BasicSymbol(newSprite, column, row);
        }
        return { newItem: newItem };
    },

    _setSymbolSpinAnims: function(sprite, column, row) {
        var targetY = this.startPaddingY + (row * this.symbolsPaddingY) - (sprite.height / 2);
        var delay = utils.getRandomArbitraryInRange(50, 200) + (this.symbolsCache.length - row - 1) * 350;
        var overthrowTween = game.add.tween(sprite).to({ y: targetY }, 100, Phaser.Easing.Linear.Out, true, delay, 0, false);
        overthrowTween.frameBased = true;

        //Smalll bounce back to make scale look "a bit" nicer.
        var jumpTween = game.add.tween(sprite).to({ y: targetY - utils.getRandomArbitraryInRange(75, 100) }, 200, Phaser.Easing.Quadratic.Out, false, 0, 0, false);
        jumpTween.frameBased = true;

        var bounceTween = game.add.tween(sprite).to({ y: targetY }, this.fallDuration, Phaser.Easing.Bounce.Out, false, 0, 0, false);
        bounceTween.frameBased = true;
        bounceTween.onComplete.addOnce(this._onSpinCompleted, this);
        overthrowTween.chain(jumpTween);
        jumpTween.chain(bounceTween);

        if (row === this.rowCount - 1) {
            overthrowTween.onComplete.addOnce(function(target) {
                audioManager.playReelStopSound();
                target.frame = 1;
            }, this);
        } else {
            overthrowTween.onComplete.addOnce(function(target) {
                target.frame = 1;
            }, this);
        }
    },

    spin: function(spinArray) {
        if (!gm.isDummySpin && !this.isFirstSpin) {
            if (gm.SpinsLeft <= Number.MAX_VALUE) {
                if (gm.SpinsLeft > 0) {
                    gm.SpinsLeft = gm.SpinsLeft - 1;
                    if (gm.SpinsLeft === 0) {
                        //this._interface.toggleButton(this._interface.btnSpinStop, false, true);
                        this._interface.toggleSpinButton(true, false);

                    } else {
                        //this._interface.turnIconAnim.play();
                        this._interface.turnAnimPlay();
                    }
                }
            }
        }

        gm.CurrentSpinState = gm.spinStates.spinning;
        var symbolIndex = 0;
        this.setCompletedCounter = 0;

        for (var row = 0; row < this.rowCount; row++) {
            for (var col = 0; col < this.columnCount; col++) {
                symbolIndex = (col * this.rowCount) + row;
                var posX = this.startPaddingX + (col * this.symbolsPaddingX);
                var posY = this.startHeight + (row * this.symbolsPaddingY);
                var spawnResult = this._spawnSymbol(spinArray[symbolIndex], posX, posY, col, row);
                this.setSymbolCache(col, row, spawnResult.newItem);
                this._setSymbolSpinAnims(spawnResult.newItem.sprite, col, row);
            }
        }
        //game.world.bringToTop(this.defaultGroup);
        //game.world.bringToTop(this.uiGroup);
        //game.world.bringToTop(balanceUI.layer);

        this._updateSymbolDrawOrder();
        this._updateDrawOrder();
    },

    //_updateDrawOrder: function (updateSymbolDrawOrder)
    //{
    //    game.world.bringToTop(this.defaultGroup);
    //    game.world.bringToTop(this.uiGroup);

    //    game.world.bringToTop(balanceUI.layer);

    //    if (updateSymbolDrawOrder) this._updateSymbolDrawOrder();
    //},
    _updateDrawOrder: function() {
        utils.updateDrawOrder([
            this._interface.firstLayer,
            this.defaultGroup,
            this._interface.overlayLayer,
            this._interface.uiGroup,
            this._interface.betUiGroup,
            //this.foreGroundgroup,
            this._interface.balanceUI.layer,
            textureOverlay.fadeSprite,
            boardPopUp.board,
            introductionUI.uiGroup,
            gm.spinStates.bigWin.ContainerLayer,
        ])
    },


    _updateSymbolDrawOrder: function() {
        for (var row = 0; row < this.rowCount; row++) {
            for (var col = 0; col < this.columnCount; col++) {
                var sprite = this.symbolsCache[row][col].sprite;
                sprite.parent.bringToTop(sprite);
            }
        }
    },

    drop: function(forceDropAll) {
        gm.CurrentSpinState = gm.spinStates.dropping;
        this._interface.toggleAllButtons(false, true);
        if (gm.isAutoSpinning && gm.SpinsLeft > 0) {
            //this._interface.toggleButton(this._interface.btnSpinStop, true, false);
            this._interface.toggleSpinButton(true, false);
        }
        winLineHelper.hideAllWinLines();
        for (var row = 0; row < this.symbolsCache.length; row++) {
            for (var col = 0; col < this.symbolsCache[row].length; col++) {
                if (this.symbolsCache[row][col]) {
                    var dropTween = this.getDropTween(this.symbolsCache[row][col].sprite, true, (this.symbolsCache[row].length - col) * 75 + (this.symbolsCache.length - row) * 50);
                    dropTween.onStart.addOnce(function(target) { target.frame = 0; }, this);
                    dropTween.onComplete.addOnce(this.onDropCompleted, this);
                } else this.onDropCompleted();
            }
        }
    },

    removeFromSymbolCache: function(col, row) {
        if (this.symbolsCache[row][col]) {
            this.piecePool.addToPool(this.symbolsCache[row][col].sprite);
            this.symbolsCache[row][col] = null;
        } else console.warn("Can't remove " + col + ", " + row + " from symbolsCache. It does not exist.");
    },

    setSymbolCache: function(col, row, symbolItem, removeIfExists) {
        if (removeIfExists && this.symbolsCache[row][col]) removeFromSymbolCache(col, row);
        this.symbolsCache[row][col] = symbolItem;
    },

    _onSpinCompleted: function() {
        this.setCompletedCounter++;
        if (this.setCompletedCounter === this.columnCount * this.rowCount) {
            this.setCompletedCounter = 0;
            gm.CurrentSpinState = gm.spinStates.spinned;

            this._interface.toggleAllButtons(false, true);
            if (gm.isAutoSpinning && gm.SpinsLeft > 0) {
                //this._interface.toggleButton(this._interface.btnSpinStop, true, false);
                this._interface.toggleSpinButton(true, false)
            }

            var bonusTriggered;
            //Check if we have triggered bonus.
            if (gm.spinReturnData.spinResult.ScatterWins !== null) {
                for (var j = 0; j < gm.spinReturnData.spinResult.ScatterWins.length; j++) {
                    var symbolKey = symbols.getSymbol(gm.spinReturnData.spinResult.ScatterWins[j].winSymbol);
                    if (symbolKey === symbols.BELT) bonusTriggered = true;
                }
            }

            if (gm.spinReturnData.spinResult.ExpandingWild) {
                gm.spinStates.expandingWild.init(gm.spinReturnData.spinResult.ExpandingWild,
                    this.findSymbolOfType(ExpandingWildSymbol), this._spotlights, this.defaultGroup);
                this.spinCompletedQueue.push(gm.spinStates.expandingWild);
            }

            //Decide reference row for winline.
            if (gm.spinReturnData.spinResult.Wins !== null) {
                var allWinLines = winLineHelper.getWinLinesWithStartRow(gm.spinReturnData.spinResult.Wins);
                if (allWinLines.length > 0) {
                    gm.spinStates.winLines.init(allWinLines, undefined, false);
                    this.spinCompletedQueue.push(gm.spinStates.winLines);
                }
            }

            if (bonusTriggered) {
                //99 is champBelt point
                audioManager.playSymbolSound(99);
                gm.spinStates.bonusGame.init(this._portraitMask);
                this.spinCompletedQueue.push(gm.spinStates.bonusGame);



            }

            if (gm.spinReturnData.spinResult.ExpandingWild) {
                gm.spinStates.multiplierSubState.init(this._spotlights);
                this.spinCompletedQueue.push(gm.spinStates.multiplierSubState);
            }

            this._onProcessSpinCompletedQueue();
        }
    },

    findSymbolsOfTypeByKey: function(key) {
        var symbols = new Array();
        for (var row = 0; row < this.symbolsCache.length; row++) {
            for (var col = 0; col < this.symbolsCache[row].length; col++) {
                if (this.symbolsCache[row][col].sprite.key === key) symbols.push(this.symbolsCache[row][col]);
            }
        }
        return symbols;
    },

    findSymbolsOfType: function(type) {
        var symbols = new Array();
        for (var row = 0; row < this.symbolsCache.length; row++) {
            for (var col = 0; col < this.symbolsCache[row].length; col++) {
                if (this.symbolsCache[row][col] instanceof type) symbols.push(this.symbolsCache[row][col]);
            }
        }
        return symbols;
    },

    findSymbolOfType: function(type) {
        for (var row = 0; row < this.symbolsCache.length; row++) {
            for (var col = 0; col < this.symbolsCache[row].length; col++) {
                if (this.symbolsCache[row][col] instanceof type) {
                    return this.symbolsCache[row][col];
                }
            }
        }
        return undefined;
    },

    _onSpinCompletedQueueFinished: function(delayAmount) {
        console.log("queue finished: " + gm.CurrentSpinState);
        this._interface.updateBalanceUI();
        this._interface.updateSpinUI(gm.SpinsLeft);
        if (gm.CurrentSpinState !== gm.spinStates.dropping && gm.CurrentSpinState !== gm.spinStates.idle) {
            this.delayedAutoDrop(delayAmount);
        }
    },

    delayedAutoDrop: function(delayAmount) {
        delayAmount = delayAmount === undefined ? 500 : delayAmount;
        game.time.events.add(delayAmount, this.tryAutoDrop, this);
    },

    tryAutoDrop: function() {
        this._interface.updateBalanceUI(undefined, undefined, undefined, gm.CumulativeRealMoneyWon);
        if (gm.canSpin()) {
            this._validateAndSpin();
        } else {
            if (gm.isAutoSpinning) {
                gm.autoSpinIndex = 0;
                gm.isAutoSpinning = false;
            }
            this._interface.toggleAllButtons(true, true);

            if (this.isFirstSpin) this.isFirstSpin = false;
        }
    },

    animateWinSymbol: function(symbolItem) {
        if (symbolItem) {
            var targetSprite = symbolItem.sprite;
            var spriteKey = symbolItem.spriteKey;
            if (spriteKey === "K" ||
                spriteKey === "Q" ||
                spriteKey === "A" ||
                spriteKey === "J" ||
                spriteKey === "Ten") {
                game.tweens.removeFrom(targetSprite);
                targetSprite.frame = 0;
                var tween0 = game.add.tween(targetSprite.scale).to({ x: 0.6 * this.symbolScale, y: 0.6 * this.symbolScale }, 100, Phaser.Easing.Linear.Out, true);
                var tween1 = game.add.tween(targetSprite.scale).to({ x: 1.1 * this.symbolScale, y: 1.1 * this.symbolScale }, 80, Phaser.Easing.Bounce.Out, false);
                var tween2 = game.add.tween(targetSprite.scale).to({ x: this.symbolScale, y: this.symbolScale }, 125, Phaser.Easing.Bounce.Out, false);
                var tween3 = game.add.tween(targetSprite.scale).to({ x: 0.6 * this.symbolScale, y: 0.6 * this.symbolScale }, 100, Phaser.Easing.Linear.Out, false, 125);
                var tween4 = game.add.tween(targetSprite.scale).to({ x: 1.2 * this.symbolScale, y: 1.2 * this.symbolScale }, 80, Phaser.Easing.Linear.Out, false);
                var tween5 = game.add.tween(targetSprite.scale).to({ x: this.symbolScale, y: this.symbolScale }, 125, Phaser.Easing.Linear.Out, false);
                tween0.chain(tween1);
                tween1.chain(tween2);
                tween2.chain(tween3);
                tween3.chain(tween4);
                tween4.chain(tween5);

                tween5.onComplete.addOnce(function() {
                    this.sprite.frame = 1;
                    this.sprite.scale = { x: this.context.symbolScale, y: this.context.symbolScale };
                }, { context: this, sprite: targetSprite });
            } else if (spriteKey !== "expandingWildAnim") {
                if (targetSprite.animations.currentAnim) targetSprite.animations.currentAnim.stop(true, false);
                targetSprite.loadTexture(spriteKey + "anim");
                var anim = targetSprite.animations.add("idle");

                anim.play(30, false);
                anim.onComplete.addOnce(function(targetSprite) {
                    targetSprite.loadTexture(this.targetSpriteKey);
                    targetSprite.animations.frame = 1;
                }, { targetSpriteKey: spriteKey });

                targetSprite.parent.bringToTop(targetSprite);
            }
        }
    },

    showScorePopup: function(data, posX, posY, delay) {
        delay = (delay === undefined ? 0 : delay);
        var scorePopup = game.add.bitmapText(posX, posY, "ObelixProWinAmountWinline", data.toString(), 100);
        scorePopup.anchor.setTo(0.5, 0.5);
        scorePopup.visible = true;

        scorePopup.scale.setTo(0);
        var overThrowTween = game.add.tween(scorePopup.scale).to({ x: 1.2, y: 1.2 }, 350, Phaser.Easing.Linear.In, true, delay);
        var toNormalTween = game.add.tween(scorePopup.scale).to({ x: 1, y: 1 }, 100, Phaser.Easing.Linear.In, false, delay);
        var scaleDown = game.add.tween(scorePopup.scale).to({ x: 0, y: 0 }, 250, Phaser.Easing.Linear.In, false, 375);

        overThrowTween.chain(toNormalTween);
        toNormalTween.chain(scaleDown);

        scaleDown.onComplete.addOnce(scorePopup.kill, this);
    },

    onDropCompleted: function() {
        this.setCompletedCounter++;
        if (this.setCompletedCounter >= this.columnCount * this.rowCount) {
            for (var row = 0; row < this.symbolsCache.length; row++) {
                for (var col = 0; col < this.symbolsCache[row].length; col++) {
                    this.removeFromSymbolCache(col, row);
                }
            }
            this.setCompletedCounter = 0;
            if (gm.CurrentSpinState !== gm.spinStates.scatterBonus) {
                gm.CurrentSpinState = gm.spinStates.idle;
                if (gm.canSpin() || this.needAutoSpin) {

                    console.log("NEEDAUTOSPIN +++++" + this.needAutoSpin);
                    this.needAutoSpin = false;
                    this._validateAndSpin();
                } else {
                    console.log("NO AUTOSPIN");
                    gm.autoSpinIndex = 0;
                    gm.isAutoSpinning = false;
                    this._interface.toggleAllButtons(true, true);
                }
            }
        }

    },

    bonusGameMask: function() {
        if (bonusGameSubState.mainGroup && bonusGameSubState.mainGroup.game) {
            console.log("MASK BONUS");
            bonusGameSubState.mainGroup.mask = this._portraitMask;
        }
    },

    getDropTween: function (sprite, autoStart, delay)
    {
        game.add.tween(sprite.scale).to({ y: 1.1 }, 150, Phaser.Easing.Linear.In, autoStart, delay);
        return game.add.tween(sprite).to({ y: this.exitHeight }, this.dropDuration, Phaser.Easing.Linear.In, autoStart, delay);
    }
};

var gameUI;