﻿function Symbols() { }

Symbols.prototype =
{
    none: null,
    Thunder: "Apollo",
    TheGentleman: "BritishFighter",
    Hurricane: "DragoRussian",
    TheChamp: "SparringPartner",
    KING: "K",
    QUEEN: "Q",
    ACE: "A",
    JACK: "J",
    TEN: "Ten",
    WildStar: "WildStar",
    ExpandingWild: "SluggerWild",
    BELT: "ChampionBelt",

    //RingGirl: new SpecialSymbolTemplate("RingGirl", 3),
    //WildStar: new SpecialSymbolTemplate("WildStar", 1),
    //ChampionBelt: new SpecialSymbolTemplate("ChampionBelt", 3),
    //SluggerWild: new SpecialSymbolTemplate("SluggerWild", 1),

    getSymbol: function (selector) {
        switch (selector) {
            case 0:
                return symbols.TheChamp;
            case 1:
                return symbols.Hurricane;
            case 2:
                return symbols.Thunder;
            case 3:
                return symbols.TheGentleman;
            case 4:
                return symbols.ACE;
            case 5:
                return symbols.KING;
            case 6:
                return symbols.QUEEN;
            case 7:
                return symbols.JACK;
            case 8:
                return symbols.TEN;
            case 10:
                return symbols.BELT;
            case 11:
                return symbols.ExpandingWild;
            case 99:
                return symbols.BELT;
        }
        return null;
    }
};

var symbols = new Symbols();