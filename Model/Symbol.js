﻿function BasicSymbol(sprite, col, row)
{
    this.spriteKey = sprite.key;
    this.sprite = sprite;
    this.col = col;
    this.row = row;
};

BasicSymbol.prototype =
{
    sprite: null,
    spriteKey: "",
    col: -1,
    row: -1
}

var parentScatterDrill = extend(ScatterBeltSymbol, BasicSymbol);
function ScatterBeltSymbol(sprite, col, row)
{
    this.spriteKey = sprite.key;
    this.sprite = sprite;
    this.col = col;
    this.row = row;
};

ScatterBeltSymbol.prototype =
{
    sprite: null,
    spriteKey: "",
    col: -1,
    row: -1
}

var parentScatterDrill = extend(ExpandingWildSymbol, BasicSymbol);
function ExpandingWildSymbol(sprite, col, row)
{
    this.spriteKey = sprite.key;
    this.sprite = sprite;
    this.col = col;
    this.row = row;
};

ExpandingWildSymbol.prototype =
{
    sprite: null,
    spriteKey: "",
    col: -1,
    row: -1
}