﻿function ServerManager() { }

ServerManager.prototype =
{
    //_baseUri: "http://52.49.216.241:80/Champ/",
    _baseUri: "http://localhost:3615/Champ/",
    //<server-ip>
    _token: undefined,
    _onCompletedCallback: new Phaser.Signal(),
    maxRetryCount: 3,
    _currentRetryCount: undefined,
    _requestQueue: new Array(),
    _messageTypes: { OK: 0, NOT_ENOUGH_BALANCE: 100, INVALID_SESSION: 101, INVALID_PARAM: 102, USER_ALREADY_EXISTS: 103, MULTIPLE_REQUESTS_DENIED: 104, INVALID_GAME_SESSION: 105, DB_DOWN: 106 },

    _checkReturnCode: function (resultMessage, ignoreUndefined)
    {
        console.log(resultMessage);
        switch (resultMessage.Code)
        {
            case undefined:
                if (!ignoreUndefined && !alert(resultMessage.Message))
                {
                    window.location.reload();
                }
                break;
            case this._messageTypes.NOT_ENOUGH_BALANCE:
                dialogPopUp.show("outOfCreditsTitle", "outOfCreditsDesc", false);
                break;
            case this._messageTypes.INVALID_SESSION:
            case this._messageTypes.USER_ALREADY_EXISTS:
            case this._messageTypes.INVALID_PARAM:
            case this._messageTypes.MULTIPLE_REQUESTS_DENIED:
            case this._messageTypes.INVALID_GAME_SESSION:
            case this._messageTypes.DB_DOWN:
                if (!alert(resultMessage.Message))
                {
                    window.location.reload();
                }
                break;
        }
    },

    _unlockInputAndfireEvent: function (onCompletedCallback, context, priority, param1, param3, param4, param5) {
        inputController.removeInputLock();
        sm._onCompletedCallback.addOnce(onCompletedCallback, context, priority, param1, param3, param4, param5);
        sm._onCompletedCallback.dispatch();
    },

    _controlRetryStatus: function () {
        if (sm._currentRetryCount === undefined) sm._currentRetryCount = 0;
        else sm._currentRetryCount++;
        if (sm._currentRetryCount > sm.maxRetryCount) {
            if (!alert("Request Failed. Click OK to Refresh the Game!")) { window.location.reload(); }
        }
        else {
            if (sm._currentRetryCount !== 0) inputController.removeInputLock();
            debugUtils.log("Retrying: " + sm._currentRetryCount);
        }
    },

    _tryProcessRequest: function (newRequest, args) {
        //Add request to the queue
        this._requestQueue.push({ request: newRequest, args: args });
        //Proess the request if it is the only one.
        if (this._requestQueue.length === 1) newRequest.apply(sm, args);
    },

    //Do not use, to process the first item.
    _processNextRequest: function () {
        if (this._requestQueue.length > 0) {
            debugUtils.log("Active Request Finished");
            //Remove active request from list since it is completed.
            sm._requestQueue.shift();
            //Any request left to process?
            if (sm._requestQueue.length > 0) {
                debugUtils.log("Consume queued request");
                //Get next request
                var requestInQueue = sm._requestQueue[0];
                //Call it!
                if (requestInQueue.args && requestInQueue.args.length > 0) requestInQueue.request.apply(sm, requestInQueue.args);
                else requestInQueue.request();
            }
            else debugUtils.log("All requests consumed!");
        }
        else debugUtils.log("All requests consumed!");
    },

    makeQueuedRequest: function (requestFuncWithoutParams) {
        if (!requestFuncWithoutParams) return;
        if (arguments.length > 1) {
            var args = Array.prototype.splice.call(arguments, 1);
            this._tryProcessRequest(requestFuncWithoutParams, args);
        }
        else this._tryProcessRequest(requestFuncWithoutParams);
    },

    ////
    requestSimple: function (apiName, params, onCompletedCallback, context)
    {
        console.log(sm._token);
        console.log(this._token);
        sm._controlRetryStatus();
        inputController.addInputLock();
        var uri = this._baseUri + apiName;
        if (params === undefined)
        {
            params = { FullToken: this._token };
        }
        if (typeof params !== "string") params = JSON.stringify(params);
        var dataType = "json";
        var contentType = "application/json";
        $.ajax({
            url: uri,
            type: "POST",
            data: params,
            dataType: dataType,
            contentType: contentType,
            success: function (data)
            {
                sm._currentRetryCount = undefined;
                var resultMessage = undefined;
                var rtValue = undefined;
                if (data)
                {
                    resultMessage = data.resultMessage;
                    if (resultMessage.Code === 0) rtValue = data.Obj;
                    else debugUtils.log(data);
                }
                else debugUtils.log("No Data: " + status);
                sm._checkReturnCode(resultMessage);
                sm._unlockInputAndfireEvent(onCompletedCallback, context, undefined, rtValue);
                sm._processNextRequest();
            },
            error: function () { sm.requestSimple(apiName, params, onCompletedCallback, context); }
        });
    },

    requestExpandingWildResult: function (onCompletedCallback, context)
    {
        this.requestSimple("GetExpandingWildResult", undefined, onCompletedCallback, context);
    },

    requestWinLines: function (onCompletedCallback, context, maxWinLine)
    {
        var params = {
            FullToken: this._token,
            maxWinLine: maxWinLine
        };
        this.requestSimple("GetWinLines", params, onCompletedCallback, context);
    },

    requestInitializationData: function (onCompletedCallback, context) {
        this.requestSimple("GetInitializationData", undefined, onCompletedCallback, context);
    },

    requestCurrentBalance: function (onCompletedCallback, context) {
        this.requestSimple("GetCurrentBalance", undefined, onCompletedCallback, context);
    },

    requestBetMax: function (onCompletedCallback, context) {
        this.requestSimple("GetMaxBet", undefined, onCompletedCallback, context);
    },

    requestSetBetValues: function (onCompletedCallback, context, betLevelIndex, coinValueIndex, winLine, forcePreview) {
        sm._controlRetryStatus();
        inputController.addInputLock();
        var uri = this._baseUri + "SetBetValues";
        var params = {
            FullToken: this._token,
            betLevelIndex: betLevelIndex,
            coinValueIndex: coinValueIndex,
            winLineIndex: winLine
        };

        $.ajax({
            url: uri,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(params),
            success: function (data) {
                sm._currentRetryCount = undefined;
                var resultMessage = undefined;
                var rtValue = undefined;
                if (data) {
                    resultMessage = data.resultMessage;
                    if (resultMessage.Code === 0) rtValue = data.Obj;
                    else debugUtils.log(data);
                }
                else debugUtils.log("No Data: " + status);
                sm._checkReturnCode(resultMessage, true);
                sm._unlockInputAndfireEvent(onCompletedCallback, context, undefined, rtValue, forcePreview);
                sm._processNextRequest();
            },
            error: function () { sm.requestSetBetValues(onCompletedCallback, context, betLevelIndex, coinValueIndex, winLine, forcePreview); }
        });
    },

    requestCanChangeBet: function (onCompletedCallback, context, betLevelIndex, coinValueIndex, winLine) {
        var params = {
            FullToken: this._token,
            betLevelIndex: betLevelIndex,
            coinValueIndex: coinValueIndex,
            winLineIndex: winLine
        };
        this.requestSimple("CanChangeBet", params, onCompletedCallback, context);
    },

    requestToken: function (onCompletedCallback, context) {
        sm._controlRetryStatus();
        inputController.addInputLock();
        if (sm._token === undefined) {
            var uri = this._baseUri + "RequestToken";
            $.ajax({
                url: uri,
                type: "GET",
                success: function (data) {
                    sm._currentRetryCount = undefined;
                    var resultMessage = undefined;
                    var rtValue = undefined;
                    console.log(data);
                    if (data) {
                        resultMessage = data.resultMessage;
                        console.log(resultMessage);
                        if (resultMessage.Code === 0) {
                            sm._token = data.SessionId;
                            console.log(sm._token);
                            rtValue = true;
                        }
                        else debugUtils.log(data);
                    }
                    else debugUtils.log("No Data: " + status);
                    sm._checkReturnCode(resultMessage);
                    sm._unlockInputAndfireEvent(onCompletedCallback, context, undefined, rtValue);
                    sm._processNextRequest();
                },
                error: function () {
                    sm.requestToken(onCompletedCallback, context);
                }
            });
        }
        else {
            sm._unlockInputAndfireEvent(onCompletedCallback, context);
            sm._processNextRequest();
        }
    },

    requestCurrentBet: function (onCompletedCallback, context, asValues) {
        var params = {
            FullToken: this._token,
            asValues: asValues
        };
        this.requestSimple("GetCurrentBet", params, onCompletedCallback, context);
    },

    requestConfiguration: function (onCompletedCallback, context) {
        this.requestSimple("GetConfiguration", undefined, onCompletedCallback, context);
    },

    requestPayTable: function (onCompletedCallback, context) {
        this.requestSimple("GetPayTable", undefined, onCompletedCallback, context);
    },

    requestSpinResult: function (onCompletedCallback, context, allowSpecial, betLevelIndex, coinValueIndex, winLine, dummySpin) {
        sm._controlRetryStatus();
        inputController.addInputLock();
        betLevelIndex = betLevelIndex == undefined ? 0 : betLevelIndex;
        coinValueIndex = coinValueIndex == undefined ? 0 : coinValueIndex;
        winLine = winLine == undefined ? 1 : winLine;
        dummySpin = dummySpin == undefined ? false : dummySpin;
        var uri = spinDebugQueue && spinDebugQueue.length > 0 ? this._baseUri + "GetSpinResultDebug" : this._baseUri + "GetSpinResult";
        var params = {
            FullToken: this._token,
            betLevelIndex: betLevelIndex,
            coinValueIndex: coinValueIndex,
            winLineIndex: winLine,
            dummySpin: dummySpin
        };
        if (spinDebugQueue && spinDebugQueue.length > 0) params.debugMode = spinDebugQueue.shift();
        $.ajax({
            url: uri,
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(params),
            success: function (data) {
                sm._currentRetryCount = undefined;
                var resultMessage = undefined;
                var rtValue = undefined;
                var results = undefined;
                if (data) {
                    resultMessage = data.resultMessage;
                    if (resultMessage.Code === 0) {
                        //console.log(data.Obj.spinResult.View);
                        results = new Array();
                        var view = data.Obj.spinResult.View;
                        for (var i = 0; i < view.length; i++) {
                            for (var j = 0; j < view[i].length; j++) {
                                results.push(symbols.getSymbol(view[i][j]));
                            }
                        }
                        rtValue = data.Obj;
                    }
                    else debugUtils.log(data);
                }
                else debugUtils.log("No Data: " + status);
                sm._checkReturnCode(resultMessage, true);
                sm._unlockInputAndfireEvent(onCompletedCallback, context, undefined, results, rtValue);
                sm._processNextRequest();
            },
            error: function () { sm.requestSpinResult(onCompletedCallback, context, betLevelIndex, coinValueIndex, winLine, dummySpin); }
        });
    },

    requestBonusScore: function (onCompletedCallback, context) {
        this.requestSimple("GetScatterBonusScore", undefined, onCompletedCallback, context);
    },

    requestBonusInit: function (onCompletedCallback, context)
    {
        this.requestSimple("GetScatterBonusInit", undefined, onCompletedCallback, context);
    },
}

var sm = new ServerManager();