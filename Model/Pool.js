﻿function Pool(maxSize)
{
    this.poolArray = new Array();
    this.maxSize = maxSize;
}

Pool.prototype = {
    poolArray: null,
    maxSize: 0,

    getFromPool: function (key)
    {
        for (var i = 0; i < this.poolArray.length; i++)
        {
            var sprite = this.poolArray[i];
            if (sprite.key === key && !sprite.alive)
            {
                //debugUtils.log("Get: " + this.poolArray.length);
                this.poolArray.splice(i, 1);
                return sprite;
            }
        }
        return null;
    },

    addToPool: function (newObject)
    {
        newObject.kill();
        if (this.poolArray.length === this.maxSize)
        {
            //Destroy and replace first item in pool.
            //Probably faster than splicing.
            this.poolArray[0].destroy();
            this.poolArray[0] = newObject;
        }
        else
        {
            this.poolArray.push(newObject);
        }
        //debugUtils.log("Add: " + this.poolArray.length);
    },

    clear: function ()
    {
        for (var i = 0; i < this.poolArray.length; i++)
        {
            this.poolArray[i].destroy();
        }
        this.poolArray.splice(0, this.poolArray.length);
    }
}