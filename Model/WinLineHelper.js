﻿function WinLineHelper() { }

WinLineHelper.prototype = {
    _winLineArray: new Array(),
    _winFrameArray: new Array(),

    getWinLinesWithStartRow: function (wins)
    {
        var allWinLines = new Array();
        for (var j = 0; j < wins.length; j++)
        {
            var winDataWithRef = { refRow: this.getWinLineStartRow(wins[j].winLine), winData: wins[j] };
            allWinLines.push(winDataWithRef);
        }
        return allWinLines;
    },

    getWinLineStartRow: function (winLine)
    {
        var winLineReferenceRow;
        var currentRow = winLine[0];
        var largestRow = Math.max.apply(Math, winLine);
        if (largestRow > currentRow) winLineReferenceRow = largestRow;
        else winLineReferenceRow = currentRow;
        return winLineReferenceRow;
    },

    getWinLineTexture: function (winLineIndex)
    {
        if (winLineIndex < 4) return "winline1-3";
        else if (winLineIndex < 5) return "winline4";
        else if (winLineIndex < 6) return "winline5";
        else if (winLineIndex < 8) return "winline6-7";
        else if (winLineIndex < 10) return "winline8-9";
        else if (winLineIndex < 12) return "winline10-11";
        else if (winLineIndex < 14) return "winline12-13";
        else if (winLineIndex < 15) return "winline14";
        else if (winLineIndex < 16) return "winline15";
        else if (winLineIndex < 18) return "winline16-17";
        else if (winLineIndex < 20) return "winline18-19";
        else if (winLineIndex === 20) return "winline20";
        return undefined;
    },

    showWinLine: function (winLineStartRow, winLineIndex, winLineData, showFrames, winX, delay)
    {
        if (winLineIndex === undefined) debugUtils.log("Invalid Winline Index: " + winLineIndex);
        var winlineTexKey = this.getWinLineTexture(++winLineIndex);
        if (!winlineTexKey) return undefined;

        var winLineX = gameUI.startPaddingX;// + gameUI.symbolsPaddingX / 2;
        var winLineY = gameUI.startPaddingY + (winLineStartRow * gameUI.symbolsPaddingY) - (gameUI.symbolsPaddingY / 2);
        //30 is kind of a special case since it reaches both upper and lower rows.
        //The option is to define every winline with their preferred rows and use that as a reference instead of
        //dynamic row calculation.
        if (winLineIndex === 30) winLineY -= 20;
        var winLine = game.add.sprite(winLineX, winLineY, winlineTexKey);
        winLine.anchor.setTo(0, 1);
        winLine.scale.setTo(0.7);
        winLine.alpha = 0;
        game.world.bringToTop(winLine);

        var colIndex = 0;
        var framesArray = new Array();
        for (var row = 0; row < winX; row++)
        {
            if (gameUI.symbolsCache[winLineData[row]][colIndex])
            {
                var sprite = gameUI.symbolsCache[winLineData[row]][colIndex].sprite;

                var frame = game.add.sprite(sprite.x, sprite.y, "symbolWinFrame");
                frame.anchor = sprite.anchor;
                frame.scale = { x: gameUI.symbolScale * 0.5, y: gameUI.symbolScale * 0.5 };

                gameUI.animateWinSymbol(gameUI.symbolsCache[winLineData[row]][colIndex]);

                framesArray.push(frame);
            }
            colIndex++;
        }

        this._winFrameArray.push(framesArray);

        this._winLineArray.push(winLine);
        return { sprite: winLine, tween: game.add.tween(winLine).to({ alpha: 1 }, 250, Phaser.Easing.Linear.In, true, delay) };
    },

    hideWinLine: function (winLine, delay, destroy, animate)
    {
        destroy = destroy === undefined ? true : animate;
        animate = animate === undefined ? true : animate;
        this.stopWinLineTween(winLine);
        if (!animate)
        {
            this.destroyWinLine(winLine);
            return null;
        }
        else
        {
            var tween = game.add.tween(winLine).to({ alpha: 0 }, 250, Phaser.Easing.Linear.In, true, delay);

            var index = this._winLineArray.indexOf(winLine);
            for (var i = 0; i < this._winFrameArray[index].length; i++)
            {
                game.add.tween(this._winFrameArray[index][i]).to({ alpha: 0 }, 250, Phaser.Easing.Linear.In, true, delay);
            }

            if (destroy)
            {
                tween.onComplete.addOnce(function (target)
                {
                    this.destroyWinLine(target);
                }, this);
            }
            return tween;
        }
    },

    stopWinLineTween: function (winLine)
    {
        game.tweens.removeFrom(winLine);
        var index = this._winLineArray.indexOf(winLine);
        if (index > -1)
        {
            for (var i = 0; i < this._winFrameArray[index].length; i++)
            {
                game.tweens.removeFrom(this._winFrameArray[index][i]);
            }
        }
    },

    destroyWinLine: function (winLine)
    {
        this.stopWinLineTween(winLine);
        var index = this._winLineArray.indexOf(winLine);
        if (index > -1)
        {
            for (var i = 0; i < this._winFrameArray[index].length; i++)
            {
                this._winFrameArray[index][i].destroy();
            }
            this._winLineArray.splice(index, 1);
            this._winFrameArray.splice(index, 1);
        }
        if (winLine.mask) winLine.mask.destroy();
        winLine.destroy();
    },

    hideAllWinLines: function (delay, destroy, animate)
    {
        animate = animate === undefined ? true : animate;
        var sampleTween = undefined;
        for (var i = 0; i < this._winLineArray.length; i++)
        {
            var index = animate ? i : 0;
            if (sampleTween === undefined) sampleTween = this.hideWinLine(this._winLineArray[index], delay, destroy, animate);
            else this.hideWinLine(this._winLineArray[index], delay, destroy, animate);
        }
        return sampleTween;
    },

    dispose()
    {
        var i;
        for (i = 0; i < this._winLineArray.length; i++)
        {
            if (this._winLineArray[i].mask) this._winLineArray[i].mask.destroy();
            this._winLineArray[i].destroy();
        }
        for (var j = 0; j < this._winFrameArray.length; j++)
        {
            for (i = 0; i < this._winFrameArray[index].length; i++)
            {
                this._winFrameArray[j][i].destroy();
            }
        }

        this._winLineArray.splice(0, this._winLineArray.length);
        this._winFrameArray.splice(0, this._winFrameArray.length);
    }
}

var winLineHelper = new WinLineHelper();