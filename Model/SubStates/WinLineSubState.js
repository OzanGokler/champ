﻿function WinLineSubState() { }

var parent = extend(WinLineSubState, Completable);
WinLineSubState.prototype._allWinLines = null;
WinLineSubState.prototype._currentWinLine = null;
WinLineSubState.prototype._clearWinValue = false;

WinLineSubState.prototype.init = function (allWinLines, currentWinLine, clearWinValue)
{
    this._allWinLines = allWinLines;
    this._currentWinLine = currentWinLine;
    this._clearWinValue = clearWinValue;
    console.log(this._clearWinValue);
};

WinLineSubState.prototype.begin = function ()
{
    //console.log("win line begin");
    this._showWinLines(this._allWinLines, this._currentWinLine);
};

WinLineSubState.prototype._showWinLines = function (allWinLines, currentWinLineIndex, showFrames)
{
    currentWinLineIndex = currentWinLineIndex === undefined ? 0 : currentWinLineIndex;
    var rawWinData = allWinLines[currentWinLineIndex];
    var winLine = winLineHelper.showWinLine(rawWinData.refRow, rawWinData.winData.winLineIndex, rawWinData.winData.winLine, showFrames, rawWinData.winData.winX);
    gameUI.showScorePopup(rawWinData.winData.creditsWon, winLine.sprite.x + (winLine.sprite.width / 2), winLine.sprite.y - (winLine.sprite.height / 2), 0);
    if (!gm.isDummySpin)
    {
        console.log("FIRST " + gm.cumulativeCreditsWon);
        gm.cumulativeCreditsWon += rawWinData.winData.creditsWon;
        gm.currentCredits += rawWinData.winData.creditsWon;
        gameUI.Interface.updateBalanceUI(true);
        
        console.log("ISNOTDUMMY");
        console.log("LAST " + gm.cumulativeCreditsWon);
        console.log("RAW " + rawWinData.winData.creditsWon);
        console.log(rawWinData);
    }
    audioManager.playWinSound(currentWinLineIndex > 0);
    //console.log("11111111111111111111111111111111111111111111111");
    //console.log(rawWinData.winData.winSymbol);
    audioManager.playSymbolSound(rawWinData.winData.winSymbol);
    //audioManager.playLaughterSound();



    winLine.tween.onComplete.addOnce(function ()
    {
        if (currentWinLineIndex < allWinLines.length - 1)
        {
            currentWinLineIndex++;
            game.time.events.add(500, function ()
            {
                var hideTween = winLineHelper.hideAllWinLines(500);
                if (hideTween) hideTween.onComplete.addOnce(function () { this._showWinLines(allWinLines, currentWinLineIndex); }, this);
                else this._showWinLines(allWinLines, currentWinLineIndex);
                //balanceUI.updateBalanceUIOld(0, undefined, undefined, undefined, undefined);
            }, this);
        }
        else
        {
            winLineHelper.hideAllWinLines(500);
            game.time.events.add(1000, this._stateCompleted, this);
        }
    }, this);
};

WinLineSubState.prototype._stateCompleted = function () {
    if (this._clearWinValue) gm.clearCumulativeWonAmount(gameUI.Interface);
    game.time.events.add(400, this._onComplete, this);
}
winLineSubState = new WinLineSubState();