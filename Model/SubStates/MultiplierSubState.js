﻿function MultiplierSubState() { }
function MultiplierSubState() { }

var parent = extend(MultiplierSubState, Completable);


MultiplierSubState.prototype._multiplierText = null;
MultiplierSubState.prototype._totalScoreText = null;
MultiplierSubState.prototype._totalScoreValueText = null;
MultiplierSubState.prototype._multiplier = null;
MultiplierSubState.prototype._creditsToAdd = null;
MultiplierSubState.prototype._creditsToAdd = null;
MultiplierSubState.prototype._spotLights = null;
MultiplierSubState.prototype._scoreProgress = { step: 0 };

MultiplierSubState.prototype.init = function (spotLights)
{
    this._spotLights = spotLights;
};

//Called after winlines and everything is shown. But before big win check. So this should be last in queue.
MultiplierSubState.prototype.begin = function ()
{
    sm.requestExpandingWildResult(this._onResultReceived, this);
};

MultiplierSubState.prototype._onResultReceived = function (result)
{
    console.log(result);
    this._multiplier = result.multiplier;
    this._creditsToAdd = result.credits;
    gm.cumulativeCreditsWon += this._creditsToAdd;
    gm.currentCredits += this._creditsToAdd;
    this._positionUI();
};

MultiplierSubState.prototype._positionUI = function ()
{
    this._multiplierText = game.add.bitmapText(game.width / 2, vm.ActiveGameHeight / 2, "ObelixProWinAmountWinline", languageManager.langJSON.multiplier.format(this._multiplier), 60);
    this._multiplierText.anchor.set(0.5, 1);
    this._multiplierText.align = "center";
    this._multiplierText.scale.set(1, 0);
    this._multiplierText.y += 30;

    this._totalScoreText = game.add.bitmapText(game.width / 2, vm.ActiveGameHeight / 2, "ObelixProWinAmountWinline", languageManager.langJSON.totalWin, 60);
    this._totalScoreText.anchor.set(0, 0.5);
    this._totalScoreText.align = "center";

    this._totalScoreValueText = game.add.bitmapText(game.width / 2, vm.ActiveGameHeight / 2, "ObelixProWinAmountWinline", gm.cumulativeCreditsWon.toFixed(0).toString(), 80);
    this._totalScoreValueText.anchor.set(0.5);
    this._totalScoreValueText.align = "center";
    this._totalScoreValueText.angle = 270;
    this._totalScoreValueText.alpha = 0;

    var paddingBetweenTexts = 10;
    this._totalScoreText.x = (game.width / 2) - (this._totalScoreText.textWidth + this._totalScoreValueText.textWidth + paddingBetweenTexts) / 2;
    //+20 _totalScoreText y movement.
    this._totalScoreValueText.x = (this._totalScoreText.x) + (this._totalScoreText.textWidth + this._totalScoreValueText.textWidth / 2) + paddingBetweenTexts - 20;

    this._totalScoreText.text = "";

    var multiplierTween = game.add.tween(this._multiplierText.scale).to({ x: 1, y: 1 }, 500, Phaser.Easing.Linear.In, true, 500);

    multiplierTween.onComplete.addOnce(function ()
    {
        game.time.events.add(500, function ()
        {
            this._multiplierText.visible = false;

            game.add.tween(this._totalScoreText).to({ x: this._totalScoreText.x - 20 }, 450, Phaser.Easing.Linear.In, true);

            game.add.tween(this._totalScoreValueText).to({ alpha: 1, angle: 360 }, 450, Phaser.Easing.Linear.In, true);
            game.add.tween(this._totalScoreValueText.scale).from({ x: 0, y: 0 }, 450, Phaser.Easing.Linear.In, true);

            this.startValueChangeTween(this._scoreProgress, this._totalScoreValueText, 0, gm.cumulativeCreditsWon);

            game.time.events.repeat(35, languageManager.langJSON.bonusGame.length, this.displayNextBonusLetter,
                 { textObject: this._totalScoreText, message: languageManager.langJSON.totalWin, counter: 1 });

            game.time.events.add(3000, function ()
            {
                console.log("this._totalScoreText fade out");
                gameUI.Interface.updateBalanceUI(true);
                game.add.tween(this._totalScoreText).to({ alpha: 0 }, 350, Phaser.Easing.Linear.In, true).onComplete.addOnce(this._onStateComplete, this);
                game.add.tween(this._totalScoreValueText).to({ alpha: 0 }, 350, Phaser.Easing.Linear.In, true).onComplete.addOnce(this._onStateComplete, this);
                gameUI.Interface.updateBalanceUI(gameUI.Interface.balanceUI.cashValue + gm.convertCreditsToRealMoney(this._creditsToAdd), gameUI.Interface.balanceUI.coinsValue + this._creditsToAdd, undefined, gm.CumulativeRealMoneyWon);
            }, this);
        }, this);
    }, this);
};

MultiplierSubState.prototype.displayNextBonusLetter = function ()
{
    this.textObject.text = this.message.substr(0, this.counter++);
}

MultiplierSubState.prototype._onStateComplete = function ()
{
    console.log("_onStateComplete");
    for (var i = 0; i < this._spotLights.length; i++)
    {
        this._spotLights[i].frame = 0;
    }
    game.time.events.add(750, this._onComplete, this);
};


MultiplierSubState.prototype.startValueChangeTween = function (valueProgressObj, textObject, startValue, endValue)
{
    game.tweens.removeFrom(valueProgressObj, false);
    valueProgressObj.step = 0;
    var tween = game.add.tween(valueProgressObj).to({ step: 1 }, 1300, Phaser.Easing.Linear.Out, true);
    tween.onUpdateCallback(this.valueUpdate, { text: textObject, startValue: startValue, endValue: endValue });
    tween.onComplete.add(this.valueComplete, { text: textObject, endValue: endValue });
};

MultiplierSubState.prototype.valueUpdate = function (twn, percent)
{
    this.startValue = this.startValue == undefined ? 0 : this.startValue;
    //use tofixed while animating.
    this.text.setText(game.math.linear(this.startValue, this.endValue, percent).toFixed(0));
};

MultiplierSubState.prototype.valueComplete = function ()
{
    this.text.setText(this.endValue.numberFormat(0));
};