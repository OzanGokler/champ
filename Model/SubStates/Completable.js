﻿function Completable() { }

Completable.prototype._onCompletedCallback = null;
Completable.prototype._onCompletedContext = null;
Completable.prototype._onCompletingCallbacks = undefined;
Completable.prototype._completingSignal = undefined;
Completable.prototype._completedSignal = undefined;
Completable.prototype.status = { OK: 0, ERROR: 1 };

Completable.prototype.addOnCompletingEvent = function (callback, context)
{
    if (!this._onCompletingCallbacks) this._onCompletingCallbacks = new Array();
    this._onCompletingCallbacks.push({ callback: callback, context: context });
}

Completable.prototype.setOnCompleteEvent = function (callback, context)
{
    this._onCompletedCallback = callback;
    this._onCompletedContext = context;
}

Completable.prototype.begin = function ()
{
}

Completable.prototype._onComplete = function (status, data)
{
   
    //Fire on completing events just before on completed events.
    if (this._onCompletingCallbacks)
    {
       
        if (!this._completingSignal) this._completingSignal = new Phaser.Signal();
        for (var i = 0; i < this._onCompletingCallbacks.length; i++)
        {
            this._completingSignal.addOnce(this._onCompletingCallbacks[i].callback, this._onCompletingCallbacks[i].context, undefined, status, data);
            //console.log(this._onCompletingCallbacks[i]);
        }
        
        this._onCompletingCallbacks.splice(0, this._onCompletingCallbacks.length);
        this._completingSignal.dispatch();
       
    }
    
    status = status === undefined ? this.status.OK : status;
    if (!this._completedSignal) this._completedSignal = new Phaser.Signal();
    if (this._onCompletedCallback && this._onCompletedContext)
    {
        this._completedSignal.addOnce(this._onCompletedCallback, this._onCompletedContext, undefined, status, data);
        this._completedSignal.dispatch();
    }
}