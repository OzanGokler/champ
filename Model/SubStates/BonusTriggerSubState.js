﻿function BonusTriggerSubState() { }

var parent = extend(BonusTriggerSubState, Completable);

BonusTriggerSubState.prototype.begin = function ()
{
    gameUI.toggleAllButtons(false, true);
    game.time.events.add(350, this._showBonusBoard, this);
}

BonusTriggerSubState.prototype._showBonusBoard = function ()
{
    gameUI.toggleAllButtons(false, true);
    audioManager.playBonusLaughterSound();
    bonusBoard.show(this._onBonusBoardClicked, this);
};

BonusTriggerSubState.prototype._onBonusBoardClicked = function ()
{
    audioManager.playBonusIntro();
    gameUI.drop(true);
    //this.drop changes game state to spinning and this later toggles buttons on.
    //We are making sure that the state is still bonus so the buttons wont trigger.
    gm.CurrentSpinState = gm.spinStates.scatterBonus;
    boardPopUp.onClosed.addOnce(gameUI.startBonusRound, gameUI);
};