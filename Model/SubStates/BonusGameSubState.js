﻿function BonusGameSubState()
{

}

var parent = extend(BonusGameSubState, Completable);


BonusGameSubState.prototype.langJSON = null;
BonusGameSubState.prototype.slugger = null;
BonusGameSubState.prototype.ChallangeTheChamp = null;

BonusGameSubState.prototype.crowdrightcheer0 = null;
BonusGameSubState.prototype.crowdrightcheeranim0 = null;
BonusGameSubState.prototype.crowdrightcheer1 = null;
BonusGameSubState.prototype.crowdrightcheeranim1 = null;
BonusGameSubState.prototype.crowdrightcheer2 = null;
BonusGameSubState.prototype.crowdrightcheeranim2 = null;
BonusGameSubState.prototype.crowdrightcheer3 = null;
BonusGameSubState.prototype.crowdrightcheeranim3 = null;

BonusGameSubState.prototype.crowdleftcheer0 = null;
BonusGameSubState.prototype.crowdleftcheeranim0 = null;
BonusGameSubState.prototype.crowdleftcheer1 = null;
BonusGameSubState.prototype.crowdkeftcheeranim1 = null;
BonusGameSubState.prototype.crowleftcheer2 = null;
BonusGameSubState.prototype.crowdleftcheeranim2 = null;
BonusGameSubState.prototype.crowdleftcheer3 = null;
BonusGameSubState.prototype.crowdleftcheeranim3 = null;

BonusGameSubState.prototype.crowdleftshout0 = null;
BonusGameSubState.prototype.crowdleftshoutanim0 = null;

BonusGameSubState.prototype.areaOutlines = null;
BonusGameSubState.prototype.animTransition = null;
BonusGameSubState.prototype.introAnim = null;
BonusGameSubState.prototype.outroAnim = null;
BonusGameSubState.prototype.updateScore = false;
BonusGameSubState.prototype.textPopups = new Array(0);
BonusGameSubState.prototype.mainGroup = null;
BonusGameSubState.prototype.sluggerGroup = null;
BonusGameSubState.prototype.scoreTableGroup = null;
BonusGameSubState.prototype.buttons = new Array(12);
BonusGameSubState.prototype.points = new Array(11);
BonusGameSubState.prototype.bg = null;
BonusGameSubState.prototype.crowdStillRight = null;
BonusGameSubState.prototype.crowdStillRight2 = null;
BonusGameSubState.prototype.crowdStillLeft = null;
BonusGameSubState.prototype.crowdStillLeft2 = null;
BonusGameSubState.prototype.scoreboard = null;
BonusGameSubState.prototype.collect = null;
BonusGameSubState.prototype.total = null;
BonusGameSubState.prototype.tweenScore = null;
BonusGameSubState.prototype.winValue = 0;
BonusGameSubState.prototype.winValueText = null;
BonusGameSubState.prototype.totalwinValueText = null;
BonusGameSubState.prototype.decimalCoun = 0;
BonusGameSubState.prototype.scoreProgress = { step: 0 };
BonusGameSubState.prototype.clickmeKO = null;
BonusGameSubState.prototype.onCompleteEvent = new Phaser.Signal();
BonusGameSubState.prototype.scoreArray = null;
BonusGameSubState.prototype.bigStar = null;
BonusGameSubState.prototype.smallStar = null;
BonusGameSubState.prototype.starsPool = new Pool(3);
BonusGameSubState.prototype.bonusMask = null;

BonusGameSubState.prototype.anims = { knockOut: 0, idle: 1, face: 2, hitsBack: 3, stomach: 4, misses: 5 };

//Table UI
BonusGameSubState.prototype.selectedLid = null;

BonusGameSubState.prototype.preload = function ()
{
    console.log("BonusUI.preload");
};

BonusGameSubState.prototype.begin = function ()
{

    gameUI.Interface.toggleAllButtons(false, true);
    gameUI.Interface.hideBtnSpin();
    textureOverlay.fade(0xFFFFFF, undefined, 0, 1, 100, true).onComplete.addOnce(function ()
    {
        sm.requestBonusInit(this.bonusInitReceived, this);

        gameUI._lightStreakStart(true);
    }, this);
};

BonusGameSubState.prototype.bonusInitReceived = function (bonusData)
{
    this.scoreArray = bonusData;
    console.log(bonusData);
    this.show();
    this.currentChallangeTheChamp();
};

BonusGameSubState.prototype.currentChallangeTheChamp = function ()
{
    game.add.tween(this.ChallangeTheChamp.scale).to({ x: 1, y: 1 }, 1000, Phaser.Easing.Bounce.Out, true);
    this.ChallangeTheChamp.alpha = 1;
    game.world.bringToTop(this.ChallangeTheChamp);
};

BonusGameSubState.prototype.show = function ( /*onCompletedCallback, context*/)
{

    //this.onCompleteEvent.addOnce(onCompletedCallback, context);
    console.log("BonusUI.create");
    this.winValue = 0;
    bonusGm.reset();
    this.langJSON = game.cache.getJSON("language");
    this.positionUI();
    if (Phaser.Device.desktop)
    {
        this.initializeInput();
        this.playIntroAnimation();
    } else
    {
        this.initializeInput();
        for (var i = 0; i < this.buttons.length; i++)
        {
            this.buttons[i].inputEnabled = true;
        }
    }
    //vm.setBalanceBar();
    game.world.bringToTop(gameUI.Interface.balanceUI.layer);
};

BonusGameSubState.prototype.updateBonusUI = function (win, vwt)
{
    var tween;
    if (win != undefined && win !== this.winValue)
    {
        game.tweens.removeFrom(this.scoreProgress, false);
        this.scoreProgress.step = 0;
        tween = game.add.tween(this.scoreProgress).to({ step: 1 }, 500, Phaser.Easing.Linear.Out, true);
        tween.onUpdateCallback(this.valueUpdate, { text: vwt, startValue: this.winValue, endValue: this.winValue + win });
        tween.onComplete.add(this.valueComplete, { text: vwt, endValue: this.winValue + win });
        this.winValue += win;
    }
    if (win === 0)
    {
        game.tweens.removeFrom(this.scoreProgress, false);
        this.scoreProgress.step = 0;
        tween = game.add.tween(this.scoreProgress).to({ step: 1 }, 500, Phaser.Easing.Linear.Out, true);
        tween.onUpdateCallback(this.valueUpdate, { text: vwt, startValue: win, endValue: this.winValue + win });
        tween.onComplete.add(this.valueComplete, { text: vwt, endValue: this.winValue + win });
        this.winValue += win;
    }
};

BonusGameSubState.prototype.valueUpdate = function (twn, percent)
{
    this.startValue = this.startValue == undefined ? 0 : this.startValue;
    this.text.setText(game.math.linear(this.startValue, this.endValue, percent).toFixed(this.decimalCount));
};
BonusGameSubState.prototype.valueComplete = function ()
{
    this.text.setText(this.endValue.toFixed(this.decimalCount));
};

BonusGameSubState.prototype.positionUI = function ()
{
    if (debugUtils.debugSettings.showLogs)
        console.log("BonusUI - positionUI");
    if (this.mainGroup && this.mainGroup.game)
    {
        console.log(" this.mainGroup.position");
        this.mainGroup.position.y = 600;
        console.log("resetBonusGame");
        this.resetBonusGame();
        console.log("  this.slugger.pla");
        this.slugger.play(this.anims.idle);
        return;
    }
    console.log("BonusUI - CREATE STUFF");

    this.bg = game.add.sprite(0, 0, "bonusBg");
    this.bg.scale.setTo(0.65);

    this.crowdStillRight = game.add.sprite(660, 390, "crowdStill");
    this.crowdStillRight.scale.setTo(0.7);

    this.crowdStillRight2 = game.add.sprite(1000, 450, "crowdStill");
    this.crowdStillRight2.scale.setTo(0.4);

    this.crowdStillLeft = game.add.sprite(650, 390, "crowdStill");
    this.crowdStillLeft.scale.setTo(-0.7, 0.7);
    this.crowdStillLeft2 = game.add.sprite(400, 390, "crowdStill");
    this.crowdStillLeft2.scale.setTo(-0.7, 0.7);

    //RIGHT CROWD ANIM
    this.crowdrightcheer0 = game.add.sprite(850, 400, "crowdcheer");
    this.crowdrightcheer0.scale.setTo(1.5);
    this.crowdrightcheeranim0 = this.crowdrightcheer0.animations.add("cheer");
    this.crowdrightcheeranim0.play(30, true);

    this.crowdrightcheer2 = game.add.sprite(950, 390, "crowdcheer");
    this.crowdrightcheer2.scale.setTo(1.5);
    this.crowdrightcheeranim2 = this.crowdrightcheer2.animations.add("cheer");

    game.time.events.add(Phaser.Timer.SECOND * 0.5, function ()
    {
        this.crowdrightcheeranim2.play(30, true);
    }, this);

    this.crowdrightcheer3 = game.add.sprite(800, 370, "crowdcheer");
    this.crowdrightcheer3.scale.setTo(1.5);
    this.crowdrightcheeranim3 = this.crowdrightcheer3.animations.add("cheer");

    game.time.events.add(Phaser.Timer.SECOND * 0.85, function ()
    {
        this.crowdrightcheeranim3.play(30, true);
    }, this);

    this.crowdrightcheer1 = game.add.sprite(1020, 450, "crowdcheer");
    this.crowdrightcheer1.scale.setTo(1.5);
    this.crowdrightcheeranim1 = this.crowdrightcheer1.animations.add("cheer");

    game.time.events.add(Phaser.Timer.SECOND * 0.10, function ()
    {
        this.crowdrightcheeranim1.play(30, true);
    }, this);

    //LEFT CROWD ANIM
    this.crowdleftcheer0 = game.add.sprite(200, 410, "crowdcheer");
    this.crowdleftcheer0.scale.setTo(-1.5, 1.5);
    this.crowdleftcheeranim0 = this.crowdleftcheer0.animations.add("cheer");
    game.time.events.add(Phaser.Timer.SECOND * 0.30, function ()
    {
        this.crowdleftcheeranim0.play(30, true);
    }, this);

    this.crowdleftcheer1 = game.add.sprite(280, 350, "crowdcheer");
    this.crowdleftcheer1.scale.setTo(-1.5, 1.5);
    this.crowdleftcheeranim1 = this.crowdleftcheer1.animations.add("cheer");
    game.time.events.add(Phaser.Timer.SECOND * 0.60, function ()
    {
        this.crowdleftcheeranim1.play(30, true);
    }, this);

    this.crowdleftcheer2 = game.add.sprite(350, 400, "crowdcheer");
    this.crowdleftcheer2.scale.setTo(-1.5, 1.5);
    this.crowdleftcheeranim2 = this.crowdleftcheer2.animations.add("cheer");
    game.time.events.add(Phaser.Timer.SECOND * 0.60, function ()
    {
        this.crowdleftcheeranim2.play(30, true);
    }, this);

    this.crowdleftcheer3 = game.add.sprite(480, 370, "crowdcheer");
    this.crowdleftcheer3.scale.setTo(-1.5, 1.5);
    this.crowdleftcheeranim3 = this.crowdleftcheer3.animations.add("cheer");
    game.time.events.add(Phaser.Timer.SECOND * 0.45, function ()
    {
        this.crowdleftcheeranim3.play(30, true);
    }, this);

    this.crowdleftshout0 = game.add.sprite(540, 460, "crowdshout");
    this.crowdleftshout0.scale.setTo(-1.5, 1.5);
    this.crowdleftshoutanim0 = this.crowdleftshout0.animations.add("cheer");
    game.time.events.add(Phaser.Timer.SECOND * 0.70, function ()
    {
        this.crowdleftshoutanim0.play(30, true);
    }, this);


    this.areaOutlines = game.add.sprite(380, 120, "areaOutlines");
    this.scoreboard = game.add.sprite(710, 240, "scoreboard");
    this.scoreboard.scale.setTo(0.9);

    this.mainGroup = game.add.group();
    this.sluggerGroup = game.add.group();
    this.scoreTableGroup = game.add.group();
    this.sluggerGroup.add(this.areaOutlines);
    this.scoreTableGroup.add(this.scoreboard);

    //Position lids:
    this.buttons[0] = game.add.button(552, 355, "belly", null, null, 1, 0);
    this.buttons[1] = game.add.button(577, 190, "head", null, null, 1, 0);
    this.buttons[2] = game.add.button(673, 263, "leftArm", null, null, 0, 1);

    this.buttons[3] = game.add.button(618, 294, "leftBoob", null, null, 1, 0);
    this.buttons[4] = game.add.button(645, 258, "leftGlove", null, null, 1, 0);
    this.buttons[5] = game.add.button(608, 543, "leftLeg", null, null, 1, 0);

    this.buttons[6] = game.add.button(610, 443, "pantsLeft", null, null, 1, 0);
    this.buttons[7] = game.add.button(485, 423, "pantsRight", null, null, 1, 0);
    this.buttons[8] = game.add.button(438, 240, "rightArm", null, null, 1, 0);
    this.buttons[9] = game.add.button(507, 286, "rightBoob", null, null, 1, 0);
    this.buttons[10] = game.add.button(495, 233, "rightGlove", null, null, 1, 0);
    this.buttons[11] = game.add.button(500, 545, "rightLeg", null, null, 1, 0);

    //Position Points

    this.points[0] = game.add.bitmapText(810, 275, "ObelixProBonusGameLight", "0", 22);
    this.points[1] = game.add.bitmapText(775, 305, "ObelixProBonusGameLight", "0", 22);
    this.points[2] = game.add.bitmapText(835, 305, "ObelixProBonusGameLight", "0", 22);
    this.points[3] = game.add.bitmapText(775, 335, "ObelixProBonusGameLight", "0", 22);
    this.points[4] = game.add.bitmapText(835, 335, "ObelixProBonusGameLight", "0", 22);
    this.points[5] = game.add.bitmapText(775, 365, "ObelixProBonusGameLight", "0", 22);
    this.points[6] = game.add.bitmapText(835, 365, "ObelixProBonusGameLight", "0", 22);
    this.points[7] = game.add.bitmapText(765, 395, "ObelixProBonusGameLight", "0", 22);
    this.points[8] = game.add.bitmapText(835, 395, "ObelixProBonusGameLight", "0", 22);
    this.points[9] = game.add.bitmapText(765, 425, "ObelixProBonusGameLight", "0", 22);
    this.points[10] = game.add.bitmapText(835, 425, "ObelixProBonusGameLight", "0", 22);
    this.collect = game.add.bitmapText(770, 455, "ObelixProBonusGameLight", this.langJSON.pointCollect, 22);
    this.total = game.add.bitmapText(775, 490, "ObelixProBonusGameLight", this.langJSON.pointTotal, 22);
    this.total.tint = 0x000000;
    this.winValueText = game.add.bitmapText(820, 535, "ObelixProBonusGameLight", "0", 22);
    this.winValueText.anchor.setTo(0.5);
    this.winValueText.tint = 0x000000;

    for (var j = 0; j < this.scoreArray.length - 1; j++)
    {
        this.points[j].text = this.scoreArray[j].toString();
    }

    this.clickmeKO = game.add.button(game.width / 2, vm.ActiveGameHeight / 2, "clickmeKO", null, null, 1, 0);
    this.clickmeKO.anchor.setTo(0.5);
    this.clickmeKO.scale.setTo(0, 0);
    this.clickmeKO.inputEnabled = false;
    this.clickmeKO.events.onInputDown.addOnce(this.returnToGame, this);
    //game.add.tween(this.clickmeKO.scale).to({ x: 1, y: 1 }, 500, Phaser.Easing.Bounce.Out, true);
    this.totalwinValueText = game.add.bitmapText(675, 470, "ObelixProBonusGameLight", "0", 40);
    this.totalwinValueText.anchor.setTo(0.5);
    this.totalwinValueText.scale.setTo(0, 0);


    this.scoreTableGroup.add(this.points[0]);
    this.scoreTableGroup.add(this.points[1]);
    this.scoreTableGroup.add(this.points[2]);
    this.scoreTableGroup.add(this.points[3]);
    this.scoreTableGroup.add(this.points[4]);
    this.scoreTableGroup.add(this.points[5]);
    this.scoreTableGroup.add(this.points[6]);
    this.scoreTableGroup.add(this.points[7]);
    this.scoreTableGroup.add(this.points[8]);
    this.scoreTableGroup.add(this.points[9]);
    this.scoreTableGroup.add(this.points[10]);
    this.scoreTableGroup.add(this.collect);
    this.scoreTableGroup.add(this.total);
    this.scoreTableGroup.add(this.winValueText);

    for (var i = 0; i < this.buttons.length; i++)
    {
        this.buttons[i].anchor.setTo(0.5, 0.5);
        this.buttons[i].inputEnabled = false;
        this.buttons[i].alpha = 0;
        this.buttons[i].events.onInputOver.add(this.sluggerOn, this);
        this.buttons[i].events.onInputOut.add(this.sluggerOut, this);
        this.sluggerGroup.add(this.buttons[i]);
    }

    this.sluggerGroup.scale.setTo(0.85);
    this.sluggerGroup.position.x = 80;
    this.sluggerGroup.position.y = 100;

    this.titleText = game.make.bitmapText(game.world.centerX, 200, "chalkFont", this.langJSON.youWin, 120);
    this.titleText.anchor.setTo(0.5, 0.5);

    this.scoreText = game.make.bitmapText(game.world.centerX, 350, "chalkFont", "0", 180);
    this.scoreText.anchor.setTo(0.5, 0.5);

    this.ChallangeTheChamp = game.add.sprite(game.width / 2, vm.ActiveGameHeight / 2, "challangeTheChamp");
    this.ChallangeTheChamp.anchor.setTo(0.5);
    this.ChallangeTheChamp.scale.setTo(0);
    this.ChallangeTheChamp.inputEnabled = true;
    this.ChallangeTheChamp.events.onInputDown.add(this.bonusUIinTween, this);

    this.mainGroup.add(this.crowdStillRight2);
    this.mainGroup.add(this.crowdrightcheer2);
    this.mainGroup.add(this.crowdrightcheer3);
    this.mainGroup.add(this.crowdrightcheer1);
    this.mainGroup.add(this.crowdStillRight);
    this.mainGroup.add(this.crowdleftcheer1);
    this.mainGroup.add(this.crowdleftcheer3);
    this.mainGroup.add(this.crowdStillLeft2);
    this.mainGroup.add(this.crowdleftcheer0);
    this.mainGroup.add(this.crowdStillLeft);
    this.mainGroup.add(this.crowdleftcheer2);
    this.mainGroup.add(this.crowdrightcheer0);
    this.mainGroup.add(this.crowdleftshout0);
    this.mainGroup.add(this.bg);
    this.mainGroup.add(this.scoreTableGroup);
    this.mainGroup.add(this.sluggerGroup);
    this.bonusMask = game.add.graphics(0, 0);
    this.bonusMask.drawRect(0, 0, game.width, vm.ActiveGameHeight);
    this.mainGroup.mask = this.bonusMask;


    this.mainGroup.position.y = 600;

    //SLUGGER ANIMS
    if (!this.slugger)
    {
        this.slugger = new CharacterAnimator();
    }

    this.slugger.init();

    this.slugger.addSpriteAnimPair(this.anims.idle, 638, 393, this.mainGroup, 1.05, "idle", true);

    this.slugger.play(this.anims.idle);

    this.mainGroup.bringToTop(this.sluggerGroup);

    //game.time.events.add(Phaser.Timer.SECOND * 0.5, function () {
    //    game.add.tween(this.mainGroup).to({ y: 0 }, 2500, Phaser.Easing.Quartic.Out, true);
    //}, this);

    //game.add.tween(this.mainGroup.scale).to({ x:0,y: 0 }, 1000, Phaser.Easing.Linear.In, true)

};

BonusGameSubState.prototype.bonusUIinTween = function ()
{
    audioManager.playBonusMusicIntro();
    audioManager.playBonusMusic();
    game.time.events.add(Phaser.Timer.SECOND * 0.5, function ()
    {
        game.add.tween(this.mainGroup).to({ y: 0 }, 2500, Phaser.Easing.Quartic.Out, true);
    }, this);
    var challangeTheChampTween = game.add.tween(this.ChallangeTheChamp).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
    challangeTheChampTween.onComplete.addOnce(this.ChallangeTheChampScale, this);
    //game.add.tween(this.ChallangeTheChamp.scale).to({ x: 0, y: 0 }, 2000, Phaser.Easing.Bounce.Out, true);

    gameUI.gameUiOutTween();
};
BonusGameSubState.prototype.ChallangeTheChampScale = function ()
{
    this.ChallangeTheChamp.scale.setTo(0);
};

BonusGameSubState.prototype.bonusUIOutTween = function ()
{
    game.add.tween(this.mainGroup).to({ y: 600 }, 2500, Phaser.Easing.Quartic.Out, true).onComplete.addOnce(this._onComplete, this);
    //clickTween = game.add.tween(this.clickmeKO.scale).to({ x: 0, y: 0 }, 1000, Phaser.Easing.Linear.Out, true);
    var clickTween = game.add.tween(this.clickmeKO).to({ alpha: 0 }, 500, Phaser.Easing.Linear.Out, true);
    //clickTween = game.add.tween(this.totalwinValueText).to({ alpha: 0 }, 500, Phaser.Easing.Linear.Out, true);
    game.add.tween(this.totalwinValueText).to({ alpha: 0 }, 500, Phaser.Easing.Linear.Out, true);
    //game.add.tween(this.totalwinValueText.scale).to({ x: 0, y: 0 }, 500, Phaser.Easing.Bounce.Out, true);
    clickTween.onComplete.addOnce(this.clickmeKOScale, this);
};
BonusGameSubState.prototype.clickmeKOScale = function ()
{
    this.clickmeKO.scale.setTo(0);
    this.totalwinValueText.scale.setTo(0);
};

BonusGameSubState.prototype.sluggerOn = function (selected)
{
    selected.alpha = 1;
};

BonusGameSubState.prototype.sluggerOut = function (selected)
{
    if (selected.inputEnabled) selected.alpha = 0;
};

//initializeButtonInput= function (button) {
//    button.inputEnabled = false;
//    button.events.onInputDown.add(this.openLid, this);
//};

BonusGameSubState.prototype.initializeInput = function ()
{
    if (debugUtils.debugSettings.showLogs)
        console.log("BonusUI - initializeInput");

    for (var i = 0; i < this.buttons.length; i++)
    {
        this.buttons[i].inputEnabled = false;
        this.buttons[i].events.onInputDown.add(this.openLid, this);
    }
};

BonusGameSubState.prototype.returnToGame = function ()
{
    this.onCompleteEvent.dispatch();
    this.bonusUIOutTween();
    audioManager.playBonusMusicOut();
    audioManager.playMainMusic();
    gameUI.gameUiInTween(this.destroyTextures, this);
    gameUI.Interface.toggleAllButtons(true, true);
};

BonusGameSubState.prototype.destroyTextures = function ()
{
    this.mainGroup.destroy(true);
    this.slugger.destroy();
};

BonusGameSubState.prototype.openLid = function (selectedLid)
{
    if (debugUtils.debugSettings.showLogs)
        console.log("BonusUI - openLid: " + bonusGm.lidsOpened + ":" + bonusGm.scoreEarned);
    this.selectedLid = selectedLid;
    this.selectedLid.alpha = 0.3;
    this.selectedLid.inputEnabled = false;
    game.input.enable = false;
    bonusGm.openLid(this._onBonusScoreReceived, this);

};

BonusGameSubState.prototype._onBonusScoreReceived = function (returnData)
{

    if (!returnData.isCollect)
    {
        this.scoreReceived(returnData.credits, returnData.realMoney);

    }
    else
    {
        this.bonusCollect(returnData.credits, returnData.realMoney);
        gameUI._lightStreakStart(true);
    }
    gm.cumulativeCreditsWon += returnData.credits;
    gm.currentCredits += returnData.credits;
    gameUI.Interface.updateBalanceUI(true);

    /////////
    for (var j = 0; j < this.scoreArray.length; j++)
    {
        if (this.scoreArray[j] === returnData.credits)
        {
            this.points[j].tint = 000000;
            break;
        }
    }
};

BonusGameSubState.prototype.bonusCollect = function (credits, realMoney)
{

    if (!this.slugger.anims.knockOut) this.slugger.addSpriteAnimPair(this.anims.knockOut, 638, 393, this.mainGroup, 1.05, ["knockOut1", "knockOut2", "knockOut3", "knockOut4"], false, false);
    this.slugger.play(this.anims.knockOut);
    var knockOutRingTween;
    var sluggerKnockOut;
    game.time.events.add(Phaser.Timer.SECOND * 1.25, function ()
    {
        knockOutRingTween = game.add.tween(this.bg).to({ y: 20 }, 250, Phaser.Easing.Bounce.Out, true);
        sluggerKnockOut = game.add.tween(this.slugger.animSpritePairs[0].sprites[3]).to({ y: 400 }, 300, Phaser.Easing.Bounce.Out, true);
        knockOutRingTween.yoyo(true);
        sluggerKnockOut.yoyo(true);
    }, this);

    this.sluggerGroup.alpha = 0;
    this.collect.tint = 000000;
    console.log(this);
    if (credits > 0) bonusGm.addScore(credits, realMoney);
    audioManager.playBonusCollect();
    inputController.removeInputLock();
    for (var i = 0; i < this.buttons.length; i++)
    {
        this.buttons[i].inputEnabled = false;
    }
    this.showScoreScatterSelected(languageManager.langJSON.collect);
    game.time.events.add(Phaser.Timer.SECOND * 1, this.showTotalScore, this);
    this.totalwinValueText.alpha = 1;
};

BonusGameSubState.prototype.addBonusScore = function ()
{
    bonusGm.requestBonusScore(this.scoreReceived, this);
};

BonusGameSubState.prototype.scoreReceived = function (score)
{
    console.log("SCORE RECIEVED");
    var random = utils.getRandomIntInRange(1, 4);
    audioManager.playSluggerClick();
    audioManager.playBonusGameWin();
    switch (random)
    {
        case 1:
            inputController.addInputLock();
            if (!this.slugger.anims.face) this.slugger.addSpriteAnimPair(this.anims.face, 638, 393, this.mainGroup, 1.05, ["face1", "face2"], false);
            this.sluggerGroup.alpha = 0;

            audioManager.playSluggerHitSound();
            this.slugger.play(this.anims.face, this.getBackSlugger, this);
            break;
        case 2:
            inputController.addInputLock();
            if (!this.slugger.anims.hitsBack) this.slugger.addSpriteAnimPair(this.anims.hitsBack, 638, 393, this.mainGroup, 1.05, ["hitBack1", "hitBack2", "hitBack3", "hitBack4"], false);
            this.sluggerGroup.alpha = 0;
            audioManager.playSluggerHitPunch();
            this.slugger.play(this.anims.hitsBack, this.getBackSlugger, this);
            break;
        case 3:
            inputController.addInputLock();
            if (!this.slugger.anims.stomach) this.slugger.addSpriteAnimPair(this.anims.stomach, 638, 393, this.mainGroup, 1.05, ["stomach1", "stomach2"], false);
            this.sluggerGroup.alpha = 0;
            audioManager.playSluggerHitSound();
            this.slugger.play(this.anims.stomach, this.getBackSlugger, this);
            break;
        case 4:
            inputController.addInputLock();
            if (!this.slugger.anims.misses) this.slugger.addSpriteAnimPair(this.anims.misses, 638, 393, this.mainGroup, 1.05, ["misses1", "misses2"], false);
            this.sluggerGroup.alpha = 0;
            audioManager.playSluggerHitSound();
            this.slugger.play(this.anims.misses, this.getBackSlugger, this);
            break;
    }
    game.input.enable = true;
    bonusGm.addScore(score);
    this.showScoreScatterSelected("+" + score);
    this.selectedLid = null;
    this.updateBonusUI(score, this.winValueText);

};

BonusGameSubState.prototype.getBackSlugger = function ()
{
    inputController.removeInputLock();
    this.sluggerGroup.alpha = 1;
};
//Burasına bakılacak
BonusGameSubState.prototype._spawnsmallStars = function (textureKey, posX, posY)
{
    var smallStar = this.starsPool.getFromPool(textureKey);

    if (!smallStar)
    {
        smallStar = game.add.sprite(posX, posY, textureKey);
    }
    else
    {
        if (!smallStar.game)
        {
            smallStar.destroy();
            smallStar = game.add.sprite(posX, posY, textureKey);
        }
        else smallStar.reset(posX, posY);
    }
    smallStar.anchor.setTo(0.5);
    smallStar.scale.setTo(1, 1);
    game.world.bringToTop(smallStar);
    var tweenSmallStarX = game.add.tween(smallStar).to({ x: smallStar.x - 100 }, 500, Phaser.Easing.Quadratic.Out, true);
    var tweenSmallStarY0 = game.add.tween(smallStar).to({ y: smallStar.y - 50 }, 250, Phaser.Easing.Quadratic.Out, true);
    var tweenSmallStarY1 = game.add.tween(smallStar).to({ y: smallStar.y + 100 }, 500, Phaser.Easing.Quadratic.Out, false);
    var tweenSmallStarY2 = game.add.tween(smallStar.scale).to({ x: 0, y: 0 }, 1000, Phaser.Easing.Quadratic.Out, true);
    tweenSmallStarY0.chain(tweenSmallStarY1);
    tweenSmallStarY1.chain(tweenSmallStarY2);

    tweenSmallStarY2.onComplete.addOnce(function ()
    {
        this.context.starsPool.addToPool(this.sprite);
    }, { context: this, sprite: smallStar });

},

BonusGameSubState.prototype._spawnbigStars = function (textureKey, posX, posY)
{
    var smallStar = this.starsPool.getFromPool(textureKey);

    if (!smallStar)
    {
        smallStar = game.add.sprite(posX, posY, textureKey);
    }
    else
    {
        if (!smallStar.game)
        {
            smallStar.destroy();
            smallStar = game.add.sprite(posX, posY, textureKey);
        }
        else smallStar.reset(posX, posY);
    }
    smallStar.anchor.setTo(0.5);
    smallStar.scale.setTo(1, 1);
    game.world.bringToTop(smallStar);
    var tweenSmallStarX = game.add.tween(smallStar).to({ x: smallStar.x + 100 }, 500, Phaser.Easing.Quadratic.Out, true);
    var tweenSmallStarY0 = game.add.tween(smallStar).to({ y: smallStar.y - 50 }, 250, Phaser.Easing.Quadratic.Out, true);
    var tweenSmallStarY1 = game.add.tween(smallStar).to({ y: smallStar.y + 100 }, 500, Phaser.Easing.Quadratic.Out, false);
    var tweenSmallStarY2 = game.add.tween(smallStar.scale).to({ x: 0, y: 0 }, 1000, Phaser.Easing.Quadratic.Out, true);
    tweenSmallStarY0.chain(tweenSmallStarY1);
    tweenSmallStarY1.chain(tweenSmallStarY2);

    tweenSmallStarY2.onComplete.addOnce(function ()
    {
        this.context.starsPool.addToPool(this.sprite);
    }, { context: this, sprite: smallStar });

},

BonusGameSubState.prototype.showScoreScatterSelected = function (data)
{
    console.log("SHOW SCORE SCATTER");
    var posX = this.selectedLid.position.x;
    var posY = this.selectedLid.position.y;
    var scorePopup = game.add.bitmapText(
        posX,
        posY,
        "badaboomAutospin",
        data.toString(), 100);
    scorePopup.anchor.setTo(0.5, 0.5);
    scorePopup.scale = { x: 1, y: 1 };

    this.textPopups.push(scorePopup);
    this._spawnsmallStars("smallStar", posX, posY);
    this._spawnbigStars("bigStar", posX, posY);
    var tween0 = game.add.tween(scorePopup.scale).from({ x: 0, y: 0 }, 500, Phaser.Easing.Quadratic.Out, true, 250);
    var tween1 = game.add.tween(scorePopup.scale).to({ x: 0, y: 0 }, 500, Phaser.Easing.Quadratic.Out, false, 0);
    tween1.onComplete.addOnce(function () { this.destroyPoint(scorePopup); }, this);
    tween0.chain(tween1);
};

BonusGameSubState.prototype.destroyPoint = function (target)
{
    target.destroy();
};

BonusGameSubState.prototype.showTotalScore = function ()
{

    this.clickmeKO.inputEnabled = true;

    var i;
    for (i = 0; i < this.buttons.length; i++)
    {
        this.buttons[i].inputEnabled = false;
    }
    this.scoreProgress = 0;
    this.updateScore = true;




    game.time.events.add(Phaser.Timer.SECOND * 1.5, function ()
    {
        game.add.tween(this.clickmeKO.scale).to({ x: 1, y: 1 }, 1000, Phaser.Easing.Bounce.Out, true);
        audioManager.playClickMeKOintroSound();
    }, this);
    game.time.events.add(Phaser.Timer.SECOND * 1.5, function ()
    {
        game.add.tween(this.totalwinValueText.scale).to({ x: 1, y: 1 }, 500, Phaser.Easing.Bounce.Out, true);
    }, this);
    game.time.events.add(Phaser.Timer.SECOND * 2, function ()
    {
        var t = game.add.tween(this.totalwinValueText.scale).to({ x: 1.5, y: 1.5 }, 500, Phaser.Easing.Bounce.Out, true);
        this.updateBonusUI(0, this.totalwinValueText);
        t.yoyo(true);
    }, this);

    for (i = 0; i < this.textPopups.length; i++)
    {
        this.textPopups[i].visible = false;
    }
};

BonusGameSubState.prototype.playOutroAnimation = function ()
{
    if (debugUtils.debugSettings.showLogs)
        console.log("BonusUI - playOutroAnimation: ");
    this.onOutroAnimationCompleted();
    console.log("BonusUI - playIntroAnimation2");
};

BonusGameSubState.prototype.playIntroAnimation = function ()
{
    if (debugUtils.debugSettings.showLogs)
        console.log("BonusUI - playIntroAnimation");

    this.onIntroAnimationCompleted();
    console.log("BonusUI - playIntroAnimation2");
};


BonusGameSubState.prototype.onIntroAnimationCompleted = function ()
{
    console.log("BonusUI - onIntroAnimationCompleted");
    for (var i = 0; i < this.buttons.length; i++)
    {
        this.buttons[i].inputEnabled = true;
    }
};

BonusGameSubState.prototype.onOutroAnimationCompleted = function ()
{
    if (debugUtils.debugSettings.showLogs)
        console.log("BonusUI - onOutroAnimationCompleted");
};

BonusGameSubState.prototype.init = function (mask)
{
    this.bonusMask = mask;
};

BonusGameSubState.prototype.resetBonusGame = function ()
{
    var i;
    for (i = 0; i < this.textPopups.length; i++)
    {
        this.textPopups[i].visible = true;
    }

    this.clickmeKO.alpha = 1;

    this.clickmeKO.inputEnabled = true;
    this.totalwinValueText.alpha = 0;
    this.clickmeKO.input.enabled = true;
    //this.totalwinValueText.alpha = 1;
    this.clickmeKO.events.onInputDown.addOnce(this.returnToGame, this);
    if (this.scoreTableGroup && this.scoreTableGroup.game)
    {
        for (i = 0; i < this.points.length; i++)
        {
            this.points[i].tint = 0xffffff;
        }
        this.collect.tint = 0xffffff;
        this.winValueText.setText(0);

    }
    this.sluggerGroup.alpha = 1;
    for (var k = 0; k < this.buttons.length; k++)
    {
        this.buttons[k].alpha = 0;
    }
    this.areaOutlines.alpha = 1;
    console.log("ALPHA" + this.areaOutlines.alpha);
    console.log(this.areaOutlines);

    //for (var i = 0; i < this.buttons.length; i++) {
    //    this.buttons[i].inputEnabled = false;
    //    this.buttons[i].events.onInputDown.add(this.openLid, this);
    //}

}


