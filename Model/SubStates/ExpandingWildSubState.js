﻿function ExpandingWildSubState() { }

var parent = extend(ExpandingWildSubState, Completable);

ExpandingWildSubState.prototype._expandingWild = null;
ExpandingWildSubState.prototype._lampParticles = null;
ExpandingWildSubState.prototype._lampParticlesAnim = null;
ExpandingWildSubState.prototype._expandingWildSymbol = null;
ExpandingWildSubState.prototype._entryAnimation = null;
ExpandingWildSubState.prototype._introSprite = null;
ExpandingWildSubState.prototype._introAnim = null;
ExpandingWildSubState.prototype._spotLights = null;
ExpandingWildSubState.prototype._multiplierIndex = null;
ExpandingWildSubState.prototype._jumpTween = null;
ExpandingWildSubState.prototype._group = null;

ExpandingWildSubState.prototype._spotLightAnimProgress = { step: 0 };




ExpandingWildSubState.prototype.begin = function ()
{
    this._positionUI();
};


ExpandingWildSubState.prototype._positionUI = function ()
{
    this._introSprite = game.add.sprite(this._expandingWildSymbol.sprite.x, this._expandingWildSymbol.sprite.y, "expandingWildAnim");
    this._introSprite.anchor.setTo(0.5);
    this._introSprite.scale.setTo(1.28);
    this._introSprite.visible = false;
    this._lampParticles = game.add.sprite(0, 0, "lampParticles");
    this._lampParticles.anchor.setTo(0.5,0);
    this._lampParticles.scale.setTo(1);
    this._lampParticles.visible = false;
    this._group.add(this._introSprite);
   
    

    this._introAnim = this._introSprite.animations.add("idle");
    this._lampParticlesAnim = this._lampParticles.animations.add("idle");

    var duration = 500;
    var targetX = gameUI.startPaddingX + (this._expandingWild.SelfConversion.Col * gameUI.symbolsPaddingX);
    var targetY = gameUI.startPaddingY + (this._expandingWild.SelfConversion.Row * gameUI.symbolsPaddingY) - (gameUI.symbolsPaddingY / 2) - 13;
    this._jumpTween = game.add.tween(this._introSprite).to({ x: targetX, y: targetY }, duration, Phaser.Easing.Linear.In, false);

    this._introAnim.onComplete.addOnce(this._onIntroAnimCompleted, this);
    this._jumpTween.onComplete.addOnce(this._onMoveAnimCompleted, this);

    game.time.events.add(125, function ()
    {
        gameUI.removeFromSymbolCache(this._expandingWildSymbol.col, this._expandingWildSymbol.row);
        this._introSprite.visible = true;
        this._introAnim.play();
        this._jumpTween.start();
    }, this);
    audioManager.playExpandinWildPunchSound();
};
ExpandingWildSubState.prototype.init = function (expandingWild, expandingWildSymbol, spotLights, group) {
    this._expandingWild = expandingWild;
    this._expandingWildSymbol = expandingWildSymbol;
    this._spotLights = spotLights;
    this._multiplierIndex = this._expandingWild.MultiplierIndex;
    this._group = group;
    console.log("expanding  "+this._expandingWild);
};

ExpandingWildSubState.prototype._onMoveAnimCompleted = function ()
{
    for (var i = 0; i < this._expandingWild.Conversions.length; i++)
    {
        gameUI.removeFromSymbolCache(this._expandingWild.Conversions[i].Col, this._expandingWild.Conversions[i].Row);
    }
};

ExpandingWildSubState.prototype._onIntroAnimCompleted = function ()
{
    game.tweens.removeFrom(this._spotLightAnimProgress, false);
    var expandingWildAnimSymbol = new BasicSymbol(this._introSprite, this._expandingWild.SelfConversion.Col, this._expandingWild.SelfConversion.Row);
    gameUI.setSymbolCache(this._expandingWild.SelfConversion.Col, this._expandingWild.SelfConversion.Row, expandingWildAnimSymbol, true);
    this._spotLightAnimProgress.step = 0;
    var tween = game.add.tween(this._spotLightAnimProgress).to({ step: (this._multiplierIndex + 1) * 21 }, 2000, Phaser.Easing.Exponential.Out, true);
    tween.onUpdateCallback(this.valueUpdate, this);
    tween.onComplete.add(this.valueComplete, this);
};

ExpandingWildSubState.prototype.valueUpdate = function (tween, percent)
{
    var index = ((Math.round(this._spotLightAnimProgress.step) + this._multiplierIndex) % 3);
    //console.log(index + ":" + (Math.round(this._spotLightAnimProgress.step) + this._multiplierIndex));
    for (var i = 0; i < this._spotLights.length; i++)
    {
        this._spotLights[i].frame = i === index ? 1 : 0;
    }
}

ExpandingWildSubState.prototype.valueComplete = function ()
{
    var index = ((Math.round(this._spotLightAnimProgress.step) + this._multiplierIndex) % 3);
    //console.log(index + ":" + (Math.round(this._spotLightAnimProgress.step) + this._multiplierIndex));
    for (var i = 0; i < this._spotLights.length; i++)
    {
        if (index === i ) {
            this._spotLights[i].frame = 1;
            this._lampParticles.visible = true;
            this._lampParticles.position.x = this._spotLights[i].position.x-50;
            this._lampParticles.position.y = this._spotLights[i].position.y;

        }
        else this._spotLights[i].frame = 0;
    }

    this._lampParticlesAnim.play();
    this._onComplete();
};