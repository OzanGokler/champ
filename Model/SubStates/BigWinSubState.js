﻿function BigWinSubState() {
    Object.defineProperty(this, "ContainerLayer", {
        get: function() { return this._container }
    });
    this.state = this.states.CLOSED;
}

var parent = extend(BigWinSubState, Completable);
BigWinSubState.prototype._winAmount = 0;
BigWinSubState.prototype._staticSprite = null;
BigWinSubState.prototype._moneySplashSprites = new Array();
BigWinSubState.prototype._moneySplashAnims = new Array();
BigWinSubState.prototype._activeMoneySplashAnimIndex = 0;
BigWinSubState.prototype._scoreText = null;
BigWinSubState.prototype._scoreTextTween = null;
BigWinSubState.prototype._container = null;
BigWinSubState.prototype._scoreAnimProgress = { step: 0 };
BigWinSubState.prototype.states = { CLOSED: 0, OPENED: 1, CLOSING: 2, OPENNING: 3 };
BigWinSubState.prototype.state = undefined;
BigWinSubState.prototype._onTapEvent = undefined;
BigWinSubState.prototype._autoHideEvent = undefined;
BigWinSubState.prototype._startTextAnimEvent = undefined;
BigWinSubState.prototype._cancellableIntroTweens = new Array();
BigWinSubState.prototype._dismissHotKey = null;
BigWinSubState.prototype._dismissHotkeyEvent = null;



BigWinSubState.prototype.init = function (lineWinTotal)
{
    this._winAmount = lineWinTotal;
}
BigWinSubState.prototype.begin = function ()
{
    if (this.state === this.states.CLOSED)
    {
        this.state = this.states.OPENNING;
        audioManager.playBigWinSound();
        this._positionUI();

        //Delay to prevent hickups on anim start.
        game.time.events.add(150, this._delayedStart, this);
    }
}

BigWinSubState.prototype._delayedStart = function ()
{
    this._initializeInputs();
    this._startAnimation();
}

BigWinSubState.prototype._startAnimation = function ()
{
    this._cancellableIntroTweens.push(game.add.tween(this._staticSprite.scale).to({ x: 0.7, y: 0.7 }, 350, Phaser.Easing.Linear.None, true));
    this._startTextAnimEvent = game.time.events.add(350, this._startTextAnimation, this);
}

BigWinSubState.prototype._startTextAnimation = function ()
{
    console.log("try _startTextAnimation: " + this.state);
    if (this.state === this.states.OPENNING || this.state === this.states.OPENED)
    {
        console.log("_startTextAnimation: " + this.state);
        audioManager.startCounterSound();
        game.tweens.removeFrom(this._scoreAnimProgress, false);
        this._scoreAnimProgress.step = 0;
        this._scoreTextTween.start();
        this._scoreTextTween.onUpdateCallback(this._valueUpdate, { text: this._scoreText, startValue: 0, endValue: this._winAmount });
        this._scoreTextTween.onComplete.add(this._valueComplete, { text: this._scoreText, endValue: this._winAmount });
        this._scoreTextTween.onComplete.add(function ()
        {
            this.state = this.states.OPENED;
            audioManager.stopCounterSound();
        }, this);

        if (gm.isAutoSpinning)
        {
            this._scoreTextTween.onComplete.addOnce(function ()
            {
                console.log("_scoreTextTween.onComplete: " + this.state);
                this._autoHideEvent = game.time.events.add(1200, this.hide, this);
            }, this);
        }

        this._cancellableIntroTweens.push(this._scoreAnimProgress);
    }

}

BigWinSubState.prototype._positionUI = function ()
{
    if (!this._container || !this._container.game)
    {
        this._container = game.add.group();

        textureOverlay.fade(0x000000, undefined, 0.2, 0, 250, true);

        for (var i = 0; i < 4; i++)
        {
            var tempSprite = game.add.sprite(game.width / 2, vm.ActiveGameHeight / 2, "moneySplash" + (i + 1));
            tempSprite.anchor.setTo(0.5);
            tempSprite.scale.setTo(2, 2);

            this._moneySplashSprites.push(tempSprite);
            this._moneySplashAnims.push(tempSprite.animations.add("anim"));

            if (i > 0)
            {
                this._moneySplashSprites[i].visible = false;
                this._moneySplashAnims[i - 1].onComplete.addOnce(function ()
                {
                    this.context._moneySplashSprites[this.animIndex - 1].visible = false;

                    this.context._moneySplashSprites[this.animIndex].visible = true;
                    this.context._moneySplashAnims[this.animIndex].play(25);

                    this.context._activeMoneySplashAnimIndex = this.animIndex;
                }, { context: this, animIndex: i });
            }
        }
        this._activeMoneySplashAnimIndex = 0;
        this._moneySplashAnims[this._activeMoneySplashAnimIndex].play(25);

        this._staticSprite = game.add.sprite(game.width / 2, vm.ActiveGameHeight / 2, "bigWin");
        this._staticSprite.anchor.setTo(0.5);
        this._staticSprite.scale.setTo(0);

        this._scoreText = game.add.bitmapText(game.width / 2 + 7, vm.ActiveGameHeight / 2 + 156, "chalkFont", "", 60);
        this._scoreText.anchor.setTo(0.5, 0.5);

        this._container.add(this._staticSprite);
        this._container.add(this._scoreText);
    }

    this._scoreTextTween = game.add.tween(this._scoreAnimProgress).to({ step: 1 }, 3000, Phaser.Easing.Linear.Out, false);
    this._anim = this._staticSprite.animations.add("intro");
    game.world.bringToTop(this._container);

    this._cancellableIntroTweens.push(this._scoreTextTween);
}

BigWinSubState.prototype._valueUpdate = function (twn, percent)
{
    this.startValue = this.startValue == undefined ? 0 : this.startValue;
    this.text.setText(game.math.linear(this.startValue, this.endValue, percent).toFixed(2));
},

BigWinSubState.prototype._valueComplete = function ()
{
    this.text.setText(this.endValue.toFixedNonZero(2));
}

BigWinSubState.prototype._initializeInputs = function ()
{
    this._staticSprite.inputEnabled = true;
    this._onTapEvent = game.input.onTap.addOnce(this.hide, this).getListener();
    this._staticSprite.events.onInputDown.addOnce(this.hide, this);

    this._dismissHotKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    this._dismissHotkeyEvent = this._dismissHotKey.onDown.addOnce(this.hide, this).getListener();;
}

BigWinSubState.prototype.hide = function ()
{
    if (this.state !== this.states.CLOSING && this.state !== this.states.CLOSED)
    {
        this.state = this.states.CLOSING;
        textureOverlay.fade(0x000000, undefined, 0, undefined, 250, true);

        this._staticSprite.inputEnabled = false;
        this._staticSprite.events.onInputDown.removeAll();

        this._dismissHotKey.onDown.remove(this._dismissHotkeyEvent);

        game.input.onTap.remove(this._onTapEvent);
        if (this._autoHideEvent) game.time.events.remove(this._autoHideEvent);
        if (this._startTextAnimEvent) game.time.events.remove(this._startTextAnimEvent);

        for (var i = 0; i < this._cancellableIntroTweens.length; i++)
        {
            game.tweens.remove(this._cancellableIntroTweens[i]);
        }
        this._cancellableIntroTweens.splice(0, this._cancellableIntroTweens.length);

        game.world.bringToTop(this._container);

        game.add.tween(this._moneySplashSprites[this._activeMoneySplashAnimIndex]).to({ alpha: 0 }, 350, Phaser.Easing.Linear.None, true);

        var hideTween = game.add.tween(this._staticSprite.scale).to({ x: 0, y: 0 }, 350, Phaser.Easing.Linear.None, true);
        game.add.tween(this._scoreText.scale).to({ x: 0, y: 0 }, 350, Phaser.Easing.Linear.None, true);
        //game.add.tween(this._bgRays).to({ alpha: 0 }, 350, Phaser.Easing.Linear.None, true);
        game.add.tween(this._scoreText.position).to(this._staticSprite.position, 350, Phaser.Easing.Linear.None, true);
        hideTween.onComplete.addOnce(this._stateCompleted, this);

    }
}

BigWinSubState.prototype._stateCompleted = function ()
{
    audioManager.stopCounterSound();
    this.state = this.states.CLOSED;
    gameUI.Interface.updateBalanceUI(true);
    gameUI.Interface.updateSpinUI(gm.SpinsLeft); // updatechange
    for (var j = 0; j < this._moneySplashAnims.length; j++)
    {      
        this._moneySplashAnims[j].destroy();
        this._moneySplashSprites[j].destroy();
    }
    this._moneySplashAnims.splice(0,this._moneySplashAnims.length);
    this._moneySplashSprites.splice(0, this._moneySplashSprites.length);
    this._container.destroy(true);
    this._onComplete();
    
}