﻿GroupHorizontalLayout = function(paddingFromEdges, minDistanceBetweenItems)
{
    this.paddingFromEdges = paddingFromEdges;
    this.minDistanceBetweenItems = minDistanceBetweenItems;
    this.cachedPositionX = new Array();
}

GroupHorizontalLayout.prototype.paddingFromEdges = 200;
GroupHorizontalLayout.prototype.minDistanceBetweenItems = 200;
GroupHorizontalLayout.prototype.cachedPositionX = null;

GroupHorizontalLayout.prototype.positionItems = function(group, relatedItemCount, recalculate)
{
    var i;
    if (!recalculate && this.cachedPositionX.length)
    {
        for (i = 0; i < group.children.length; i++)
        {
            group.children[i].position.x = this.cachedPositionX[i];
            //group.children[i].position.y = this.cachedPositionX[i].y;
        }
        return;
    }
    if (this.cachedPositionX.length) this.cachedPositionX.splice(0, this.cachedPositionX.length);
    var itemCount = group.length / relatedItemCount;
    var padding = this.paddingFromEdges + 15;
    var actualWidth;
    var spaceBetweenTexts;
    do
    {
        padding -= 15;
        actualWidth = (game.width - (game.scalingOffset + padding) * 2);
        spaceBetweenTexts = actualWidth / (itemCount);
    }
    while (spaceBetweenTexts < this.minDistanceBetweenItems)

    for (i = 0; i < itemCount; i++)
    {
        //Evenly position all without aligning.
        group.children[i].x = spaceBetweenTexts * (i) + (group.children[i].width / 2);
        var newOffset = 0;
        for (var j = 0; j < relatedItemCount - 1; j++)
        {
            if (i + j + itemCount === group.children[i].length)
            {
                newOffset = group.children[i + j].x + (group.children[i + j].width) + 10;
            }
            else newOffset = group.children[i + j].x + (group.children[i + j].width / 2) + 10;
            newOffset = group.children[i + j].x + (group.children[i + j].width * (1 - group.children[i + j].anchor.x)) + 10;
            group.children[i + j + itemCount].x = newOffset;
        }
    }

    //Reposition and center everything on screen.
    var posOffset = (group.children[group.length - 1].x + group.children[group.length - 1].width * (1 - group.children[group.length - 1].anchor.x)) / 2;
    for (i = 0; i < group.children.length; i++)
    {
        group.children[i].x -= posOffset;
        group.children[i].x += game.width / 2;
        this.cachedPositionX.push(group.children[i].x);
    }
}