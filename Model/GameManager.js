﻿function GameManager() {
    Object.defineProperty(this, "IsAutoSpinEnabled", {
        get: function () { return gm.autoSpinEnabled && gm.autoSpinIndex > 0; },
        set: function (value) { gm.autoSpinEnabled = value; }
    });

    Object.defineProperty(this, "CurrentSpinState", {
        get: function () { return this._currentSpinState; },
        set: function (x) {
             this._currentSpinState = x;
        }
    });

    Object.defineProperty(this, "TotalBetInRealMoney", {
        get: function () {
            return this.coinValues && this.coinValueIndex !== undefined ? this.winLinesValue * this.betLevelValue * this.coinValues[this.coinValueIndex] : 0;
        }
    });

    Object.defineProperty(this, "CumulativeRealMoneyWon", {
        get: function () {
            return this.coinValues && this.coinValueIndex !== undefined ? this.convertCreditsToRealMoney(this.cumulativeCreditsWon, this.coinValues[this.coinValueIndex]) : 0;
        }
    });

    Object.defineProperty(this, "CurrentRealMoney", {
        get: function () {
            return this.coinValues && this.coinValueIndex !== undefined ? this.convertCreditsToRealMoney(this.currentCredits, this.coinValues[this.coinValueIndex]) : 0;
        }
    });

    Object.defineProperty(this, "TotalBetInCredits", {
        get: function () { return this.winLinesValue * this.betLevelValue }
    });

    Object.defineProperty(this, "SpinsLeft", {
        get: function () { return this._spinsLeft; },
        set: function (x) {

            this._spinsLeft = x;
            //if (this._spinsLeft < 0) this._spinsLeft = 0;
            gameUI.Interface.updateSpinUI(this._spinsLeft);
        }
    });
}

GameManager.prototype =
{
    spinStates:
    {
        idle: 0,
        spinning: 1,
        spinned: 2,
        dropping: 3,
        scatterBonus: 4,
        bonusGame: new BonusGameSubState(),
        expandingWild: new ExpandingWildSubState(),
        multiplierSubState: new MultiplierSubState(),
        winLines: new WinLineSubState(),
        bigWin: new BigWinSubState()
    },
    _currentSpinState: 0,
    isAutoSpinning: false,
    isDummySpin: true,
    spinReturnData: null,
    autoSpinIndex: 0,
    autoSpinValues: [0, 10, 25, 50, Number.MAX_VALUE],
    cumulativeCreditsWon: 0,
    bigWinRatio: 25,
    betValue: 0,
    _spinsLeft: 0,
    autoSpinEnabled: false,
    coinValueIndex: null,
    betLevelValue: null,
    winLinesValue: null,
    coinValues: null,
    currentCredits:0,

    clearCumulativeWonAmount: function (activeInterface)
    {
        this.cumulativeCreditsWon = 0;
        activeInterface.updateBalanceUI(true);
    },

    canSpin: function () {
        console.log("isAUTOSPINNING +++" + this.isAutoSpinning);
        console.log("ASpinsLeft +++" + this.SpinsLeft);
        return this.autoSpinEnabled && (this.isAutoSpinning && this.SpinsLeft > 0);
    },

    convertRealMoneyToCredits: function (realMoney, coinValue)
    {
        coinValue = coinValue === undefined ? this.coinValues[this.coinValueIndex] : coinValue;
        return realMoney / coinValue;
    },

    convertCreditsToRealMoney: function (credits, coinValue)
    {
        coinValue = coinValue === undefined ? this.coinValues[this.coinValueIndex] : coinValue;
        return credits * coinValue;
    },

    increaseAutoSpin: function ()
    {
        this.autoSpinIndex++;
        if (this.autoSpinIndex >= this.autoSpinValues.length) this.autoSpinIndex = 0;
        this.SpinsLeft = this.getAutoSpinValue();
    },

    decreaseAutoSpin: function ()
    {
        this.autoSpinIndex--;
        if (this.autoSpinIndex <= 0) this.autoSpinIndex = 0;
        this.SpinsLeft = this.getAutoSpinValue();
    },

    getAutoSpinValue: function () { return this.autoSpinValues[this.autoSpinIndex]; },

    resetAutoSpinValue: function ()
    {
        this.autoSpinIndex = 0;
        this.SpinsLeft = this.getAutoSpinValue();
    },

    startState: function (gameToStart, clearWorld, clearCache, arg1, arg2)
    {

        debugUtils.log("gameManager - startState: " + gameToStart);
        clearWorld = clearWorld === undefined ? true : clearWorld;
        clearCache = clearCache === undefined ? false : clearCache;
        vm.startState(gameToStart, clearWorld, clearCache, arg1, arg2);
    },

    gameReady: function ()
    {
        debugUtils.log("gameManager - gmGameReady");
        this.startState(gameStates.main);
    },

    updateBetUI: function (betLevel, winLines, coinValue)
    {
        vm.updateBetUI(betLevel, winLines, coinValue);
    },

    startBonusRound: function ()
    {
        gm.startState(gameStates.bonus);
    },

    requestToken: function (callback, context)
    {
        //sm.makeQueuedRequest(function () { sm.requestToken(callback, context) });
        sm.makeQueuedRequest(sm.requestToken, callback, context);
    },

    requestConfiguration: function (callback, context)
    {
        sm.makeQueuedRequest(sm.requestConfiguration, callback, context);
    },

    requestPayTable: function (callback, context)
    {
        sm.makeQueuedRequest(sm.requestPayTable, callback, context);
    },

    requestInitializationData: function (callback, context)
    {
        sm.makeQueuedRequest(sm.requestInitializationData, callback, context);
    },

    requestCurrentBalance: function (callback, context)
    {
        sm.makeQueuedRequest(sm.requestCurrentBalance, callback, context);
    },

    requestCurrentBet: function (callback, context, asValues)
    {
        sm.makeQueuedRequest(sm.requestCurrentBet, callback, context, asValues);
    },

    requestBetMax: function (callback, context)
    {
        sm.makeQueuedRequest(sm.requestBetMax, callback, context);
    },

    requestCanChangeBet: function (callback, context, betLevelIndex, coinValueIndex, winLineIndex)
    {
        sm.makeQueuedRequest(sm.requestCanChangeBet, callback, context, betLevelIndex, coinValueIndex, winLineIndex);
    },

    requestSetBetValues: function (callback, context, allowSpecial, betLevelIndex, coinValueIndex, winLineIndex, forcePreview)
    {
        sm.makeQueuedRequest(sm.requestSetBetValues, callback, context, allowSpecial, betLevelIndex, coinValueIndex, winLineIndex, forcePreview);
    },

    requestSpin: function (callback, context, allowSpecial, betLevelIndex, coinValueIndex, winLineIndex, dummySpin)
    {
        sm.makeQueuedRequest(sm.requestSpinResult, callback, context, allowSpecial, betLevelIndex, coinValueIndex, winLineIndex, dummySpin);
    },
    
    requestBalanceData: function ()
    {
        sm.makeQueuedRequest(sm.requestBalanceData, gameUI.Interface.updateBalanceUI);
    },

    requestWinLines: function (callback, context, maxWinLine)
    {
        sm.makeQueuedRequest(sm.requestWinLines, callback, context, maxWinLine);
    },

    requestExpandingWildResult: function (callback, context)
    {
        sm.makeQueuedRequest(sm.requestWinLines, callback, context);
    }
}

var gm = new GameManager();