﻿var InputController = function() { };

InputController.prototype = 
{
    _inputLock: 0,

    addInputLock: function ( )
    {
        this._inputLock++;
        debugUtils.log("Lock Add: " + this._inputLock, false);
        game.input.enabled = this._inputLock === 0;
    },
    removeInputLock: function (force)
    {
        if (force) this._inputLock = 0;
        else this._inputLock--;
        debugUtils.log("Lock Remove: " + this._inputLock, false);
        if (this._inputLock < 0) this._inputLock = 0;
        game.input.enabled = this._inputLock === 0;
    }
}

var inputController = new InputController();