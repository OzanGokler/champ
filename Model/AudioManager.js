﻿var AudioManager = function () { };

AudioManager.prototype = {
    _audioSprite: null,
    _scoreCounterSound: null,
    _winLinePlayIndex:0,


    //MAIN GAME
    _MainGameIntro001: "MainGameIntro001",
    _MainGameMusicLoop001: "MainGameMusicLoop001",
    _ReelStopSound001: "ReelStopSound001",

    _WinSoundkey: "WinSound00",
    //_WinSound002: "WinSound002",
    //_WinSound003: "WinSound003",
    //_WinSound004: "WinSound004",
    //_WinSound005: "WinSound005",
    //_winSoundCount: 5,

    //WINS AND SYMBOLS
    _Symbolkey: "Symbol001",
    _Symbol002: "Symbol002",
    _Symbol003: "Symbol003",
    _Symbol004: "Symbol004",
    _Symbol005: "Symbol005",
    _Symbol006: "Symbol006",
    _Symbol007: "Symbol007",
    _BigWinSound001: "BigWinSound001",

    //FREE SPINS
    _HoldSign001: "HoldSign001",
    _HoldSign002: "HoldSign002",
    _FreeSpinMultiplier001: "FreeSpinMultiplier001",

    //BONUS GAME
    _BonusGameIntro001: "BonusGameIntro001",
    _BonusGameLoop001: "BonusGameLoop001",
    _BonusGameClick001: "BonusGameClick001",
    _BonusGameHit001: "BonusGameHit001",
    _BonusGameHit002: "BonusGameHit002",
    _BonusGameHit003: "BonusGameHit003",
    _BonusGameWin001: "BonusGameWin001",
    _BonusGameWin002: "BonusGameWin002",
    _BonusMiscSoundFX001: "BonusMiscSoundFX001",
    _BonusMiscSoundFX002: "BonusMiscSoundFX002",
    _BonusMiscSoundFX003: "BonusMiscSoundFX003",
    _BonusMiscSoundFX004: "BonusMiscSoundFX004",
    _BonusGameOutro001: "BonusGameOutro001",

    //GUI SOUNDS
    _MenuClickUp001: "MenuClickUp001",
    _MenuClickDown001: "MenuClickDown001",
    _SpinButton001: "SpinButton001",
    _SpinButton002: "SpinButton002",

    //MISCELLANEOUS
    _MiscSoundFX001: "MiscSoundFX001",
    _MiscSoundFX002: "MiscSoundFX002",
    _Counter001: "Counter001",
    _Counter002: "Counter002",
    _CounterCurrent: null,
    _MiscSoundFX003: "MiscSoundFX003",
    _MiscSoundFX004: "MiscSoundFX004",
    _MiscSoundFX005: "MiscSoundFX005",
    _MiscSoundFX006: "MiscSoundFX006",

    _currentMusic: null,
    _ambientMusic: null,

    init: function ()
    {
        this._audioSprite = game.add.audioSprite("audioSprite");
    },

    _playSound: function (key, volume, loop)
    {
        return this._audioSprite.get(key).play(key, 0, volume, loop, false);
    },

    stopCurrentMusic: function ()
    {
        if (this._currentMusic)
        {
            this._currentMainMusicIndex = -1;
            this._currentMusic.onStop.removeAll();
            this._currentMusic.stop();
        }
    },

    playAmbientSound: function (restart)
    {
        if (!this._ambientMusic || restart)
        {
            this.stopAmbientSound();
            this._ambientMusic = this._playSound(this._MainGameIntro001, 1, true);
        }
    },

    playBonusCollect: function ()
    {
        this._playSound(this._BonusMiscSoundFX003, 2, false);
    },

    stopAmbientSound: function ()
    {
        if (this._ambientMusic) this._ambientMusic.fadeOut(1000);
        //this._ambientMusic.stop();
    },

    playMainMusic: function (crossFadeDuration)
    {
        if (this._audioSprite.isDecoding)
        {
            this._audioSprite.onDecoded.addOnce(function ()
            {
                this.playMainMusic(crossFadeDuration);
            }, this);
            return;
        }
        //console.log("this._audioSprite.isDecoding: " + this._audioSprite.isDecoding);
        if (!this._currentMusic || this._MainGameMusicLoop001 !== this._currentMusic.currentMarker)
        {
            crossFadeDuration = crossFadeDuration === undefined ? 1000 : crossFadeDuration;
            if (this._currentMusic) this._currentMusic.fadeOut(crossFadeDuration);

            this._currentMusic = this._playSound(this._MainGameMusicLoop001, 0.65, true);
            this._currentMusic.fadeIn(crossFadeDuration, true);
        }

        this.stopAmbientSound();
    },


    playBonusMusic: function ()
    {
        this._currentMusic.fadeOut();
        this._currentMusic = this._playSound(this._BonusGameLoop001, 1, true);
        
    },
    playBonusMusicIntro: function()
    {       
        this._playSound(this._BonusGameIntro001, 1, false);      
    },

    playBonusMusicOut: function () {
        this._playSound(this._BonusGameOutro001, 1, false);
    },

    playSluggerHitSound: function()
    {
        var random = utils.getRandomIntInRange(1, 3);
        switch (random)
        {
            case 1:
                this._playSound(this._BonusGameHit001, 2, false);
                break;
            case 2:
                this._playSound(this._BonusGameHit002, 2, false);
                break;
            case 3:
                this._playSound(this._BonusGameHit003, 2, false);
                break;
        }
    },
    playSluggerHitPunch: function()
    {
        var random = utils.getRandomIntInRange(1, 2);
        switch (random)
        {
            case 1:
                this._playSound(this._BonusMiscSoundFX001, 2, false);
                break;
            case 2:
                this._playSound(this._BonusMiscSoundFX002, 2, false);
        }
    },

    playWinSound: function (consecutive) {
        if (consecutive) {

            if (this._winLinePlayIndex<4) {
                this._winLinePlayIndex++;
            }

        } else this._winLinePlayIndex = 1;
        console.log(this._WinSoundkey + this._winLinePlayIndex);
        this._audioSprite.play(this._WinSoundkey + this._winLinePlayIndex);
    },

    playBigWinSound: function ()
    {
        this._audioSprite.play(this._BigWinSound001);
    },

    playMenuClickDown: function ()
    {
        this._audioSprite.play(this._MenuClickDown001, 1);
    },

    playMenuClickUp: function ()
    {
        this._audioSprite.play(this._MenuClickUp001, 1);
    },

    playMenuClickDownAlt: function ()
    {
        this._audioSprite.play(this._SpinButton001, 1);
    },

    playMenuClickUpAlt: function ()
    {
        this._audioSprite.play(this._SpinButton002, 1);
    },

    playScatterBonus: function ()
    {
        this._audioSprite.play(this._scatterBonusKey);
    },

    playSluggerClick:function()
    {
        this._playSound(this._BonusGameClick001, 1, false);
    },
    playBonusGameWin: function()
    {
        var random = utils.getRandomIntInRange(1, 2);
        switch (random)
        {
            case 1:
                this._playSound(this._BonusGameWin001, 1, false);
                break;
            case 2:
                this._playSound(this._BonusGameWin002, 1, false);
                break;
        }
    },

    playSymbolSound: function (symbolNumber)
    {
        switch (symbolNumber)
        {
            case 0:
                this._audioSprite.play(this._Symbol003);
                break;
            case 1:
                this._audioSprite.play(this._Symbol004);
                break;
            case 2:
                this._audioSprite.play(this._Symbol006);
                break;
            case 3:
                this._audioSprite.play(this._Symbol002);
                break;
            case 99:
                this._audioSprite.play(this._Symbol005);
                break;
        }
        
    },

    playSymbolStopSound: function ()
    {
        this._audioSprite.play(this._symbolStopSoundKey + (utils.getRandomIntInRange(1, this._symbolStopSoundCount)).pad(3));
    },

    playReelStopSound: function ()
    {
        this._playSound(this._ReelStopSound001, 1.5, false);
    },

    playSpinButton: function ()
    {
        this._audioSprite.play(this._SpinButton001);
    },

    startCounterSound: function ()
    {
        var random = utils.getRandomIntInRange(1, 2);
        if (random === 1)
            this._CounterCurrent = this._Counter001;
        else this._CounterCurrent = this._Counter002;
        this._playSound(this._CounterCurrent, 1, true);
    },

    stopCounterSound: function ()
    {
        this._audioSprite.stop(this._CounterCurrent);
    },

    addClickUpDownSound: function (items)
    {
        for (var i = 0; i < arguments.length; i++)
        {
            arguments[i].events.onInputUp.add(audioManager.playMenuClickUp, audioManager);
            arguments[i].events.onInputDown.add(audioManager.playMenuClickDown, audioManager);
        }      
    },
    playClickMeKOintroSound: function()
    {
        this._playSound(this._MiscSoundFX002, 2);
    },

    playExpandinWildPunchSound: function()
    {
        game.time.events.add(Phaser.Timer.SECOND * 1.10, function () {
            this._playSound(this._MiscSoundFX005, 1);
        }, this);
        game.time.events.add(Phaser.Timer.SECOND * 2, function () {
            this._playSound(this._MiscSoundFX006, 1);
        }, this);
        
    },

    addClickUpDownAltSound: function (items)
    {
        for (var i = 0; i < arguments.length; i++)
        {
            arguments[i].events.onInputUp.add(audioManager.playMenuClickUpAlt, audioManager);
            arguments[i].events.onInputDown.add(audioManager.playMenuClickDownAlt, audioManager);
        }
    },

    mute: function () { game.sound.mute = true; },
    unmute: function () { game.sound.mute = false; }
}

audioManager = new AudioManager();