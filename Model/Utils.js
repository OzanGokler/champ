﻿function Utils() { }

Utils.prototype =
{
    shuffleArray: function (array)
    {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffleArray...
        while (0 !== currentIndex)
        {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    },

    getRandomArbitraryInRange: function (min, max)
    {
        return Math.random() * (max - min) + min;
    },

    getRandomArbitrary: function (max)
    {
        return Math.random() * max;
    },

    getRandomIntInRange: function (min, max)
    {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    getRandomInt: function (max)
    {
        return Math.floor(Math.random() * (max + 1));
    },

    countObjectChildren: function (obj)
    {
        var count = 0;
        var i;
        for (i in obj)
        {
            if (obj.hasOwnProperty(i))
            {
                count++;
            }
        }
        return count;
    },

    objectHasMultipleElements: function (obj)
    {
        var count = 0;
        var i;
        for (i in obj)
        {
            if (obj.hasOwnProperty(i))
            {
                count++;
                if (count > 1) break;
            }
        }
        return count > 1;
    },

    updateDrawOrder: function (layerArray) {
        for (var i = 0; i < layerArray.length; i++) {
            if (layerArray[i]) {
                //console.log(layerArray[i]);
                game.world.bringToTop(layerArray[i]);
            }
        }
    }

}

var utils = new Utils;

function DebugUtils() { }

DebugUtils.prototype =
{
    debugSettings: null,
    //Call from render function.
    showMousePositionInt: function ()
    {
        game.debug.text("(" + Math.floor(game.input.mousePointer.x) + "," + Math.floor(game.input.mousePointer.y) + ")", game.input.mousePointer.x, game.input.mousePointer.y);
    },
    //Call from render function.
    showMousePosition: function ()
    {
        game.debug.text("(" + game.input.mousePointer.x + "," + game.input.mousePointer.y + ")", game.input.mousePointer.x, game.input.mousePointer.y);
    },

    log: function (text, printStackTrace)
    {
        if (!this.debugSettings || this.debugSettings.showLogs)
        {
            console.log(text);
            if (printStackTrace) console.trace();
        }
    }
};

var debugUtils = new DebugUtils;

var LoaderHelper = function () { };

LoaderHelper.prototype =
{
    getAssetPath: function (path, cache)
    {
        cache = cache === undefined ? debugUtils.debugSettings.allowCache : false;
        //Comes from index.html and bumped via compiler phyton.
        if (cache) return path + "?=" + gameVersion;
        return path + "?v=" + Date.now();
    }
}

var loaderHelper = new LoaderHelper();

// First, checks if it isn't implemented yet.
if (!Number.prototype.toFixedNonZero)
{
    Number.prototype.toFixedNonZero = function (x)
    {
        var that = (+this).toFixed(x);
        return that.replace(/([0-9]+(\.[0-9]+[1-9])?)(\.?0+$)/, "$1");
    }
}

// First, checks if it isn't implemented yet.
if (!Number.prototype.numberFormat)
{
    Number.prototype.numberFormat = function (decimals, decPoint, thousandsSep)
    {
        decPoint = typeof decPoint !== "undefined" ? decPoint : ",";
        thousandsSep = typeof thousandsSep !== "undefined" ? thousandsSep : ".";

        var parts = this.toFixed(decimals).split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousandsSep);

        return parts.join(decPoint);
    }
}

// First, checks if it isn't implemented yet.
if (!Number.prototype.pad)
{
    Number.prototype.pad = function (size)
    {
        var s = String(this);
        while (s.length < (size || 2)) { s = "0" + s; }
        return s;
    }
}

// First, checks if it isn't implemented yet.
if (!String.prototype.format)
{
    String.prototype.format = function ()
    {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number)
        {
            return typeof args[number] != "undefined"
              ? args[number]
              : match
            ;
        });
    };
}

/**
 * Extends one class with another.
 *
 * @param {Function} destination The class that should be inheriting things.
 * @param {Function} source The parent class that should be inherited from.
 * @return {Object} The prototype of the parent.
 */
function extend(destination, source)
{
    destination.prototype = Object.create(source.prototype);
    destination.prototype.constructor = destination;
    return source.prototype;
}