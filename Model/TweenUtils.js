﻿function TweenUtils() { }

TweenUtils.prototype =
{
    getShakeTweens: function (sprite, shakeCount, shakeAngle, shakeDuration)
    {
        var rotateTweens = new Array(shakeCount);
        var startAngle = sprite.angle;
        for (var j = 0; j < rotateTweens.length; j++)
        {
            var dur = (j === 0 ? shakeDuration / 2 : shakeDuration) ;
            var angle = (j % 2 ? -shakeAngle : shakeAngle);
            rotateTweens[j] = game.add.tween(sprite).to({ angle: angle }, dur, Phaser.Easing.Linear.In, false);
            if (j > 0)
            {
                rotateTweens[j - 1].chain(rotateTweens[j]);
            }
        }

        //Return to original angle.
        rotateTweens[rotateTweens.length - 1] = game.add.tween(sprite).to({ angle: startAngle }, shakeDuration / 2, Phaser.Easing.Linear.In, false);
        rotateTweens[rotateTweens.length - 2].chain(rotateTweens[rotateTweens.length - 1]);

        return rotateTweens;
    }
}

var tweenUtils = new TweenUtils();