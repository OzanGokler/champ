﻿function SpriteAnimPair(sprites, anims, loop) {
    this.sprites = sprites;
    this.anims = anims;
    this.loop = loop;
    if (Array.isArray(this.anims)) {
        for (var j = 0; j < this.anims.length - 1; j++) {
            sprites[j].visible = false;
            this.anims[j].onComplete.add(function () {
                sprites[this.targetIndex].visible = false;
                sprites[this.targetIndex + 1].visible = true;
                this.context.anims[this.targetIndex + 1].play(20, false);
            }, { context: this, targetIndex: j });
        }
        if (loop) {
            this.anims[this.anims.length - 1].onComplete.add(function () {
                this.sprites[this.sprites.length - 1].visible = false;
                this.sprites[0].visible = true;
                this.anims[0].play(20, false);
            }, this);
        }
    }
};
SpriteAnimPair.prototype =
{
    sprites: null,
    anims: null,
    play: function () {
        console.log("play");
        if (Array.isArray(this.anims)) {
            this.anims[0].play(30, false);
            this.sprites[0].visible = true;
        }
        else {
            this.sprites.visible = true;
            this.anims.play(30, this.loop);
        }
    },
    stop: function () {
        if (Array.isArray(this.anims)) {
            for (var i = 0; i < this.anims.length; i++) {
                this.sprites[i].visible = false;
                this.anims[i].stop();
            }
        }
        else {
            this.sprites.visible = false;
            this.anims.stop();
        }
    },
    destroy: function () {
        if (Array.isArray(this.anims)) {
            for (var i = 0; i < this.anims.length; i++) {
                this.sprites[i].destroy();
                this.anims[i].destroy();
            }
        } else {
            if (this.sprites) this.sprites.destroy();
            if (this.anims) this.anims.destroy();
        }
    },
    reset: function (x, y) {
        if (Array.isArray(this.sprites)) {
            for (var i = 0; i < this.sprites.length; i++) {
                this.sprites[i].reset(x, y);
            }
        } else {
            this.sprites.reset(x, y);
        }
    },
    isSpritesValid: function () {
        if (this.sprites) {
            if (Array.isArray(this.sprites)) {
                return (this.sprites[0].game !== undefined && this.sprites[0].game !== null);
            } else {
                return (this.sprites.game !== undefined && this.sprites.game !== null);
            }
        } else return false;
    },
    addOnCompleteCallbackOnce: function (callback, context) {
        if (!this.loop && callback && context) {
            if (Array.isArray(this.anims)) {
                return (this.anims[this.anims.length - 1].onComplete.addOnce(callback, context));
            } else {
                return (this.sprites.game !== undefined && this.sprites.game !== null);
            }
        }
    }
}
function CharacterAnimator() { };
CharacterAnimator.prototype =
{
    anims: new Array(),
    animSpritePairs: new Array(),
    defaultIndex: 0,
    init: function () {
        this.animSpritePairs.splice(0, this.animSpritePairs.length);
        this.anims.splice(0, this.anims.length);
    },

    addSpriteAnimPair: function (id, x, y, group, scale, spriteKey, isDefault, resetToDefault) {
        var idExists = false;
        for (var i = 0; i < this.anims.length; i++) {
            if (this.anims[i] === id) {
                idExists = true;
                break;
            }
        }
        if (!idExists) this.anims.push(id);

        resetToDefault = resetToDefault === undefined ? true : resetToDefault;
        if (isDefault) this.defaultIndex = id;
        var spriteAnimPair = this.animSpritePairs[id];
        if (spriteAnimPair && spriteAnimPair.isSpritesValid()) { spriteAnimPair.reset(x, y); }
        else {
            if (spriteAnimPair && !spriteAnimPair.isSpritesValid()) spriteAnimPair.destroy();
            spriteAnimPair = this.createSpriteAndAnim(x, y, group, scale, spriteKey, isDefault, resetToDefault);
        }
        this.animSpritePairs[id] = spriteAnimPair;
        this.animSpritePairs[id].stop();
    },

    destroy: function () {
        console.log("ANIMATIONSSSSSSSS");
        console.log(this.animSpritePairs);
        console.log(this.animSpritePairs.length);
        for (var i = 0; i < this.animSpritePairs.length; i++) {
            console.log(i);
            if (!this.animSpritePairs) {
                this.animSpritePairs[i].destroy();
            }
            
        }
    },
    createSpriteAndAnim: function (x, y, group, scale, spriteKeys, isDefault, resetToDefault) {
        var tempSprite;
        var tempAnim;
        if (Array.isArray(spriteKeys)) {
            var sprites = new Array();
            var anims = new Array();
            for (var i = 0; i < spriteKeys.length; i++) {
                tempSprite = game.add.sprite(x, y, spriteKeys[i]);
                tempSprite.anchor.set(0.5, 0.5);
                if (group) group.add(tempSprite);
                tempSprite.scale.setTo(scale === undefined ? 1 : scale);
                tempAnim = tempSprite.animations.add("anim");
                sprites.push(tempSprite);
                anims.push(tempAnim);
            }
            if (!isDefault && resetToDefault) anims[anims.length - 1].onComplete.add(function () { this.play(this.defaultIndex); }, this);
            return new SpriteAnimPair(sprites, anims, isDefault);
        }
        else {
            tempSprite = game.add.sprite(x, y, spriteKeys);
            tempSprite.anchor.set(0.5, 0.5);
            if (group) group.add(tempSprite);
            tempSprite.scale.setTo(scale === undefined ? 1 : scale);
            tempAnim = tempSprite.animations.add("anim");
            if (!isDefault && resetToDefault) tempAnim.onComplete.add(function () { this.play(this.defaultIndex); }, this);
            return new SpriteAnimPair(tempSprite, tempAnim, isDefault);
        }
    },
    play: function (spriteIndex, onCompleteCallback, context) {
        for (var i = 0; i < this.anims.length; i++) {
            var animIndex = this.anims[i];
            console.log(animIndex);
            if (animIndex === spriteIndex) {
                this.animSpritePairs[animIndex].play();
                this.animSpritePairs[animIndex].addOnCompleteCallbackOnce(onCompleteCallback, context);
            }
            else this.animSpritePairs[animIndex].stop();
        }
    }
}