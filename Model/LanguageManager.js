﻿function LanguageManager()
{
    var userLang = (navigator.language || navigator.userLanguage).toLowerCase();
    if (userLang.indexOf("en") > -1) this.language = "EN";
    //else if (userLang.indexOf("tr") > -1) this.language = "TR";

    if (!this.language) this.language = "EN";
}


LanguageManager.prototype.language = undefined;
LanguageManager.prototype.langJSON = "en";

LanguageManager.prototype.cacheCurrentLanguage = function ()
{
    this.langJSON = game.cache.getJSON("language");
}

languageManager = new LanguageManager();