setTimeout(function ()
{
    //if (Phaser.Plugin.Debug) game.add.plugin(Phaser.Plugin.Debug);

    var div = document.getElementById("debugUiContainer");

    $(document).keypress(function (e)
    {
        if (e.which === 100)
        {
            if (div.style.display === "block") div.style.display = "none";
            else div.style.display = "block";
        }
    });

    var noOfSpins = 10;
    var selects = new Array();

    var onSelectChanged = function ()
    {
        var isAnyDisabled = false;
        for (var k = 1; k < selects.length; k++)
        {
            if (selects[k - 1].selectedIndex === 0 || isAnyDisabled)
            {
                selects[k].disabled = true;
                isAnyDisabled = true;
            }
            else selects[k].disabled = false;
        }
    };

    var createOption = function (index)
    {
        var optionContainer = document.createElement("div");
        optionContainer.id = "option" + index;
        var label = document.createElement("Label");
        label.innerHTML = "Spin " + (index + 1) + ": ";
        var select = document.createElement("select");
        optionContainer.id = "select" + index;

        select.onchange = onSelectChanged;

        for (var i = 0; i < spinOptions.length; i++)
        {
            select.options.add(new Option(spinOptions[i], spinOptions[i], false, false));
        }

        optionContainer.appendChild(label);
        optionContainer.appendChild(select);

        div.appendChild(optionContainer);
        selects.push(select);
    };

    for (var j = 0; j < noOfSpins; j++)
    {
        createOption(j);
    }

    var saveButton = document.createElement("button");
    var saveButtonText = document.createTextNode("Save Spin Queue");
    saveButton.appendChild(saveButtonText);
    saveButton.onclick = function ()
    {
        spinDebugQueue.splice(0, spinDebugQueue.length);
        for (var k = 0; k < selects.length; k++)
        {
            if (!selects[k].disabled && selects[k].selectedIndex !== 0)
            {
                spinDebugQueue.push(spinOptions[selects[k].selectedIndex]);
            }
        }
    };
    div.appendChild(saveButton);

    var clearButton = document.createElement("button");
    var clearButtonText = document.createTextNode("Clear Spin Queue");
    clearButton.appendChild(clearButtonText);
    clearButton.onclick = function ()
    {
        spinDebugQueue.splice(0, spinDebugQueue.length);
        for (var k = 0; k < selects.length; k++)
        {
            selects[k].selectedIndex = 0;
        }
        onSelectChanged();
    };
    div.appendChild(clearButton);


    var resetPlayerButton = document.createElement("button");
    var resetPlayerButtonText = document.createTextNode("Reset Player");
    resetPlayerButton.appendChild(resetPlayerButtonText);
    resetPlayerButton.onclick = function ()
    {
        var dummyFunct = function () { };
        sm.requestSimple("ResetPlayerSession", undefined, dummyFunct, this);
        if (!alert("Click OK to refresh!"))
        {
            window.location.reload();
        }
    };
    div.appendChild(resetPlayerButton);

    var resetGameButton = document.createElement("button");
    var resetGameButtonText = document.createTextNode("Reset Game");
    resetGameButton.appendChild(resetGameButtonText);
    resetGameButton.onclick = function ()
    {
        var dummyFunct = function () { };
        sm.requestSimple("ResetGameSession", undefined, dummyFunct, this);
        if (!alert("Click OK to refresh!"))
        {
            window.location.reload();
        }
    };
    div.appendChild(resetGameButton);

    onSelectChanged();
}, 1000);

var spinOptions = ["none", "regular", "win", "win-3x", "win-4x", "win-5x", "no-win", "expanding-wild", "bonus-belt"];

var spinDebugQueue = new Array();
